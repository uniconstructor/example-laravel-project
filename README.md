# Управление автотранспортом

Проект основан на базе фреймворка Laravel 5.3. Этот репозиторий содержит клиентскую и серверную часть системы управления автотранспортом.

##### Установка

* [Установка (Docker)](docs/install-with-docker.md)

```bash
git clone git@gitlab.com:iflat/transport.git ./
composer install
```

Настройка и запуск vagrant (не обязательно)

```bash
cp Homestead.example.yaml Homestead.yaml
vagrant up
```

Установка пакета для преобразования DOM в PDF
```bash
sudo apt-get install wkhtmltopdf
```


Заполнить секции `DB_CONNECTION` и `USERSIDE_DB_CONNECTION`,
для файла .env.testing прописать в начале `APP_ENV=testing`

```bash
cp .example .env
artisan key:generate # Полученный ключ записать в .env APP_KEY = base64:...
cp .env .env.testing
```

Примeнить миграции

```bash
artisan migrate
artisan migrate --path=vendor/venturecraft/revisionable/src/migrations
```

Импорт сотрудников и отделов из Юзерсайда

```bash
php artisan db:seed --class="ImportDepartments" -vvv
php artisan db:seed --class="ImportIflatUsers" -vvv
```

Создать изначальный набор типов документов:
```bash
php artisan db:seed --class="DocumentTypesTableSeeder" -vvv
```

Начальный набор оповещений:
```bash
php artisan db:seed --class="NotificationsSeeder"
```

Включить public-доступ к загружаемым файлам (для документов):

```bash
php artisan storage:link
```

Настройка прав на папку для загрузки изображений:
```bash
sudo chmod -R 755 /home/ilya/phpstorm-projects/iftat-transport/storage
```

##### Сборка проекта очистка кеша

Обновление
```bash
composer l5-build
```

Чистка кеша
```bash
composer l5-cc
```

##### Команды генератора

Миграция:
```bash
php artisan infyom:migration MyModel --fieldsFile="my-model.json"
```

Модель:
```bash
php artisan infyom:model MyModel --fieldsFile="my-model.json" --skip=migration
```

Все остальное:
```bash
php artisan infyom:api_scaffold MyModel --fieldsFile="my-model.json" --skip=migration,model
```


##### Документация

* Загрузка файлов в Laravel: https://laravel.com/docs/5.3/filesystem#file-uploads
