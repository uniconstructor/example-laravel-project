<?php

namespace App\Repositories;

use App\Models\Car;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CarRepository
 * @package App\Repositories
 * @version August 17, 2017, 9:02 pm MSK
 *
 * @method Car findWithoutFail($id, $columns = ['*'])
 * @method Car find($id, $columns = ['*'])
 * @method Car first($columns = ['*'])
*/
class CarRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'vendor',
        'model',
        'reg_number',
        'status',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Car::class;
    }
}
