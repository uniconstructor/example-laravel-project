<?php

namespace App\Repositories;

use App\Models\DocumentType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DocumentTypeRepository
 * 
 * @package App\Repositories
 * @version September 26, 2017, 11:05 am MSK
 *
 * @method DocumentType findWithoutFail($id, $columns = ['*'])
 * @method DocumentType find($id, $columns = ['*'])
 * @method DocumentType first($columns = ['*'])
*/
class DocumentTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name',
        'slug',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     */
    public function model()
    {
        return DocumentType::class;
    }
}
