<?php

namespace App\Repositories;

use App\Models\CarNotificationType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CarNotificationTypeRepository
 * 
 * @package App\Repositories
 * @version March 23, 2018, 6:33 am MSK
 *
 * @method CarNotificationType findWithoutFail($id, $columns = ['*'])
 * @method CarNotificationType find($id, $columns = ['*'])
 * @method CarNotificationType first($columns = ['*'])
*/
class CarNotificationTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name',
        'trigger_event',
        'subject_template',
        'action_text_template',
        'action_url_template',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     */
    public function model()
    {
        return CarNotificationType::class;
    }
}
