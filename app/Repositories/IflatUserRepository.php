<?php

namespace App\Repositories;

use App\Models\IflatUser;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class IflatUserRepository
 * 
 * @package App\Repositories
 * @version September 7, 2017, 3:27 pm MSK
 *
 * @method IflatUser findWithoutFail($id, $columns = ['*'])
 * @method IflatUser find($id, $columns = ['*'])
 * @method IflatUser first($columns = ['*'])
*/
class IflatUserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'firstname',
        'lastname',
        'middlename',
        'email',
        'phone',
        'position',
        'status',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     */
    public function model()
    {
        return IflatUser::class;
    }
}
