<?php

namespace App\DataTables;

use Illuminate\Database\Query\Builder;
use App\Models\Car;
use Form;
use Yajra\Datatables\Services\DataTable;

class CarDataTable extends DataTable
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->filterColumn('status_text', function ($query, $keyword) {
                $statuses = collect(Car::$statusList)->filter(function ($statusText) use ($keyword) {
                    return str_contains($statusText, $keyword);
                })->map(function ($item, $key) {
                    return $key;
                });
                if ($statuses->isNotEmpty()) {
                    $query->whereIn('status', $statuses->toArray());
                }
            })
            ->addColumn('action', 'cars.datatables_actions')
            ->make(true);
    }
    
    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $cars = Car::with(['department', 'employee']);
        
        return $this->applyScopes($cars);
    }
    
    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%', 'title' => 'Действия'])
            ->ajax('')
            ->parameters(
                [
                    //'serverSide' => false,
                    //'data'    => $this->query()->get()->toJson(),
                    'dom'     => 'Bfrtip',
                    'scrollX' => false,
                    // настройка изначальной сортировки грида
                    'order'   => [
                        1, // номер колонки по которой изначально сортируются записи (название)
                        'asc',
                    ],
                    'buttons' => [
                        [
                            'extend' => 'print',
                            'text'   => '<i class="fa fa-print"></i> Печать',
                        ],
                        [
                            'extend' => 'reset',
                            'text'   => '<i class="fa fa-undo"></i> Сбросить фильтр',
                        ],
                        [
                            'extend' => 'reload',
                            'text'   => '<i class="fa fa-refresh"></i> Перезагрузить',
                        ],
                        [
                            'extend'  => 'collection',
                            'text'    => '<i class="fa fa-download"></i> Экспорт',
                            'buttons' => [
                                'csv',
                                'excel',
                                //'pdf',
                            ],
                        ],
                        [
                            'extend' => 'colvis',
                            'text'   => '<i class="fa fa-table"></i> Столбцы',
                        ],
                    ],
                ]
            );
    }
    
    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'id'                  => [
                'title' => 'ID',
                'name'  => 'id',
                'data'  => 'id',
            ],
            'vendor'              => [
                'title' => 'Марка',
                'name'  => 'vendor',
                'data'  => 'vendor',
            ],
            'model'               => [
                'title' => 'Модель',
                'name'  => 'model',
                'data'  => 'model',
            ],
            'reg_number'          => [
                'title' => 'Номер',
                'name'  => 'reg_number',
                'data'  => 'reg_number',
            ],
            'department.name'     => [
                'title'      => 'Подразделение',
                'name'       => 'department.name',
                'data'       => 'department_name',
                'orderable'  => true,
                'searchable' => true,
            ],
            'employee.lastname'   => [
                'title'      => 'Сотрудник',
                'name'       => 'employee.lastname',
                'data'       => 'iflat_user_name',
                'orderable'  => true,
                'searchable' => true,
            ],
            'employee.firstname'  => [
                'title'      => 'Сотрудник',
                'name'       => 'employee.firstname',
                'data'       => 'iflat_user_name',
                'orderable'  => true,
                'searchable' => true,
                'visible'    => false,
                'exportable' => false,
                'printable'  => false,
            ],
            'employee.middlename' => [
                'title'      => 'Сотрудник',
                'name'       => 'employee.middlename',
                'data'       => 'iflat_user_name',
                'orderable'  => true,
                'searchable' => true,
                'visible'    => false,
                'exportable' => false,
                'printable'  => false,
            ],
            'status_text'         => [
                'title'      => 'Статус',
                'name'       => 'status_text',
                'data'       => 'status_text',
                'orderable'  => true,
                'searchable' => true,
            ],
            'updated_at'          => [
                'title' => 'Последнее изменение',
                'name'  => 'updated_at',
                'data'  => 'updated_at',
            ],
        ];
    }
    
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'cars';
    }
}
