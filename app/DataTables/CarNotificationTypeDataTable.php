<?php

namespace App\DataTables;

use App\Models\CarNotificationType;
use Form;
use Yajra\Datatables\Services\DataTable;

class CarNotificationTypeDataTable extends DataTable
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->filterColumn('trigger_event', function ($query, $keyword) {
                $events = collect(CarNotificationType::$eventTypes)->filter(function ($event) use ($keyword) {
                    return str_contains($event['title'], $keyword);
                })->map(function ($event, $key) {
                    return $key;
                });
                if ($events->isNotEmpty()) {
                    $query->whereIn('trigger_event', $events->toArray());
                }
            })
            ->addColumn('action', 'car_notification_types.datatables_actions')
            ->make(true);
    }
    
    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $carNotificationTypes = CarNotificationType::query();
        
        return $this->applyScopes($carNotificationTypes);
    }
    
    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%', 'title' => 'Действия'])
            ->ajax('')
            ->parameters(
                [
                    'dom'     => 'Bfrtip',
                    'scrollX' => false,
                    'buttons' => [
                        [
                            'extend' => 'print',
                            'text'   => '<i class="fa fa-print"></i> Печать',
                        ],
                        [
                            'extend' => 'reset',
                            'text'   => '<i class="fa fa-undo"></i> Сбросить фильтр',
                        ],
                        [
                            'extend' => 'reload',
                            'text'   => '<i class="fa fa-refresh"></i> Перезагрузить',
                        ],
                        [
                            'extend'  => 'collection',
                            'text'    => '<i class="fa fa-download"></i> Экспорт',
                            'buttons' => [
                                'csv',
                                'excel',
                                //'pdf',
                            ],
                        ],
                        [
                            'extend' => 'colvis',
                            'text'   => '<i class="fa fa-table"></i> Столбцы',
                        ],
                    ],
                ]
            );
    }
    
    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'id'            => [
                'name' => 'id',
                'data' => 'id',
            ],
            'name'          => [
                'name'  => 'name',
                'data'  => 'name',
                'title' => 'Название',
            ],
            /*'description'          => [
                'name'  => 'description',
                'data'  => 'description',
                'title' => 'Описание',
            ],*/
            'trigger_event' => [
                'name'  => 'trigger_event',
                'data'  => 'trigger_event_name',
                'title' => 'Событие',
            ]
        ];
    }
    
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'carNotificationTypes';
    }
}
