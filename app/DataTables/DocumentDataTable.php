<?php

namespace App\DataTables;

use App\Models\Document;
use App\Models\DocumentType;
use Form;
use Yajra\Datatables\Services\DataTable;

class DocumentDataTable extends DataTable
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->filterColumn('type_name', function ($query, $keyword) {
                $docTypes = DocumentType::where('name', 'like', '%' . $keyword . '%')->get();
                if ($docTypes->isNotEmpty()) {
                    $docTypeIds = $docTypes->pluck('id')->toArray();
                    $query->whereIn('type_id', $docTypeIds);
                }
            })
            ->addColumn('action', 'documents.datatables_actions')
            ->make(true);
    }
    
    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $documents = Document::query()->with(['car', 'documentType']);
        
        return $this->applyScopes($documents);
    }
    
    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%', 'title' => 'Действия'])
            ->ajax('')
            ->parameters([
                'dom'     => 'Bfrtip',
                'scrollX' => false,
                // настройка изначальной сортировки грида
                'order'   => [
                    1, // номер колонки по которой изначально сортируются записи (название)
                    'asc'
                ],
                'buttons' => [
                    [
                        'extend' => 'print',
                        'text'   => '<i class="fa fa-print"></i> Печать',
                    ],
                    [
                        'extend' => 'reset',
                        'text'   => '<i class="fa fa-undo"></i> Сбросить фильтр',
                    ],
                    [
                        'extend' => 'reload',
                        'text'   => '<i class="fa fa-refresh"></i> Перезагрузить',
                    ],
                    [
                        'extend'  => 'collection',
                        'text'    => '<i class="fa fa-download"></i> Экспорт',
                        'buttons' => [
                            'csv',
                            'excel',
                            //'pdf',
                        ],
                    ],
                    [
                        'extend' => 'colvis',
                        'text'   => '<i class="fa fa-table"></i> Столбцы',
                    ],
                ],
            ]);
    }
    
    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'id'         => [
                'title' => 'ID',
                'name'  => 'id',
                'data'  => 'id',
            ],
            'name'       => [
                'title' => 'Название',
                'name'  => 'name',
                'data'  => 'name',
            ],
            //'slug' => ['name' => 'slug', 'data' => 'slug'],
            'type_name' => [
                'title' => 'Тип',
                'name'  => 'type_name',
                'data'  => 'type_name',
            ],
            'car.model'     => [
                'title' => 'Автомобиль',
                'name'  => 'car.model',
                'data'  => 'car_name',
            ],
            //'creator_id' => ['name' => 'creator_id', 'data' => 'creator_id'],
            'start_date' => [
                'title' => 'Дата начала действия',
                'name'  => 'start_date',
                'data'  => 'start_date',
            ],
            'end_date'   => [
                'title' => 'Дата окончания действия',
                'name'  => 'end_date',
                'data'  => 'end_date',
            ],
        ];
    }
    
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'documents';
    }
}
