<?php

namespace App\DataTables;

use App\Models\DocumentType;
use Form;
use Yajra\Datatables\Services\DataTable;

class DocumentTypeDataTable extends DataTable
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'document_types.datatables_actions')
            ->make(true);
    }
    
    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $documentTypes = DocumentType::query();
        
        return $this->applyScopes($documentTypes);
    }
    
    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%', 'title' => 'Действия'])
            ->ajax('')
            ->parameters([
                'dom'     => 'Bfrtip',
                'scrollX' => false,
                // настройка изначальной сортировки грида
                'order'   => [
                    1, // номер колонки по которой изначально сортируются записи (название)
                    'asc'
                ],
                'buttons' => [
                    [
                        'extend' => 'print',
                        'text'   => '<i class="fa fa-print"></i> Печать',
                    ],
                    [
                        'extend' => 'reset',
                        'text'   => '<i class="fa fa-undo"></i> Сбросить фильтр',
                    ],
                    [
                        'extend' => 'reload',
                        'text'   => '<i class="fa fa-refresh"></i> Перезагрузить',
                    ],
                    [
                        'extend'  => 'collection',
                        'text'    => '<i class="fa fa-download"></i> Экспорт',
                        'buttons' => [
                            'csv',
                            'excel',
                            //'pdf',
                        ],
                    ],
                    [
                        'extend' => 'colvis',
                        'text'   => '<i class="fa fa-table"></i> Столбцы',
                    ],
                ],
            ]);
    }
    
    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'id'          => [
                'title' => 'ID',
                'name'  => 'id',
                'data'  => 'id',
            ],
            'name'        => [
                'title' => 'Название',
                'name'  => 'name',
                'data'  => 'name',
            ],
            'slug'        => [
                'title' => 'Код',
                'name'  => 'slug',
                'data'  => 'slug',
            ],
            'description' => [
                'title' => 'Описание',
                'name'  => 'description',
                'data'  => 'description',
            ],
            //'parent_id' => ['name' => 'parent_id', 'data' => 'parent_id'],
            //'creator_id' => ['name' => 'creator_id', 'data' => 'creator_id'],
            /*'created_at' => [
                'title' => 'Дата создания',
                'name'  => 'created_at',
                'data'  => 'created_at',
            ],*/
            /*'updated_at' => [
                'title' => 'Последнее изменение',
                'name'  => 'updated_at',
                'data'  => 'updated_at',
            ],*/
        ];
    }
    
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'documentTypes';
    }
}
