<?php

namespace App\DataTables;

use App\Models\Department;
use Form;
use Yajra\Datatables\Services\DataTable;

class DepartmentDataTable extends DataTable
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'departments.datatables_actions')
            ->make(true);
    }
    
    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $departments = Department::query();
        
        return $this->applyScopes($departments);
    }
    
    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%', 'title' => 'Действия'])
            ->ajax('')
            ->parameters(
                [
                    'dom'     => 'Bfrtip',
                    'scrollX' => false,
                    // настройка изначальной сортировки грида
                    'order'   => [
                        1, // номер колонки по которой изначально сортируются записи (название)
                        'asc'
                    ],
                    'buttons' => [
                        [
                            'extend' => 'print',
                            'text'   => '<i class="fa fa-print"></i> Печать',
                        ],
                        [
                            'extend' => 'reset',
                            'text'   => '<i class="fa fa-undo"></i> Сбросить фильтр',
                        ],
                        [
                            'extend' => 'reload',
                            'text'   => '<i class="fa fa-refresh"></i> Перезагрузить',
                        ],
                        [
                            'extend'  => 'collection',
                            'text'    => '<i class="fa fa-download"></i> Экспорт',
                            'buttons' => [
                                'csv',
                                'excel',
                                //'pdf',
                            ],
                        ],
                        /*[
                            'extend' => 'colvis',
                            'text'   => '<i class="fa fa-table"></i> Столбцы',
                        ],*/
                    ],
                ]
            );
    }
    
    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'id'   => [
                'title' => 'ID',
                'name'  => 'id',
                'data'  => 'id',
            ],
            'name' => [
                'title' => 'Название',
                'name'  => 'name',
                'data'  => 'name',
            ],
            //'parent_id'  => ['name' => 'parent_id', 'data' => 'parent_id'],
            //'_lft'       => ['name' => '_lft', 'data' => '_lft'],
            //'_rgt'       => ['name' => '_rgt', 'data' => '_rgt'],
            //'created_at' => ['name' => 'created_at', 'data' => 'created_at'],
            //'updated_at' => ['name' => 'updated_at', 'data' => 'updated_at'],
        ];
    }
    
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'departments';
    }
}
