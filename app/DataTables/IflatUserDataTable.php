<?php

namespace App\DataTables;

use App\Models\IflatUser;
use Form;
use Yajra\Datatables\Services\DataTable;

class IflatUserDataTable extends DataTable
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('action', 'iflat_users.datatables_actions')
            ->make(true);
    }
    
    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $iflatUsers = IflatUser::query();
        
        return $this->applyScopes($iflatUsers);
    }
    
    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%', 'title' => 'Действия'])
            ->ajax('')
            ->parameters(
                [
                    'dom'     => 'Bfrtip',
                    'scrollX' => false,
                    // настройка изначальной сортировки грида (по фамилии)
                    'order'   => [
                        1, // номер колонки по которой изначально сортируются записи (фамилия)
                        'asc'
                    ],
                    'buttons' => [
                        [
                            'extend' => 'print',
                            'text'   => '<i class="fa fa-print"></i> Печать',
                        ],
                        [
                            'extend' => 'reset',
                            'text'   => '<i class="fa fa-undo"></i> Сбросить фильтр',
                        ],
                        [
                            'extend' => 'reload',
                            'text'   => '<i class="fa fa-refresh"></i> Перезагрузить',
                        ],
                        [
                            'extend'  => 'collection',
                            'text'    => '<i class="fa fa-download"></i> Экспорт',
                            'buttons' => [
                                'csv',
                                'excel',
                                //'pdf',
                            ],
                        ],
                        [
                            'extend' => 'colvis',
                            'text'   => '<i class="fa fa-table"></i> Столбцы',
                        ],
                    ],
                ]
            );
    }
    
    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'id'         => [
                'title' => 'ID',
                'name'  => 'id',
                'data'  => 'id',
            ],
            /*'department_id' => [
                'title' => '',
                'name' => 'department_id',
                'data' => 'department_id',
            ],*/
            'lastname'   => [
                'title' => 'Фамилия',
                'name'  => 'lastname',
                'data'  => 'lastname',
            ],
            'firstname'  => [
                'title' => 'Имя',
                'name'  => 'firstname',
                'data'  => 'firstname',
            ],
            'middlename' => [
                'title' => 'Отчество',
                'name'  => 'middlename',
                'data'  => 'middlename',
            ],
            'position'   => [
                'title' => 'Должность',
                'name'  => 'position',
                'data'  => 'position',
            ],
            'email'      => [
                'title' => 'Email',
                'name'  => 'email',
                'data'  => 'email',
            ],
            'phone'      => [
                'title' => 'Телефон',
                'name'  => 'phone',
                'data'  => 'phone',
            ],
            /*'status'        => [
                'title' => '',
                'name'  => 'status',
                'data'  => 'status',
            ],*/
        ];
    }
    
    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'iflatUsers';
    }
}
