<?php

namespace App\Notifications\Channels;

use App\Notifications\CarNotification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use GuzzleHttp\Client as HttpClient;


class IflatChannel
{
    /** @var HttpClient $http */
    protected $http;

    public function __construct(HttpClient $http)
    {
        $this->http = $http;
    }

    /**
     * @param $notifiable
     * @param Notification $notification
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send($notifiable, Notification $notification)
    {
        /** @var MailMessage $message */
        /** @var CarNotification $notification */
        $message = $notification->toIflat($notifiable);
        $response = $this->http->request('POST', '/message/send',
            [
                'json' => [
                    'recipient' => $message->to,
                    'subject' => $message->subject,
                    'message' => implode(' ', $message->introLines),
                ],
            ]
        );
        \Log::info($response->getBody());
    }
}
