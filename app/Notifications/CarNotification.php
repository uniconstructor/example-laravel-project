<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
// типы каналов оповещений
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Messages\SlackAttachment;
// используемые модели приложения
use App\Models\Car;
use App\Models\CarNotificationType;

/**
 * Фабрика оповещений о событиях автомобилей
 *
 * @package App\Notifications
 */
class CarNotification extends Notification //implements ShouldQueue
{
    /**
     * @see https://laravel.com/docs/5.3/queues
     */
    //use Queueable;
    
    /**
     * @var CarNotificationType
     */
    protected $notificationType;
    /**
     * @var Car
     */
    protected $car;
    /**
     * @var string - email для отправки оповещения
     */
    protected $email;
    /**
     * @var string - телефон для отправки смс
     */
    protected $phone;
    /**
     * @var array
     */
    protected $channels = [
        'slack',
        'App\Notifications\Channels\IflatChannel'
    ];
    
    /**
     * Create a new notification instance.
     *
     * @param CarNotificationType $notificationType - тип отправляемого оповещения
     * @param Car                 $car              - автомобиль для которого отправляется оповещение
     */
    public function __construct(CarNotificationType $notificationType, Car $car)
    {
        $this->notificationType = $notificationType;
        $this->car              = $car;
        
        // определяем контактные данные для отправки оповещения
        // @todo учитывать настройки каналов из модели типа оповещения
        if ($this->car->iflatUser) {
            if ($this->car->iflatUser->email) {
                $this->email      = $this->car->iflatUser->email;
                $this->channels[] = 'mail';
            }
            if ($this->car->iflatUser->phone) {
                $this->phone = $this->car->iflatUser->phone;
                // @todo включить sms-оповещение когда для него предоставят API
                //$this->channels[] = 'sms';
            }
        }
    }
    
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return $this->channels;
    }
    
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $subject    = template($this->notificationType->subject_template, $this->car->toArray());
        $message    = template($this->notificationType->email_template, $this->car->toArray());
        $level      = $this->notificationType->level ? $this->notificationType->level : 'info';
        $actionText = template($this->notificationType->action_text_template, $this->car->toArray());
        $actionUrl  = url(template($this->notificationType->action_url_template, $this->car->toArray()));
        
        return (new MailMessage)
            ->to($this->email)
            ->level($level)
            ->subject($subject)
            ->line($message)
            ->action($actionText, $actionUrl);
    }

    /**
     * Отправлять нотификации используя iFlat messageSender
     */
    public function toIflat($notifiable)
    {
        $message = $this->toMail($notifiable);
        $message->to($notifiable->email);
        return $message;
    }
    
    /**
     * Get the Slack representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return SlackMessage
     */
    public function toSlack($notifiable)
    {
        $car     = $this->car;
        $subject = template($this->notificationType->subject_template, $this->car->toArray());
        $message = template($this->notificationType->slack_template, $this->car->toArray());
        //$level      = $this->notificationType->level != 'info' ? $this->notificationType->level : '';
        $actionText = template($this->notificationType->action_text_template, $this->car->toArray());
        $actionUrl  = url(template($this->notificationType->action_url_template, $this->car->toArray()));
        
        // @todo создавать данные для slack-вложений в соответствии с типами оповещений
        // @todo настраивать уровни оповещений
        
        return (new SlackMessage)
            //->success()
            //->from('iflat-transport')
            ->to('#transport')
            ->content($message)
            ->attachment(
                function (SlackAttachment $attachment) use ($actionUrl, $subject, $car) {
                    $nextMaintenence = ($car->last_maintenance + $car->maintenance_interval) - $car->mileage;
                    $nextOilRefresh  = ($car->last_oil_refresh + $car->oil_refresh_interval) - $car->mileage;
                    $attachment->title($subject, $actionUrl)
                        ->fields(
                            [
                                'Автомобиль'                => $car->full_name,
                                'Пробег'                    => $car->mileage . ' км',
                                'Последнее ТО'              => $car->last_maintenance . ' км',
                                'До следующего ТО'          => $nextMaintenence . ' км',
                                'Последняя замена масла'    => $car->last_oil_refresh . ' км',
                                'До следующей замены масла' => $nextOilRefresh . ' км',
                            ]
                        );
                }
            );
    }
    
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [];
    }
    
    /**
     * @todo SMS через API оповещений iFlat
     *
     * @param $notifiable
     *
     * @return array
     */
    public function toIflatSms($notifiable)
    {
        return [];
    }
    
    /**
     * @todo email через API оповещений iFlat
     *
     * @param $notifiable
     *
     * @return array
     */
    public function toIflatEmail($notifiable)
    {
        return [];
    }
}