<?php


namespace App\Database;

use Illuminate\Database\Migrations\Migration as LaravelMigration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

/**
 * Расширенный класс миграции, добавляющий часто используемые функции
 *
 * @package App\Database
 */
class Migration extends LaravelMigration
{
    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'created_at';
    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'updated_at';
    /**
     * The name of the "deleted at" column.
     *
     * @var string
     */
    const DELETED_AT = 'deleted_at';
    
    /**
     * @var string название таблицы с которой производятся все операции
     *             Как правило одна миграция работает с одной таблицей,
     *             поэтому одного поля достаточно
     */
    protected $table;
    
    /**
     * Проверить существование колонки в таблице, если она существует - удалить ее
     *
     * @param string $column - название колонки
     * @param string $table  - название таблицы
     *
     * @return null
     *
     * @throws \InvalidArgumentException
     */
    public function rollbackColumn($column, $table = null)
    {
        Schema::table($this->getTable($table), function (Blueprint $table) use ($column) {
            if (Schema::hasColumn($table->getTable(), $column)) {
                $table->dropColumn($column);
            }
        });
    }
    
    /**
     * роверить существование колонки в таблице, если она существует - удалить индекс
     *
     * @param string $column - название колонки
     * @param string $table  - название таблицы
     *
     * @return null
     *
     * @throws \InvalidArgumentException
     */
    public function rollbackIndex($column, $table = null)
    {
        Schema::table($this->getTable($table), function (Blueprint $table) use ($column) {
            if (Schema::hasColumn($table->getTable(), $column)) {
                $table->dropIndex($this->createIndexName('index', $column));
            }
        });
    }
    
    /**
     * Создать индексы для timestamp-полей
     *
     * @param string $table
     *
     * @return null
     *
     * @throws \InvalidArgumentException
     */
    public function setTimestampsIndex($table = null)
    {
        Schema::table($this->getTable($table), function (Blueprint $table) {
            if (Schema::hasColumn($table->getTable(), self::CREATED_AT)) {
                $table->index(self::CREATED_AT);
            }
            if (Schema::hasColumn($table->getTable(), self::UPDATED_AT)) {
                $table->index(self::UPDATED_AT);
            }
        });
    }
    
    /**
     * Создать индекс для softdelete-поля
     *
     * @param string $table
     *
     * @return null
     *
     * @throws \InvalidArgumentException
     */
    public function setSoftDeletesIndex($table = null)
    {
        Schema::table($this->getTable($table), function (Blueprint $table) {
            if (Schema::hasColumn($table->getTable(), self::DELETED_AT)) {
                $table->index(self::DELETED_AT);
            }
        });
    }
    
    /**
     *
     * @param null $table
     *
     * @return null
     *
     * @throws \InvalidArgumentException
     */
    public function rollbackTimestamps($table = null)
    {
        $this->rollbackColumn(self::CREATED_AT, $table);
        $this->rollbackColumn(self::UPDATED_AT, $table);
    }
    
    /**
     *
     * @param null $table
     *
     * @return null
     *
     * @throws \InvalidArgumentException
     */
    public function rollbackSoftDeletes($table = null)
    {
        $this->rollbackColumn(self::DELETED_AT, $table);
    }
    
    /**
     *
     * @param null $table
     *
     * @return null
     *
     * @throws \InvalidArgumentException
     */
    public function rollbackTimestampsIndex($table = null)
    {
        $this->rollbackIndex(self::CREATED_AT, $table);
        $this->rollbackIndex(self::UPDATED_AT, $table);
    }
    
    /**
     *
     * @param null $table
     *
     * @return null
     *
     * @throws \InvalidArgumentException
     */
    public function rollbackSoftDeletesIndex($table = null)
    {
        $this->rollbackIndex(self::DELETED_AT, $table);
    }
    
    /**
     * Автоматически вычислить полное название индекса из имени колонки и удалить его
     *
     * @param string|array $columns
     * @param string|null  $table
     *
     * @return null
     *
     * @throws \InvalidArgumentException
     */
    public function dropColumnIndex($columns, $table = null)
    {
        $table = $this->getTable($table);
        
        if (!is_array($columns)) {
            $columns = [$columns];
        }
        // вычисляем полное название индекса
        $index = $this->createIndexName('index', $columns, $table);
        
        // удаляем индекс
        Schema::table($table, function (Blueprint $table) use ($index) {
            $table->dropIndex($index);
        });
    }
    
    /**
     * Create a default index name for the table.
     *
     * @param  string       $type - index type ('index' or 'foreign')
     * @param  array|string $columns
     * @param  array        $table
     *
     * @return string
     *
     * @throws \InvalidArgumentException
     *
     * @see \Illuminate\Database\Schema\Blueprint::createIndexName()
     */
    protected function createIndexName($type, array $columns, $table = null)
    {
        $index = strtolower($this->getTable($table) . '_' . implode('_', $columns) . '_' . $type);
        
        return str_replace(['-', '.'], '_', $index);
    }
    
    /**
     *
     * @param string $table
     *
     * @return string $table
     *
     * @throws \InvalidArgumentException
     */
    protected function getTable($table = null)
    {
        if ($table) {
            return $table;
        }
        if ($this->table) {
            return $this->table;
        }
        
        throw new \InvalidArgumentException('Table name required');
    }
    
    /**
     *
     * @param  string $table
     *
     * @return null
     *
     * @throws \InvalidArgumentException
     */
    protected function setTable($table)
    {
        if (!$table) {
            throw new \InvalidArgumentException('Table name required');
        }
        
        $this->table = $table;
    }
}