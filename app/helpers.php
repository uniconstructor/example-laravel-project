<?php

/**
 * Вспомогательные функции проекта, не входящие в состав Laravel
 * Прежде чем добавлять сюда новые хелперы убедитесь что их нет в Laravel
 *
 * @see https://laravel.com/docs/5.3/helpers#available-methods (стандартные хелперы)
 * @see https://laravel.com/docs/5.3/collections#available-methods (хелперы для коллекций)
 *
 * Как правильно добавлять собственные хелперы в Laravel:
 * @see http://stackoverflow.com/questions/28290332/best-practices-for-custom-helpers-on-laravel-5
 */

use Carbon\Carbon;
use Jenssegers\Date\Date;
use Illuminate\Support\Debug\Dumper;
use Illuminate\Database\Query\Builder;

if (!function_exists('local_date')) {
    /**
     * Форматирует дату с учетом текущей локали (языка), с правильным склонением месяцев
     * а также с учетом указанной временной зоны
     *
     * С русским языком опять все непросто: никак не помогает явно указанная русская локаль,
     * причем указанная одновременно, в нескольких местах:
     * - в настройках PHP (php.ini),
     * - в настройках Laravel (config/app.php + переменные окружения),
     * - в настройках Carbon (как глобально при инициализации приложения так и локально перед выводом данных),
     * - в настройках пакета Jenssegers\Date (на базе Carbon)
     * Поэтому пришлось создать этот хелпер
     *
     * @param DateTime|Carbon|Date|string $date       - дата для форматирования
     * @param string                      $format     - формат даты и времени (см. синтаксис date() в PHP)
     * @param string                      $timezone   - временная зона (если нужно задать принудительно)
     *                                                по умолчанию используется зона из настроек php.ini
     *
     * @return string
     *
     * ВНИМАНИЕ: не добавляйте сюда другие хелперы для работы с русским языком, скорее всего решение для
     * вашей задачи уже есть в этм наборе директив - он великолепен:
     * @see https://github.com/wapmorgan/Morphos-Blade
     */
    function local_date($date, $format = 'Y-m-d', $timezone = null)
    {
        if (\is_object($date)) {
            if ($timezone) {
                $date->setTimezone($timezone);
            }
            $dateString = $date->format($format);
        } else {
            $dateString = $date;
        }
        
        $rules        = [
            "/(Январь|january)/i"     => "января",
            "/(Февраль|february)/i"   => "февраля",
            "/(Март|march)/i"         => "марта",
            "/(Апрель|april)/i"       => "апреля",
            "/(Май|may)/i"            => "мая",
            "/(Июнь|jun|june)/i"      => "июня",
            "/(Июль|jul|july)/i"      => "июля",
            "/(Август|august)/i"      => "августа",
            "/(Сентябрь|september)/i" => "сентября",
            "/(Октябрь|october)/i"    => "октября",
            "/(Ноябрь|november)/i"    => "ноября",
            "/(Декабрь|december)/i"   => "декабря",
        ];
        $patterns     = \array_keys($rules);
        $replacements = \array_values($rules);
        
        return \preg_replace($patterns, $replacements, $dateString);
    }
} else {
    throw new \App\Exceptions\HelperNameConflictException('local_date() helper is already registered');
}

if (!function_exists('d')) {
    /**
     * Выводит переменную для отладки не прерывая выполнение php-запроса
     * Полный аналог стандартного хелпера dd() за исключением того что
     * не вызывает die(1) после вывода значения переменной
     * При отладке blade-шаблонов вызов dd() часто ломает верстку страницы и иногда делает
     * невозможным просмотр результатов дампа, поэтому
     *
     * @param  mixed
     *
     * @return void
     */
    function d()
    {
        \array_map(function ($x) {
            (new Dumper)->dump($x);
        }, \func_get_args());
    }
} else {
    throw new \App\Exceptions\HelperNameConflictException('d() helper is already registered');
}

if (!function_exists('icon')) {
    /**
     * Создать тег с иконкой Font Awesome
     *
     * @param string $icon       - название иконки в Font Awesome
     * @param array  $attributes - дополнительные параметры тега
     *
     * @return string - тег иконки
     *
     * @see https://laracasts.com/discuss/channels/laravel/useful-blade-directives/replies/290476
     */
    function icon($icon, array $attributes = [])
    {
        $class = "fa {$icon}";
        if (isset($attributes['class'])) {
            $attributes['class'] = \trim($attributes['class']) . ' ' . $class;
        } else {
            $attributes['class'] = $class;
        }
        
        return \Html::tag('i', '', $attributes)->toHtml();
    }
} else {
    throw new \App\Exceptions\HelperNameConflictException('icon() helper is already registered');
}

if (!function_exists('add_clause')) {
    /**
     * Добавить новое условие в объект Query Builder
     *
     * Примеры использования:
     * add_clause($query, 'active');
     * add_clause($query, 'type', ['car']);
     * add_clause($query, 'where', ['name', '==', 'car']);
     * add_clause($query, 'whereIn', ['id', [1, 3, 5, 9]]);
     * add_clause($query, 'whereName', ['car']);
     * add_clause($query, 'whereHas', ['posts', function($q) {
     *     $q->activePosts();
     * }]);
     *
     * @param Builder $query  - запрос для дополнения
     * @param string  $method - вызываемый метод
     * @param array   $params - передаваемые аргументы
     *
     * @return Builder
     *
     * @throws \BadMethodCallException
     */
    function add_clause(Builder $query, $method, array $params = [])
    {
        return $query->__call($method, $params);
    }
} else {
    throw new \App\Exceptions\HelperNameConflictException('add_clause() helper is already registered');
}

if (!function_exists('module_path')) {
    /**
     * Получить путь к модулю Laravel
     *
     * @param string $name - название модуля
     *
     * @return string
     *
     * @see https://github.com/nwidart/laravel-modules
     */
    function module_path($name)
    {
        return Module::getModulePath($name);
    }
} else {
    throw new \App\Exceptions\HelperNameConflictException('module_path() helper is already registered');
}

if (!function_exists('template')) {
    /**
     * Подставить данные в строку используя шаблонизатор Mustache
     *
     * @param string $template - строка с шаблоном для подстановки
     * @param array  $data     - массив с данными
     *
     * @return string
     *
     * @see https://github.com/bobthecow/mustache.php/wiki/Home
     *
     * @throws \InvalidArgumentException
     */
    function template($template, array $data=[])
    {
        if (!$template) {
            throw new \InvalidArgumentException('template(): template string cannot be empty');
        }
        if (!cache()->has('Mustache_Engine')) {
            cache()->put('Mustache_Engine', new Mustache_Engine, 120);
        }
        /**
         * @var $mustache Mustache_Engine
         */
        $mustache = cache()->get('Mustache_Engine');
        
        return $mustache->render($template, $data);
    }
} else {
    throw new \App\Exceptions\HelperNameConflictException('template() helper is already registered');
}

if (!function_exists('is_image')) {
    /**
     * Определить, является ли файл изображением
     *
     * @param string $fileName - полный путь к файлу
     *
     * @return string
     *
     * @see http://php.net/manual/en/function.exif-imagetype.php
     */
    function is_image($fileName)
    {
        return !(exif_imagetype($fileName) === false);
    }
} else {
    throw new \App\Exceptions\HelperNameConflictException('is_image() helper is already registered');
}