<?php

namespace App\Models;

// эти классы не используются в коде, но подключены для того чтобы сократить блок комментариев модели
// пожалуйста не убирайте их, чтобы подсказака для IDE продолжала нормально работать
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

// base model class
use Illuminate\Database\Eloquent\Model;
// Eloquence
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Sofa\Eloquence\Metable;
use Sofa\Eloquence\Mutable;
use Sofa\Eloquence\Validable;
use Sofa\Eloquence\Contracts\CleansAttributes;
use Sofa\Eloquence\Contracts\Validable as ValidableContract;
// Model revisions
use Venturecraft\Revisionable\RevisionableTrait;
// soft deleting
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Тип регулярной задачи (например пройти техосмотр), производимой с автомобилем
 * Все типы задач могут быть назначены любому автомобилю
 *
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $pending_event
 * @property string $active_event
 * @property string $finish_event
 * @property string $expiring_event
 * @property string $expired_event
 * @property string $failed_event
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read string|null $mapping_for
 * @property-read \Sofa\Eloquence\Metable\AttributeBag $meta_attributes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Sofa\Eloquence\Metable\Attribute[] $metaAttributes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTaskType whereActiveEvent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTaskType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTaskType whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTaskType whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTaskType whereExpiredEvent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTaskType whereExpiringEvent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTaskType whereFailedEvent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTaskType whereFinishEvent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTaskType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTaskType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTaskType wherePendingEvent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTaskType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CarTaskType extends Model implements ValidableContract, CleansAttributes
{
    /**
     * @see https://github.com/jarektkaczyk/eloquence/wiki/Base
     */
    use Eloquence, Mappable, Metable, Mutable, Validable;
    /**
     * @see https://laravel.com/docs/5.3/eloquent#soft-deleting
     */
    use SoftDeletes;
    /**
     * @see https://github.com/VentureCraft/revisionable
     */
    use RevisionableTrait;
    
    /**
     * @var string
     */
    public $table = 'car_task_types';
    /**
     * @var array
     *
     * @see https://laravel.com/docs/5.3/eloquent#mass-assignment
     */
    public $fillable = [
        'name',
        'description',
        'pending_event',
        'active_event',
        'finish_event',
        'expiring_event',
        'expired_event',
        'failed_event',
    ];
    /**
     * @inheritdoc
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     *
     * @see https://laravel.com/docs/5.3/eloquent-mutators
     */
    protected $casts = [
        'id'             => 'integer',
        'name'           => 'string',
        'description'    => 'string',
        'pending_event'  => 'string',
        'active_event'   => 'string',
        'finish_event'   => 'string',
        'expiring_event' => 'string',
        'expires_event'  => 'string',
        'failed_event'   => 'string',
    ];
    /**
     * @inheritdoc
     */
    protected $appends = [];
    /**
     * @var bool - записывать операцию создания записи в историю изменений объекта
     *
     * @see https://github.com/VentureCraft/revisionable#storing-creations
     */
    protected $revisionCreationsEnabled = true;
    /**
     * @var array - список полей, исключенных из истории изменений
     *
     * @see https://github.com/VentureCraft/revisionable#more-control
     */
    protected $dontKeepRevisionOf = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * Model attribute mapping:
     * [
     *      // implicit relation mapping:
     *      'profile'           => ['first_name', 'last_name'],
     *      // explicit relation mapping:
     *      'picture'           => 'profile.picture_path',
     *      // simple alias
     *      'dev_friendly_name' => 'badlynamedcolumn',
     *      'alias'             => 'real_column'
     * ]
     *
     * @var array
     *
     * @see https://github.com/jarektkaczyk/eloquence/wiki/Mappable
     */
    protected $maps = [];
    
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];
    
    /**
     * @inheritdoc
     */
    public static function boot()
    {
        // обработка событий
        // действия перед созданием модели
        static::creating(
            function (CarTaskType $model) {
                return true;
            }
        );
        // действия перед сохранением модели
        static::saving(
            function (CarTaskType $model) {
                return true;
            }
        );
        // действия перед удалением модели
        static::deleting(
            function (CarTaskType $model) {
                return true;
            }
        );
        
        parent::boot();
    }
    
    /// RELATIONS ///
    
    /// SCOPES ///
    
    /// METHODS ///
}