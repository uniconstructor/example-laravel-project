<?php

namespace App\Models;

// эти классы не используются в коде, но подключены для того чтобы сократить блок комментариев модели
// пожалуйста не убирайте их, чтобы подсказака для IDE продолжала нормально работать
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

// base model class
use Illuminate\Database\Eloquent\Model;
// Eloquence
use Illuminate\Support\Str;
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Sofa\Eloquence\Metable;
use Sofa\Eloquence\Mutable;
use Sofa\Eloquence\Validable;
use Sofa\Eloquence\Contracts\CleansAttributes;
use Sofa\Eloquence\Contracts\Validable as ValidableContract;
// Model revisions
use Venturecraft\Revisionable\RevisionableTrait;
// soft deleting
use Illuminate\Database\Eloquent\SoftDeletes;
// загрузка файлов
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
// автогенерация читаемых ID
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
// file API
use Illuminate\Support\Facades\Storage;
// validation API
use Illuminate\Validation\Rule;
// события для оповещений
use App\Events\Car\DocumentExpires;
use App\Events\Car\DocumentExpired;
use App\Events\Car\KaskoExpires;
use App\Events\Car\KaskoExpired;
use App\Events\Car\LastMaintenanceUpdated;
use App\Events\Car\LastOilRefreshUpdated;
use App\Events\Car\MileageAdded;
use App\Events\Car\OsagoExpires;
use App\Events\Car\OsagoExpired;

/**
 * Автомобиль
 *
 * @SWG\Definition (
 *      definition="Car",
 *      required={"vendor", "model", "reg_number"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="iflat_user_id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="department_id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="vendor",
 *          description="vendor",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="model",
 *          description="model",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="reg_number",
 *          description="reg_number",
 *          type="string"
 *      ),
 *     @SWG\Property(
 *          property="slug",
 *          description="slug",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 * @property int                                                                                 $id
 * @property int                                                                                 $iflat_user_id
 * @property int                                                                                 $department_id
 * @property string                                                                              $vendor
 * @property string                                                                              $model
 * @property string                                                                              $reg_number
 * @property string                                                                              $description
 * @property string                                                                              $status
 * @property \Carbon\Carbon                                                                      $created_at
 * @property \Carbon\Carbon                                                                      $updated_at
 * @property \Carbon\Carbon                                                                      $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereModel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereRegNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereVendor($value)
 * @property-read \App\Models\Department                                                         $department
 * @property-read \App\Models\IflatUser                                                          $iflatUser
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereDepartmentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereIflatUserId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Document[]                $documents
 * @property-read string|null                                                                    $mapping_for
 * @property-read \Sofa\Eloquence\Metable\AttributeBag                                           $meta_attributes
 * @property-read string                                                                         $status_text
 * @property-read \Illuminate\Database\Eloquent\Collection|\Sofa\Eloquence\Metable\Attribute[]   $metaAttributes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Media[]          $media
 * @property string                                                                              $slug
 * @property-read string                                                                         $full_name
 * @property-read string                                                                         $name
 * @property-read int                                                                            $reg_number_digits
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereSlug($value)
 * @property int                                                                                 $mileage
 * @property int
 *           $maintenance_notification_margin
 * @property int
 *           $oil_notification_margin
 * @property int                                                                                 $last_oil_refresh
 * @property int                                                                                 $last_maintenance
 * @property int                                                                                 $oil_refresh_interval
 * @property int                                                                                 $maintenance_interval
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereLastMaintenance($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereLastOilRefresh($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereMaintenanceInterval($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereMaintenanceNotificationMargin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereMileage($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereOilNotificationMargin($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Car whereOilRefreshInterval($value)
 */
class Car extends Model implements ValidableContract, CleansAttributes, HasMedia, HasMediaConversions
{
    /**
     * @see https://github.com/jarektkaczyk/eloquence/wiki/Base
     */
    use Eloquence, Mappable, Metable, Mutable, Validable;
    /**
     * @see https://laravel.com/docs/5.3/eloquent#soft-deleting
     */
    use SoftDeletes;
    /**
     * @see https://github.com/VentureCraft/revisionable
     */
    use RevisionableTrait;
    /**
     * @see https://docs.spatie.be/laravel-medialibrary/v4/basic-usage/preparing-your-model
     */
    use HasMediaTrait;
    /**
     * @see https://github.com/spatie/laravel-sluggable
     */
    use HasSlug;
    
    /**
     * @var string
     */
    public $table = 'cars';
    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * @var array
     */
    public $fillable = [
        'department_id',
        'iflat_user_id',
        'vendor',
        'model',
        'reg_number',
        'slug',
        'description',
        'status',
        // параметры пробега автомобиля
        'mileage',
        'maintenance_interval',
        'oil_refresh_interval',
        'last_maintenance',
        'last_oil_refresh',
        'oil_notification_margin',
        'maintenance_notification_margin',
    ];
    /**
     * @var array
     */
    public $appends = [
        'status_text',
        'department_name',
        'iflat_user_name',
        'name',
        'full_name',
    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                              => 'integer',
        'department_id'                   => 'integer',
        'iflat_user_id'                   => 'integer',
        'vendor'                          => 'string',
        'model'                           => 'string',
        'reg_number'                      => 'string',
        'slug'                            => 'string',
        'description'                     => 'string',
        'status'                          => 'string',
        'mileage'                         => 'integer',
        'maintenance_interval'            => 'integer',
        'oil_refresh_interval'            => 'integer',
        'last_maintenance'                => 'integer',
        'last_oil_refresh'                => 'integer',
        'oil_notification_margin'         => 'integer',
        'maintenance_notification_margin' => 'integer',
        // динамические поля
        'department_name'                 => 'string',
        'iflat_user_name'                 => 'string',
        'full_name'                       => 'string',
        'reg_number_digits'               => 'string',
    ];
    /**
     * @var bool - записывать операцию создания записи в историю изменений объекта
     *
     * @see https://github.com/VentureCraft/revisionable#storing-creations
     */
    protected $revisionCreationsEnabled = true;
    /**
     * @var array - список полей, исключенных из истории изменений
     *
     * @see https://github.com/VentureCraft/revisionable#more-control
     */
    protected $dontKeepRevisionOf = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * Model attribute mapping:
     * [
     *      // implicit relation mapping:
     *      'profile'           => ['first_name', 'last_name'],
     *      // explicit relation mapping:
     *      'picture'           => 'profile.picture_path',
     *      // simple alias
     *      'dev_friendly_name' => 'badlynamedcolumn',
     *      'alias'             => 'real_column'
     * ]
     *
     * @var array
     *
     * @see https://github.com/jarektkaczyk/eloquence/wiki/Mappable
     */
    protected $maps = [
        'department_name' => 'department.name',
        'iflat_user_name' => 'iflatUser.full_name',
    ];
    
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'vendor'     => 'required',
        'model'      => 'required',
        'reg_number' => [
            'required',
            'min:8',
            'max:9',
        ],
    ];
    /**
     * Validation rules (create only)
     *
     * @var array
     */
    public static $createRules = [
        'vendor'     => 'required',
        'model'      => 'required',
        'reg_number' => [
            'required',
            'unique:cars,reg_number',
            'min:8',
            'max:9',
        ],
    ];
    /**
     * @var array
     */
    public static $statusList = [
        'draft'     => 'Черновик',
        'active'    => 'Исправен',
        'repairing' => 'В ремонте',
    ];
    
    /**
     * @inheritdoc
     */
    public static function boot()
    {
        // обработка событий
        // действия перед сохранением (созданием и редактированием) модели
        static::saving(
            function (Car $model) {
                // создаем строковый идентификатор автомобиля
                // (нужен для создания имени папки с файлами автомобиля)
                if (!$model->slug) {
                    $model->generateSlug();
                }
                // создаем/обновляем структуру папок для хранения загруженных файлов автомобиля (если ее нет)
                if ($model->exists) {
                    $model->prepareFolders();
                }
                if (!empty(Car::$statusList[$model->status])) {
                    $model->setMeta('status_text', Car::$statusList[$model->status]);
                } else {
                    $model->setMeta('status_text', '');
                }
                // безопасная обработка null-значений для числовых параметров
                $numberParams = [
                    'mileage',
                    'maintenance_interval',
                    'oil_refresh_interval',
                    'last_maintenance',
                    'last_oil_refresh',
                    'oil_notification_margin',
                    'maintenance_notification_margin',
                ];
                foreach ($numberParams as $numberParam) {
                    if (empty(trim($model->{$numberParam}))) {
                        $model->{$numberParam} = 0;
                    }
                }
                
                return true;
            }
        );
        // действия после обновления модели
        static::updated(
            function (Car $model) {
                // оповещения о ТО и масле по пробегу
                if ($model->mileage > $model->getOriginal('mileage')) {
                    event(new MileageAdded($model));
                }
                // произведено ТО
                if ($model->last_maintenance > $model->getOriginal('last_maintenance')) {
                    if ($model->mileage < $model->last_maintenance) {
                        $model->mileage = $model->last_maintenance;
                    }
                    event(new LastMaintenanceUpdated($model));
                }
                // произведена замена масла
                if ($model->last_oil_refresh > $model->getOriginal('mileage')) {
                    if ($model->mileage < $model->last_oil_refresh) {
                        $model->mileage = $model->last_oil_refresh;
                    }
                    event(new LastOilRefreshUpdated($model));
                }
            }
        );
        // действия после сохранения модели
        static::created(
            function (Car $model) {
                // создаем структуру папок для хранения загруженных файлов автомобиля
                $model->prepareFolders();
            }
        );
        // действия перед удалением модели
        static::deleting(
            function (Car $model) {
                // удаляем все документы автомобиля перед тем как удалить автомобиль
                $model->documents->each(
                    function ($document) {
                        /** @var \App\Models\Document $document */
                        $document->delete();
                    }
                );
                
                return true;
            }
        );
        
        parent::boot();
    }
    
    /// RELATIONS ///
    
    /**
     * Подразделение за которым закреплен автомобиль
     *
     * @return BelongsTo|Department
     */
    public function department()
    {
        return $this->belongsTo(Department::class);
    }
    
    /**
     * Пользователь за которым закреплен автомобиль
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|IflatUser
     */
    public function iflatUser()
    {
        return $this->belongsTo(IflatUser::class);
    }
    
    /**
     * Пользователь за которым закреплен автомобиль
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|IflatUser
     */
    public function employee()
    {
        return $this->iflatUser();
    }
    
    /**
     * Документы прикрепленные к этому автомобилю
     *
     * @return HasMany|Document
     */
    public function documents()
    {
        return $this->hasMany(Document::class);
    }
    
    /// GETTERS & SETTERS ///
    
    /**
     * Получить название статуса по его коду
     *
     * @return string
     */
    public function getStatusTextAttribute()
    {
        if ($this->hasMeta('status_text')) {
            return $this->getMeta('status_text');
        }
        if (!empty(Car::$statusList[$this->status])) {
            return Car::$statusList[$this->status];
        }
        
        return $this->status;
    }
    
    /**
     * Получить название автомобиля
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return $this->vendor . ' ' . $this->model;
    }
    
    /**
     * Получить название автомобиля + его номер
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->vendor . ' ' . $this->model . ' [' . $this->reg_number . ']';
    }
    
    /**
     * @param string $regNumber
     *
     * @return void
     */
    public function setRegNumberAttribute($regNumber)
    {
        $regNumber = mb_strtolower($regNumber);
        $regNumber = trim(str_replace(' ', '', $regNumber));
        
        $this->attributes['reg_number'] = $regNumber;
    }
    
    /**
     * Получить только первые 3 цифры из номера автомобиля
     *
     * @return int|string
     */
    public function getRegNumberDigitsAttribute()
    {
        return str_limit(preg_replace('/\D/', '', str_slug($this->reg_number, '')), 3, '');
    }
    
    /// METHODS ///
    
    /**
     * Преобразования для загруженных файлов
     *
     * @see https://docs.spatie.be/laravel-medialibrary/v4/converting-images/defining-conversions
     *
     * @return null
     *
     * @throws \Spatie\MediaLibrary\Exceptions\InvalidConversionParameter
     */
    public function registerMediaConversions()
    {
        // маленькие иконки изображений (для предпросмотра)
        $this->addMediaConversion('w_150')
            //->setManipulations(['w' => 150])
            ->setWidth(150)
            ->performOnCollections('photo')
            ->nonQueued();
        
        // средние изображения (для inline-галереи) 
        $this->addMediaConversion('w_500')
            //->setManipulations(['w' => 500])
            ->setWidth(500)
            ->performOnCollections('photo')
            ->nonQueued();
        
        // большие изображения (полный экран)
        $this->addMediaConversion('w_1000')
            //->setManipulations(['w' => 1000])
            ->setWidth(1000)
            ->performOnCollections('photo')
            ->nonQueued();
    }
    
    /**
     * Get the options for generating the slug.
     *
     * @return SlugOptions
     *
     * @see https://github.com/spatie/laravel-sluggable
     */
    public function getSlugOptions()
    {
        return SlugOptions::create()
            ->generateSlugsFrom(['vendor', 'model', 'reg_number_digits'])
            ->saveSlugsTo('slug')
            ->usingSeparator('_');
    }
    
    /**
     * Все типы документов, загруженные для этого автомобиля
     *
     * @return Collection<DocumentType>
     */
    public function getUploadedDocumentTypes()
    {
        $carId = $this->id;
        
        return DocumentType::whereHas(
            'documents', function ($query) use ($carId) {
            /** @var Builder $query */
            $query->where('car_id', '=', $carId);
        }
        )->latest()->get();
    }
    
    /**
     * Определить есть ли для этого автомобиля хотя бы один документ указанного типа
     *
     * @param string $slug
     *
     * @return bool
     */
    public function hasDocumentType($slug)
    {
        return $this->getUploadedDocumentTypes()->keyBy('slug')->has($slug);
    }
    
    /**
     * Подсчитать количество документов указанного типа прикрепленных к этому автомобилю
     *
     * @param DocumentType $documentType - тип документа, для которого нужно подсчитать количество
     *
     * @return int
     */
    public function getDocumentCount(DocumentType $documentType)
    {
        return $this->documents()->ofType($documentType)->count();
    }
    
    /**
     * Подсчитать общее количество документов прикрепленных к этому автомобилю
     *
     * @return int
     */
    public function getTotalDocumentCount()
    {
        return $this->documents()->count();
    }
    
    /**
     * Получить последний созданный документ указанного типа, созданный для этого автомобиля
     *
     * @param DocumentType $documentType
     *
     * @return Document|null
     */
    public function getLastDocument(DocumentType $documentType)
    {
        return $this->documents()->ofType($documentType)->latest()->first();
    }
    
    /**
     * Получить имя папки со всеми файлами автомобиля
     *
     * @return string
     */
    public function getFolderName()
    {
        return $this->slug;
    }
    
    /**
     * Получить относительный путь к папке с файлами автомобиля
     *
     * @return string
     */
    public function getRootFolderPath()
    {
        return 'public/cars/' . $this->getFolderName() . '/';
    }
    
    /**
     * Получить префикс для url-ссылок на фото автомобиля
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getRootFolderUrl()
    {
        return url('/storage/cars/' . $this->getFolderName()) . '/';
    }
    
    /**
     * Получить путь к папке с фото автомобиля
     *
     * @return string
     */
    public function getPhotoFolderPath()
    {
        return $this->getRootFolderPath() . 'photo/';
    }
    
    /**
     * Получить url-префикс для папки с фото автомобиля
     *
     * @return string
     */
    public function getPhotoFolderUrl()
    {
        return $this->getRootFolderUrl() . 'photo/';
    }
    
    /**
     * Получить полный путь к загрузке фото для автомобиля (папка + имя файла)
     *
     * @param UploadedFile $file
     *
     * @return string
     */
    public function getPhotoUploadPath(UploadedFile $file)
    {
        return $this->getPhotoFolderPath() . $this->getPhotoFileName($file);
    }
    
    /**
     * Получить путь к папке с документами указанного типа
     *
     * @param DocumentType $documentType
     *
     * @return string
     */
    public function getDocumentTypeFolderPath(DocumentType $documentType)
    {
        return $this->getRootFolderPath() . 'documents/' . $documentType->slug . '/';
    }
    
    /**
     * Получить url-префикс для папки с документами указанного типа
     *
     * @param DocumentType $documentType
     *
     * @return string
     */
    public function getDocumentTypeFolderUrl(DocumentType $documentType)
    {
        return $this->getRootFolderUrl() . 'documents/' . $documentType->slug . '/';
    }
    
    /**
     * Получить полный путь к папке с файлами документа
     *
     * @param Document $document
     *
     * @return string
     */
    public function getDocumentFolderPath(Document $document)
    {
        return $this->getDocumentTypeFolderPath($document->documentType) . $document->id . '/';
    }
    
    /**
     * Получить Url к папке с файлами документа
     *
     * @param Document $document
     *
     * @return string
     */
    public function getDocumentFolderUrl(Document $document)
    {
        return $this->getDocumentTypeFolderUrl($document->documentType) . $document->id . '/';
    }
    
    /**
     * Получить полный путь к загрузке файла документа для автомобиля (папка + имя файла)
     *
     * @param Document     $document
     * @param UploadedFile $file
     *
     * @return string
     */
    public function getDocumentUploadPath(Document $document, UploadedFile $file)
    {
        return $this->getDocumentFolderPath($document) . $this->getDocumentFileName($document, $file);
    }
    
    /**
     * Получить название файла с фотографией
     *
     * @param UploadedFile $file
     *
     * @return string
     */
    public function getPhotoFileName(UploadedFile $file)
    {
        return 'photo_' . $this->slug . '_' . str_random('4') . '.' . $file->extension();
    }
    
    /**
     * Получить имя файла для загруженного документа
     *
     * @param Document     $document
     * @param UploadedFile $file
     *
     * @return string
     */
    public function getDocumentFileName(Document $document, UploadedFile $file)
    {
        return $document->getFileName($file);
    }
    
    /**
     * Подготовить структуру папок для загрузки файлов для этого автомобиля
     *
     * @return void
     */
    public function prepareFolders()
    {
        $documentTypes = DocumentType::all();
        $folders       = [
            $this->getRootFolderPath(),
            $this->getPhotoFolderPath(),
        ];
        foreach ($documentTypes as $documentType) {
            $folders[] = $this->getDocumentTypeFolderPath($documentType);
        }
        
        foreach ($folders as $folder) {
            if (!Storage::exists($folder)) {
                Storage::makeDirectory($folder);
                Storage::setVisibility($folder, 'public');
            }
        }
    }
    
    /**
     * Получить все загруженные фотографии
     *
     * @return \Illuminate\Support\Collection
     */
    public function getPhoto()
    {
        return $this->getMedia('photo');
    }
    
    /**
     * Получить количество загруженных фотографий
     *
     * @return int
     */
    public function countPhoto()
    {
        return $this->getMedia('photo')->count();
    }
    
    /**
     * Определить есть ли у автомобиля хотя бы одна загруженная фотография
     *
     * @return bool
     */
    public function hasPhoto()
    {
        return ($this->countPhoto() > 0);
    }
    
    /**
     * Получить массив со списком фотографий автомобиля для формирования галереи
     * изображений плагином photoswipe
     *
     * @return string - список фотографий автомобиля в формате JSON
     *
     * @see http://photoswipe.com/documentation/custom-html-in-slides.html
     */
    public function getSwipeGalleryItems()
    {
        $items = [];
        if (!$this->hasPhoto()) {
            return json_encode([]);
        }
        
        foreach ($this->getPhoto() as $photo) {
            $items[] = [
                'src' => "/media/{$photo->id}/view",
                'w'   => $photo->getCustomProperty('width'),
                'h'   => $photo->getCustomProperty('height'),
            ];
        }
        
        return json_encode($items);
    }
    
    /**
     * Сохранить загруженное фото автомобиля и привязать его к автомобилю
     *
     * @param UploadedFile $file
     *
     * @return \Spatie\MediaLibrary\Media
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     * @throws \Spatie\MediaLibrary\FileAdder\FileDoesNotExist
     * @throws \LogicException
     */
    public function uploadPhotoFile(UploadedFile $file)
    {
        // загружаем файл в папку с файлами автомобиля
        $folder = $this->getPhotoFolderPath();
        $name   = $this->getPhotoFileName($file);
        // дополнительные параметры файла
        $props = [
            'collection'        => 'photo',
            'localName'         => 'Фото ' . $this->full_name,
            'originalName'      => $file->getClientOriginalName(),
            'originalExtension' => $file->getClientOriginalExtension(),
            'mimeType'          => $file->getMimeType(),
            'carId'             => $this->id,
            'isImage'           => false,
        ];
        // определяем параметры изображения
        if (is_image($file->getPath() . '/' . $file->getFilename())) {
            $imageInfo        = getimagesize($file->getPath() . '/' . $file->getFilename());
            $props['isImage'] = true;
            $props['width']   = $imageInfo[0];
            $props['height']  = $imageInfo[1];
        }
        // $path   = $file->storeAs($folder, $name);
        $file->storeAs($folder, $name);
        
        return $this->addMedia($file)
            ->usingFileName($name)
            ->withCustomProperties($props)
            ->toCollection('photo');
    }
    
    /**
     * Сохранить загруженный документ и привязать его к автомобилю
     *
     * @param Document     $document
     * @param UploadedFile $file
     *
     * @return \Spatie\MediaLibrary\Media
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function uploadDocumentFile(Document $document, UploadedFile $file)
    {
        return $document->uploadFile($file);
    }
    
    
    public function isOverMaintenanceMargin()
    {
        if (!$this->maintenance_notification_margin) {
            return false;
        }
        $limit = ($this->last_maintenance + $this->maintenance_interval) - $this->maintenance_notification_margin;
        
        return ($this->mileage > $limit);
    }
    
    
    public function isOverMaintenanceInterval()
    {
        if (!$this->maintenance_interval) {
            return false;
        }
        $limit = $this->last_maintenance + $this->maintenance_interval;
    
        return ($this->mileage > $limit);
    }
    
    
    public function isOverOilRefreshMargin()
    {
        if (!$this->oil_notification_margin) {
            return false;
        }
        $limit = ($this->last_oil_refresh + $this->oil_refresh_interval) - $this->oil_notification_margin;
    
        return ($this->mileage > $limit);
    }
    
    
    public function isOverOilRefreshInterval()
    {
        if (!$this->oil_refresh_interval) {
            return false;
        }
        $limit = $this->last_oil_refresh + $this->oil_refresh_interval;
    
        return ($this->mileage > $limit);
    }
    
    /**
     * Определить, оповещен ли сотрудник о событии автомобиля
     *
     * @param $type
     *
     * @return bool
     */
    public function isNotifiedAbout($type)
    {
        $key = $this->getNotificationStatusKey($type);

        // Оповещаем каждый раз, позже посмотрим что с этим делать
        // return ($this->hasMeta($key) && $this->getMeta($key));
        return false;
    }
    
    /**
     * Пометить что для этого автомобиля сотруднику уже было отправлено оповещение
     *
     * @param $type
     *
     * @return bool
     */
    public function markNotifiedAbout($type)
    {
        $key = $this->getNotificationStatusKey($type);
        
        $this->setMeta($key, true);
        
        return $this->save();
    }
    
    /**
     * Очистить метку оповещения о событии
     *
     * @param $type
     *
     * @return bool
     */
    public function clearNotificationAbout($type)
    {
        $key = $this->getNotificationStatusKey($type);
        
        $this->setMeta($key, false);
        
        return $this->save();
    }
    
    
    public function getNotificationStatusKey($type)
    {
        return 'has' . $type . 'Notification';
    }
}
