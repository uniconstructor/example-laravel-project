<?php

namespace App\Models;

// эти классы не используются в коде, но подключены для того чтобы сократить блок комментариев модели
// пожалуйста не убирайте их, чтобы подсказака для IDE продолжала нормально работать
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

// base model class
use Illuminate\Database\Eloquent\Model;
// Eloquence
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Sofa\Eloquence\Metable;
use Sofa\Eloquence\Mutable;
use Sofa\Eloquence\Validable;
use Sofa\Eloquence\Contracts\CleansAttributes;
use Sofa\Eloquence\Contracts\Validable as ValidableContract;
// Model revisions
use Venturecraft\Revisionable\RevisionableTrait;
// soft deleting
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Тип стандартного оповещения о каком-либо событии, которое произошло с автомобилем, например:
 * - изменен статус автомобиля
 * - автомобиль закреплен за новым сотрудником
 * - выполнена плановая задача связанная с автомобилем
 * - требует выполнения новая задача связанная с автомобилем (истекает техосмотр или страховка)
 *
 * Тип оповещения определяет то каким событием оно будет активировано
 * Текст каждого оповещения можно редактировать
 * Для отправки всех оповещений этой модели используется общий класс App\Notifications\CarNotification
 *
 * Типы оповещений можно создавать вручную из админки
 *
 * @package App\Models
 * @property int                                                                                 $id
 * @property string                                                                              $name
 * @property string                                                                              $description
 * @property string                                                                              $level
 * @property string                                                                              $trigger_event
 * @property bool                                                                                $send_email
 * @property bool                                                                                $send_sms
 * @property string                                                                              $subject_template
 * @property string                                                                              $action_text_template
 * @property string                                                                              $action_url_template
 * @property string                                                                              $email_template
 * @property string                                                                              $sms_template
 * @property string                                                                              $slack_template
 * @property bool                                                                                $notify_car_user
 * @property bool                                                                                $notify_admin_user
 * @property \Carbon\Carbon                                                                      $created_at
 * @property \Carbon\Carbon                                                                      $updated_at
 * @property \Carbon\Carbon                                                                      $deleted_at
 * @property-read string|null                                                                    $mapping_for
 * @property-read \Sofa\Eloquence\Metable\AttributeBag                                           $meta_attributes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Sofa\Eloquence\Metable\Attribute[]   $metaAttributes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType ofEvent($event)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereActionTextTemplate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereActionUrlTemplate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereEmailTemplate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereLevel($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereNotifyAdminUser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereNotifyCarUser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereSendEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereSendSms($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereSlackTemplate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereSmsTemplate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereSubjectTemplate($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereTriggerEvent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarNotificationType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CarNotificationType extends Model implements ValidableContract, CleansAttributes
{
    /**
     * @see https://github.com/jarektkaczyk/eloquence/wiki/Base
     */
    use Eloquence, Mappable, Metable, Mutable, Validable;
    /**
     * @see https://laravel.com/docs/5.3/eloquent#soft-deleting
     */
    use SoftDeletes;
    /**
     * @see https://github.com/VentureCraft/revisionable
     */
    use RevisionableTrait;
    
    public static $eventTypes = [
        /*\App\Events\Car\DocumentExpired::class => [
            'title'       => 'Истек срок действия документа',
            'description' => '',
        ],
        \App\Events\Car\DocumentExpires::class => [
            'title'       => 'Истекает срок действия документа',
            'description' => '',
        ],*/
        \App\Events\Car\KaskoExpirationNotice::class => [
            'title'       => 'Запуск оповещения об истечении срока действия КАСКО',
            'description' => '',
        ],
        \App\Events\Car\OsagoExpirationNotice::class => [
            'title'       => 'Запуск оповещения об истечении срока действия ОСАГО',
            'description' => '',
        ],
        \App\Events\Car\KaskoExpires::class => [
            'title'       => 'Истекает срок действия КАСКО',
            'description' => '',
        ],
        \App\Events\Car\LastMaintenanceUpdated::class => [
            'title'       => 'Пройдено ТО',
            'description' => '',
        ],
        \App\Events\Car\LastOilRefreshUpdated::class => [
            'title'       => 'Произведена замена масла',
            'description' => '',
        ],
        \App\Events\Car\MaintenanceMileageNotice::class => [
            'title'       => 'Запуск оповещения о прохождении ТО',
            'description' => '',
        ],
        \App\Events\Car\OilRefreshMileageNotice::class => [
            'title'       => 'Запуск оповещения о необходимости замены масла',
            'description' => '',
        ],
        \App\Events\Car\MileageAdded::class => [
            'title'       => 'Пробег автомобиля увеличен',
            'description' => '',
        ],
        \App\Events\Car\MileageOverMaintenanceInterval::class => [
            'title'       => 'Пробег автомобиля превысил интервал ТО',
            'description' => '',
        ],
        \App\Events\Car\MileageOverMaintenanceMargin::class => [
            'title'       => 'Пробег автомобиля скоро потребует потребует прохождения ТО',
            'description' => '',
        ],
        \App\Events\Car\MileageOverOilRefreshInterval::class => [
            'title'       => 'Пробег автомобиля превысил интервал замены масла',
            'description' => '',
        ],
        \App\Events\Car\MileageOverOilRefreshMargin::class => [
            'title'       => 'Пробег автомобиля скоро потребует замены масла',
            'description' => '',
        ],
        
    ];
    
    /**
     * @var string
     */
    public $table = 'car_notification_types';
    /**
     * @var array
     *
     * @see https://laravel.com/docs/5.3/eloquent#mass-assignment
     */
    public $fillable = [
        'name',
        'description',
        'level',
        'trigger_event',
        'send_email',
        'send_sms',
        'subject_template',
        'action_text_template',
        'action_url_template',
        'email_template',
        'sms_template',
        'slack_template',
        'notify_car_user',
        'notify_admin_user',
    ];
    
    /**
     * @inheritdoc
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     *
     * @see https://laravel.com/docs/5.3/eloquent-mutators
     */
    protected $casts = [
        'id'                   => 'integer',
        'name'                 => 'string',
        'description'          => 'string',
        'level'                => 'string',
        'trigger_event'        => 'string',
        'send_email'           => 'boolean',
        'send_sms'             => 'boolean',
        'subject_template'     => 'string',
        'action_text_template' => 'string',
        'action_url_template'  => 'string',
        'email_template'       => 'string',
        'sms_template'         => 'string',
        'slack_template'       => 'string',
        'notify_car_user'      => 'boolean',
        'notify_admin_user'    => 'boolean',
    ];
    /**
     * @inheritdoc
     */
    protected $appends = [
        'trigger_event_name'
    ];
    /**
     * @var bool - записывать операцию создания записи в историю изменений объекта
     *
     * @see https://github.com/VentureCraft/revisionable#storing-creations
     */
    protected $revisionCreationsEnabled = true;
    /**
     * @var array - список полей, исключенных из истории изменений
     *
     * @see https://github.com/VentureCraft/revisionable#more-control
     */
    protected $dontKeepRevisionOf = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * Model attribute mapping:
     * [
     *      // implicit relation mapping:
     *      'profile'           => ['first_name', 'last_name'],
     *      // explicit relation mapping:
     *      'picture'           => 'profile.picture_path',
     *      // simple alias
     *      'dev_friendly_name' => 'badlynamedcolumn',
     *      'alias'             => 'real_column'
     * ]
     *
     * @var array
     *
     * @see https://github.com/jarektkaczyk/eloquence/wiki/Mappable
     */
    protected $maps = [];
    
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => [
            'required',
        ],
    ];
    /**
     * Validation rules (create only)
     *
     * @var array
     */
    public static $createRules = [
        'name' => [
            'required',
            'unique:car_notification_types,name',
        ],
    ];
    
    /**
     * @inheritdoc
     */
    public static function boot()
    {
        parent::boot();
    }
    
    /**
     * Получить список опций для select-элемента "событие"
     *
     * @return mixed
     */
    public static function getTriggerEventOptions()
    {
        return collect(self::$eventTypes)
            ->map(function ($item, $key) {
                return [
                    'key'   => $key,
                    'value' => $item['title'],
                ];
            })->mapWithKeys(function ($item) {
                return [$item['key'] => $item['value']];
            })->all();
    }
    
    /// RELATIONS ///
    
    
    
    /// SCOPES ///
    
    /**
     * Все типы оповещений, отправляемые в ответ на указанное событие
     *
     * @param Builder          $query
     * @param string|\stdClass $event - полное называние класса события или сам объект события
     *
     * @return Builder
     */
    public function scopeOfEvent($query, $event)
    {
        if (\is_object($event)) {
            $event = \get_class($event);
        }
        
        return $query->where('trigger_event', '=', $event);
    }
    
    /// METHODS ///
    
    
    
    /// GETTERS & SETTERS ///
    
    public function getTriggerEventNameAttribute()
    {
        if (!empty(self::$eventTypes[$this->trigger_event]['title'])) {
            return self::$eventTypes[$this->trigger_event]['title'];
        }
        
        return $this->trigger_event;
    }
}