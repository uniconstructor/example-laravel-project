<?php

namespace App\Models;

// эти классы не используются в коде, но подключены для того чтобы сократить блок комментариев модели
// пожалуйста не убирайте их, чтобы подсказака для IDE продолжала нормально работать
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

// base model class
use Illuminate\Database\Eloquent\Model;
// Eloquence
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Sofa\Eloquence\Metable;
use Sofa\Eloquence\Mutable;
use Sofa\Eloquence\Validable;
use Sofa\Eloquence\Contracts\CleansAttributes;
use Sofa\Eloquence\Contracts\Validable as ValidableContract;
// Model revisions
use Venturecraft\Revisionable\RevisionableTrait;
// soft deleting
use Illuminate\Database\Eloquent\SoftDeletes;
// model notifications
use Illuminate\Notifications\Notifiable;

/**
 * Сотрудник iflat
 *
 * @SWG\Definition (
 *      definition="IflatUser",
 *      required={"firstname", "lastname"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="department_id",
 *          description="department_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="firstname",
 *          description="firstname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="lastname",
 *          description="lastname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="middlename",
 *          description="middlename",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="position",
 *          description="position",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 * @property int                         $id
 * @property int                         $department_id
 * @property string                      $firstname
 * @property string                      $lastname
 * @property string                      $middlename
 * @property string                      $email
 * @property string                      $phone
 * @property string                      $description
 * @property string                      $position
 * @property string                      $status
 * @property \Carbon\Carbon              $created_at
 * @property \Carbon\Carbon              $updated_at
 * @property \Carbon\Carbon              $deleted_at
 * @property-read Collection|Car[]       $cars
 * @property-read \App\Models\Department $department
 * @method static \Illuminate\Database\Query\Builder|\App\Models\IflatUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\IflatUser whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\IflatUser whereDepartmentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\IflatUser whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\IflatUser whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\IflatUser whereFirstname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\IflatUser whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\IflatUser whereLastname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\IflatUser whereMiddlename($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\IflatUser wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\IflatUser wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\IflatUser whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\IflatUser whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $full_name
 * @property-read string|null $mapping_for
 * @property-read \Sofa\Eloquence\Metable\AttributeBag $meta_attributes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Sofa\Eloquence\Metable\Attribute[] $metaAttributes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 */
class IflatUser extends Model implements ValidableContract, CleansAttributes
{
    /**
     * @see https://github.com/jarektkaczyk/eloquence/wiki/Base
     */
    use Eloquence, Mappable, Metable, Mutable, Validable;
    /**
     * @see https://laravel.com/docs/5.3/eloquent#soft-deleting
     */
    use SoftDeletes;
    /**
     * @see https://github.com/VentureCraft/revisionable
     */
    use RevisionableTrait;
    /**
     * @see https://laravel.com/docs/5.3/notifications#sending-notifications
     */
    use Notifiable;
    
    /**
     * @var string
     */
    public $table = 'iflat_users';
    
    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * @var array
     */
    public $fillable = [
        'department_id',
        'firstname',
        'lastname',
        'middlename',
        'email',
        'phone',
        'description',
        'position',
        'status',
    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'            => 'integer',
        'department_id' => 'integer',
        'firstname'     => 'string',
        'lastname'      => 'string',
        'middlename'    => 'string',
        'email'         => 'string',
        'phone'         => 'string',
        'description'   => 'string',
        'position'      => 'string',
        'status'        => 'string',
    ];
    /**
     * @var bool - записывать операцию создания записи в историю изменений объекта
     *
     * @see https://github.com/VentureCraft/revisionable#storing-creations
     */
    protected $revisionCreationsEnabled = true;
    /**
     * @var array - список полей, исключенных из истории изменений
     *
     * @see https://github.com/VentureCraft/revisionable#more-control
     */
    protected $dontKeepRevisionOf = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'firstname' => 'required',
        'lastname'  => 'required',
    ];
    
    /**
     * Route notifications for the Slack channel.
     *
     * @return string
     */
    public function routeNotificationForSlack()
    {
        return config('services.slack.webhook_url');
    }
    
    /// RELATIONS ///
    
    /**
     * Все автомобили закрепленные за сотрудником
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Car[]
     */
    public function cars()
    {
        return $this->hasMany(Car::class);
    }
    
    /**
     * Подразделение в котором числится сотрудник
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Department
     */
    public function department()
    {
        return $this->belongsTo(Department::class);
    }
    
    /// GETTERS & SETTERS ///
    
    /**
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->lastname . ' ' . $this->firstname . ' ' . $this->middlename;
    }
    
    /**
     *
     * @param string
     * 
     * @return null
     */
    public function setFullNameAttribute($fullName)
    {
        $nameData = explode(' ', $fullName);
        if (isset($nameData[0])) {
            $this->lastname = $nameData[0];
        }
        if (isset($nameData[1])) {
            $this->firstname = $nameData[1];
        }
        if (isset($nameData[2])) {
            $this->middlename = $nameData[2];
        }
    }
}
