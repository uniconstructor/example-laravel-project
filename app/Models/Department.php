<?php

namespace App\Models;

// эти классы не используются в коде, но подключены для того чтобы сократить блок комментариев модели
// пожалуйста не убирайте их, чтобы подсказака для IDE продолжала нормально работать
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

// base model class
use Illuminate\Database\Eloquent\Model;
// Eloquence
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Sofa\Eloquence\Metable;
use Sofa\Eloquence\Mutable;
use Sofa\Eloquence\Validable;
use Sofa\Eloquence\Contracts\CleansAttributes;
use Sofa\Eloquence\Contracts\Validable as ValidableContract;
// Model revisions
use Venturecraft\Revisionable\RevisionableTrait;
// soft deleting
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Подразделение iflat
 *
 * @SWG\Definition (
 *      definition="Department",
 *      required={"name"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="parent_id",
 *          description="parent_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="_lft",
 *          description="_lft",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="_rgt",
 *          description="_rgt",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 * @property int                                                                                 $id
 * @property string                                                                              $name
 * @property string                                                                              $description
 * @property int                                                                                 $parent_id
 * @property int                                                                                 $_lft
 * @property int                                                                                 $_rgt
 * @property \Carbon\Carbon                                                                      $created_at
 * @property \Carbon\Carbon                                                                      $updated_at
 * @property \Carbon\Carbon                                                                      $deleted_at
 * @property-read Collection|Car[]                                                               $cars
 * @property-read Collection|IflatUser[]                                                         $iflatUsers
 * @property-read \App\Models\Department                                                         $parentDepartment
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Department whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Department whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Department whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Department whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Department whereLft($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Department whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Department whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Department whereRgt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Department whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read string|null                                                                    $mapping_for
 * @property-read \Sofa\Eloquence\Metable\AttributeBag                                           $meta_attributes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Sofa\Eloquence\Metable\Attribute[]   $metaAttributes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 */
class Department extends Model implements ValidableContract, CleansAttributes
{
    /**
     * @see https://github.com/jarektkaczyk/eloquence/wiki/Base
     */
    use Eloquence, Mappable, Metable, Mutable, Validable;
    /**
     * @see https://laravel.com/docs/5.3/eloquent#soft-deleting
     */
    use SoftDeletes;
    /**
     * @see https://github.com/VentureCraft/revisionable
     */
    use RevisionableTrait;
    
    /**
     * @var string
     */
    public $table = 'departments';
    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * @var array
     */
    public $fillable = [
        'name',
        'description',
        'parent_id',
        '_lft',
        '_rgt',
    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'          => 'integer',
        'name'        => 'string',
        'description' => 'string',
        'parent_id'   => 'integer',
        '_lft'        => 'integer',
        '_rgt'        => 'integer',
    ];
    /**
     * @var bool - записывать операцию создания записи в историю изменений объекта
     *
     * @see https://github.com/VentureCraft/revisionable#storing-creations
     */
    protected $revisionCreationsEnabled = true;
    /**
     * @var array - список полей, исключенных из истории изменений
     *
     * @see https://github.com/VentureCraft/revisionable#more-control
     */
    protected $dontKeepRevisionOf = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
    ];
    
    /**
     * Вышестоящее подразделение
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Department
     */
    public function parentDepartment()
    {
        return $this->belongsTo(Department::class);
    }
    
    /**
     * Все сотрудники подразделения
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|IflatUser[]
     */
    public function iflatUsers()
    {
        return $this->hasMany(IflatUser::class);
    }
    
    /**
     * Все автомобили закрепленные за подразделением
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|Car[]
     */
    public function cars()
    {
        return $this->hasMany(Car::class);
    }
}
