<?php

namespace App\Models;

// эти классы не используются в коде, но подключены для того чтобы сократить блок комментариев модели
// пожалуйста не убирайте их, чтобы подсказака для IDE продолжала нормально работать
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

// base model class
use Illuminate\Database\Eloquent\Model;
// Eloquence
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Sofa\Eloquence\Metable;
use Sofa\Eloquence\Mutable;
use Sofa\Eloquence\Validable;
use Sofa\Eloquence\Contracts\CleansAttributes;
use Sofa\Eloquence\Contracts\Validable as ValidableContract;
// Model revisions
use Venturecraft\Revisionable\RevisionableTrait;
// soft deleting
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Плановая задача, которую необходимо выполнить для автомобиля
 * Каждая задача прикреплена к одному автомобилю, каждый автомобиль имеет несколько задач разного типа
 * Задача автоматически активируется и завершается событиями модели Car
 * То какие именно события отвечают за смену статуса задачи указывается в типе события
 *
 * @package App\Models
 * @property int $id
 * @property int $car_id
 * @property int $task_type_id
 * @property string $name
 * @property string $description
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read string|null $mapping_for
 * @property-read \Sofa\Eloquence\Metable\AttributeBag $meta_attributes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Sofa\Eloquence\Metable\Attribute[] $metaAttributes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTask whereCarId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTask whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTask whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTask whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTask whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTask whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTask whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTask whereTaskTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\CarTask whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CarTask extends Model implements ValidableContract, CleansAttributes
{
    /**
     * @see https://github.com/jarektkaczyk/eloquence/wiki/Base
     */
    use Eloquence, Mappable, Metable, Mutable, Validable;
    /**
     * @see https://laravel.com/docs/5.3/eloquent#soft-deleting
     */
    use SoftDeletes;
    /**
     * @see https://github.com/VentureCraft/revisionable
     */
    use RevisionableTrait;
    
    /**
     * @var string
     */
    public $table = 'car_tasks';
    /**
     * @var array
     *
     * @see https://laravel.com/docs/5.3/eloquent#mass-assignment
     */
    public $fillable = [
        'car_id',
        'task_type_id',
        'name',
        'description',
        'status',
    ];
    /**
     * @inheritdoc
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     *
     * @see https://laravel.com/docs/5.3/eloquent-mutators
     */
    protected $casts = [
        'id'           => 'integer',
        'car_id'       => 'integer',
        'task_type_id' => 'integer',
        'name'         => 'string',
        'description'  => 'string',
        'status'       => 'string',
    ];
    /**
     * @inheritdoc
     */
    protected $appends = [];
    /**
     * @var bool - записывать операцию создания записи в историю изменений объекта
     *
     * @see https://github.com/VentureCraft/revisionable#storing-creations
     */
    protected $revisionCreationsEnabled = true;
    /**
     * @var array - список полей, исключенных из истории изменений
     *
     * @see https://github.com/VentureCraft/revisionable#more-control
     */
    protected $dontKeepRevisionOf = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * Model attribute mapping:
     * [
     *      // implicit relation mapping:
     *      'profile'           => ['first_name', 'last_name'],
     *      // explicit relation mapping:
     *      'picture'           => 'profile.picture_path',
     *      // simple alias
     *      'dev_friendly_name' => 'badlynamedcolumn',
     *      'alias'             => 'real_column'
     * ]
     *
     * @var array
     *
     * @see https://github.com/jarektkaczyk/eloquence/wiki/Mappable
     */
    protected $maps = [];
    
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];
    
    /**
     * @inheritdoc
     */
    public static function boot()
    {
        // обработка событий
        // действия перед созданием модели
        static::creating(
            function (CarTask $model) {
                return true;
            }
        );
        // действия перед сохранением модели
        static::saving(
            function (CarTask $model) {
                return true;
            }
        );
        // действия перед удалением модели
        static::deleting(
            function (CarTask $model) {
                return true;
            }
        );
        
        parent::boot();
    }
    
    /// RELATIONS ///
    
    /// SCOPES ///
    
    /// METHODS ///
}