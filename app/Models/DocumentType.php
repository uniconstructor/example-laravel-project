<?php

namespace App\Models;

// эти классы не используются в коде, но подключены для того чтобы сократить блок комментариев модели
// пожалуйста не убирайте их, чтобы подсказака для IDE продолжала нормально работать
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

// base model class
use Illuminate\Database\Eloquent\Model;
// Eloquence
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Sofa\Eloquence\Metable;
use Sofa\Eloquence\Mutable;
use Sofa\Eloquence\Validable;
use Sofa\Eloquence\Contracts\CleansAttributes;
use Sofa\Eloquence\Contracts\Validable as ValidableContract;
// Model revisions
use Venturecraft\Revisionable\RevisionableTrait;
// soft deleting
use Illuminate\Database\Eloquent\SoftDeletes;
// nested set
use Kalnoy\Nestedset\NodeTrait;
// автогенерация читаемых ID
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

/**
 * Тип документа автомобиля
 *
 * @SWG\Definition (
 *      definition="DocumentType",
 *      required={"name"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="slug",
 *          description="slug",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="parent_id",
 *          description="parent_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="_lft",
 *          description="_lft",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="_rgt",
 *          description="_rgt",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="creator_id",
 *          description="creator_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 * @package App\Models
 * @property int                                                                                 $id
 * @property string                                                                              $name
 * @property string                                                                              $slug
 * @property string                                                                              $description
 * @property int                                                                                 $parent_id
 * @property int                                                                                 $_lft
 * @property int                                                                                 $_rgt
 * @property int                                                                                 $creator_id
 * @property \Carbon\Carbon                                                                      $created_at
 * @property \Carbon\Carbon                                                                      $updated_at
 * @property \Carbon\Carbon                                                                      $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Document[]                $documents
 * @property-read string|null                                                                    $mapping_for
 * @property-read \Sofa\Eloquence\Metable\AttributeBag                                           $meta_attributes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Sofa\Eloquence\Metable\Attribute[]   $metaAttributes
 * @property-read \Illuminate\Database\Eloquent\Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read \App\User                                                                      $user
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentType whereCreatorId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentType whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentType whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentType whereLft($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentType whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentType whereRgt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentType whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentType whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Kalnoy\Nestedset\Collection|\App\Models\DocumentType[]                        $children
 * @property-read \App\Models\DocumentType                                                       $parent
 * @method static \Illuminate\Database\Query\Builder|\App\Models\DocumentType d()
 */
class DocumentType extends Model implements ValidableContract, CleansAttributes
{
    /**
     * @see https://github.com/jarektkaczyk/eloquence/wiki/Base
     */
    use Eloquence, Mappable, Metable, Mutable, Validable;
    /**
     * @see https://laravel.com/docs/5.3/eloquent#soft-deleting
     */
    use SoftDeletes;
    /**
     * @see https://github.com/VentureCraft/revisionable
     */
    use RevisionableTrait;
    /**
     * @see https://github.com/lazychaser/laravel-nestedset
     */
    use NodeTrait {
        NodeTrait::newEloquentBuilder insteadof Eloquence;
        NodeTrait::newEloquentBuilder as newNodeEloquentBuilder;
        NodeTrait::replicate insteadof Eloquence;
        NodeTrait::replicate as replicateNode;
    }
    /**
     * @see https://github.com/spatie/laravel-sluggable
     */
    use HasSlug;
    
    /**
     * @var string
     */
    public $table = 'document_types';
    /**
     * @var array
     *
     * @see https://laravel.com/docs/5.3/eloquent#mass-assignment
     */
    public $fillable = [
        'name',
        'slug',
        'description',
        'parent_id',
        'creator_id',
    ];
    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     *
     * @see https://laravel.com/docs/5.3/eloquent-mutators
     */
    protected $casts = [
        'id'          => 'integer',
        'name'        => 'string',
        'slug'        => 'string',
        'description' => 'string',
        'parent_id'   => 'integer',
        '_lft'        => 'integer',
        '_rgt'        => 'integer',
        'creator_id'  => 'integer',
    ];
    /**
     * @var bool - записывать операцию создания записи в историю изменений объекта
     *
     * @see https://github.com/VentureCraft/revisionable#storing-creations
     */
    protected $revisionCreationsEnabled = true;
    /**
     * @var array - список полей, исключенных из истории изменений
     *
     * @see https://github.com/VentureCraft/revisionable#more-control
     */
    protected $dontKeepRevisionOf = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
    ];
    
    /**
     * {@inheritdoc}
     * 
     * @throws \InvalidArgumentException
     */
    public static function boot()
    {
        // вычисление количества документов, прикрепленных к автомобилю
        // @see https://laravel.com/docs/5.3/eloquent-relationships#counting-related-models
        static::addGlobalScope('withDocumentsCount', function ($builder) {
            /** @var $builder Builder */
            $builder->withCount('documents');
        });
    
        // обработка событий
        // действия перед созданием модели
        static::saving(function (DocumentType $model) {
            // автоматически генерируем код типа документа если он не задан
            if (!$model->slug) {
                $model->generateSlug();
            }
    
            return true;
        });
        // действия перед удалением модели
        static::deleting(function (DocumentType $model) {
            return $model->canBeDeleted();
        });
    
        parent::boot();
    }
    
    /// RELATIONS ///
    
    /**
     * @return HasMany|Document
     */
    public function documents()
    {
        return $this->hasMany(Document::class, 'type_id');
    }
    
    /**
     * @return BelongsTo|\App\User
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class, 'creator_id');
    }
    
    /**
     * @return BelongsTo|\App\User
     */
    public function creator()
    {
        return $this->user();
    }
    
    /// GETTERS & SETTERS ///
    
    
    
    /// SCOPES ///
    
    
    
    /// METHODS ///
    
    /**
     * Get the options for generating the slug.
     *
     * @return SlugOptions
     */
    public function getSlugOptions()
    {
        return SlugOptions::create()
            ->generateSlugsFrom(['name'])
            ->saveSlugsTo('slug')
            ->usingSeparator('_');
    }
    
    /**
     * Все автомобили для которых есть документы этого типа
     *
     * @return Collection<Car>
     */
    public function getCars()
    {
        $typeId = $this->id;
        
        return Car::whereHas('documents', function ($query) use ($typeId) {
            $query->where('type_id', '=', $typeId);
        })->get();
    }
    
    /**
     * Определить, можно ли удалять этот тип документа
     * Тип документа нельзя удалять если:
     * - в базе есть хотя бы один документ этого типа
     * - у этого типа документа есть подтипы
     *
     * @return bool
     */
    public function canBeDeleted()
    {
        if ($this->documents->isNotEmpty()) {
            return false;
        }
        if ($this->children->isNotEmpty()) {
            return false;
        }
        
        return true;
    }
}
