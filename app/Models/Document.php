<?php

namespace App\Models;

// эти классы не используются в коде, но подключены для того чтобы сократить блок комментариев модели
// пожалуйста не убирайте их, чтобы подсказака для IDE продолжала нормально работать
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

use Carbon\Carbon;
// base model class
use Illuminate\Database\Eloquent\Model;
// Eloquence
use Sofa\Eloquence\Eloquence;
use Sofa\Eloquence\Mappable;
use Sofa\Eloquence\Metable;
use Sofa\Eloquence\Mutable;
use Sofa\Eloquence\Validable;
use Sofa\Eloquence\Contracts\CleansAttributes;
use Sofa\Eloquence\Contracts\Validable as ValidableContract;
// Model revisions
use Venturecraft\Revisionable\RevisionableTrait;
// soft deleting
use Illuminate\Database\Eloquent\SoftDeletes;
// загрузка файлов
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
// автогенерация читаемых ID
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
// события для оповещений
use App\Events\Car\DocumentExpires;
use App\Events\Car\DocumentExpired;
use App\Events\Car\KaskoExpires;
use App\Events\Car\KaskoExpired;
use App\Events\Car\LastMaintenanceUpdated;
use App\Events\Car\LastOilRefreshUpdated;
use App\Events\Car\MileageAdded;
use App\Events\Car\OsagoExpires;
use App\Events\Car\OsagoExpired;

/**
 * Документ автомобиля
 *
 * @SWG\Definition (
 *      definition="Document",
 *      required={"type_id", "car_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="slug",
 *          description="slug",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="type_id",
 *          description="type_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="car_id",
 *          description="car_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="creator_id",
 *          description="creator_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="start_date",
 *          description="start_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="end_date",
 *          description="end_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 * @package App\Models
 * @property int                                                   $id
 * @property string                                                $name
 * @property string                                                $slug
 * @property string                                                $description
 * @property int                                                   $type_id
 * @property int                                                   $car_id
 * @property int                                                   $creator_id
 * @property \Carbon\Carbon                                        $start_date
 * @property \Carbon\Carbon                                        $end_date
 * @property \Carbon\Carbon                                        $created_at
 * @property \Carbon\Carbon                                        $updated_at
 * @property \Carbon\Carbon                                        $deleted_at
 * @property-read Car                                              $car
 * @property-read DocumentType                                     $documentType
 * @property-read string|null                                      $mapping_for
 * @property-read \Sofa\Eloquence\Metable\AttributeBag             $meta_attributes
 * @property-read Collection|\Sofa\Eloquence\Metable\Attribute[]   $metaAttributes
 * @property-read Collection|\Venturecraft\Revisionable\Revision[] $revisionHistory
 * @property-read \App\User                                        $user
 * @method static Builder|Document whereCarId($value)
 * @method static Builder|Document whereCreatedAt($value)
 * @method static Builder|Document whereCreatorId($value)
 * @method static Builder|Document whereDeletedAt($value)
 * @method static Builder|Document whereDescription($value)
 * @method static Builder|Document whereEndDate($value)
 * @method static Builder|Document whereId($value)
 * @method static Builder|Document whereName($value)
 * @method static Builder|Document whereSlug($value)
 * @method static Builder|Document whereStartDate($value)
 * @method static Builder|Document whereTypeId($value)
 * @method static Builder|Document whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read Collection|\Spatie\MediaLibrary\Media[]          $media
 * @property-read string                                           $car_full_name
 * @property-read string                                           $car_name
 * @property-read string                                           $full_name
 * @property-read string                                           $type_name
 * @todo добавить статус документа (черновик/действует/истек/архив)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Document ofType(\App\Models\DocumentType $documentType)
 */
class Document extends Model implements ValidableContract, CleansAttributes, HasMedia, HasMediaConversions
{
    /**
     * @see https://github.com/jarektkaczyk/eloquence/wiki/Base
     */
    use Eloquence, Mappable, Metable, Mutable, Validable;
    /**
     * @see https://laravel.com/docs/5.3/eloquent#soft-deleting
     */
    use SoftDeletes;
    /**
     * @see https://github.com/VentureCraft/revisionable
     */
    use RevisionableTrait;
    /**
     * @see https://docs.spatie.be/laravel-medialibrary/v4/basic-usage/preparing-your-model
     */
    use HasMediaTrait;
    /**
     * @see https://github.com/spatie/laravel-sluggable
     */
    use HasSlug;
    
    /**
     * @var string
     */
    public $table = 'documents';
    /**
     * @var array
     *
     * @see https://laravel.com/docs/5.3/eloquent#mass-assignment
     */
    public $fillable = [
        'name',
        'slug',
        'description',
        'type_id',
        'car_id',
        'creator_id',
        'start_date',
        'end_date',
    ];
    /**
     * @inheritdoc
     */
    protected $dates = [
        //'start_date',
        //'end_date',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     *
     * @see https://laravel.com/docs/5.3/eloquent-mutators
     */
    protected $casts = [
        'id'            => 'integer',
        'name'          => 'string',
        'slug'          => 'string',
        'description'   => 'string',
        'type_id'       => 'integer',
        'car_id'        => 'integer',
        'creator_id'    => 'integer',
        //'start_date'    => 'date',
        //'end_date'      => 'date',
        // динамические поля
        'full_name'     => 'string',
        'car_name'      => 'string',
        'car_full_name' => 'string',
        'type_name'     => 'string',
        'type_slug'     => 'string',
        'car_slug'      => 'string',
    ];
    /**
     * @inheritdoc
     */
    protected $appends = [
        'full_name',
        'car_name',
        'car_full_name',
        'type_name',
        'type_slug',
        'car_slug',
    ];
    /**
     * @var bool - записывать операцию создания записи в историю изменений объекта
     *
     * @see https://github.com/VentureCraft/revisionable#storing-creations
     */
    protected $revisionCreationsEnabled = true;
    /**
     * @var array - список полей, исключенных из истории изменений
     *
     * @see https://github.com/VentureCraft/revisionable#more-control
     */
    protected $dontKeepRevisionOf = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * Model attribute mapping:
     * [
     *      // implicit relation mapping:
     *      'profile'           => ['first_name', 'last_name'],
     *      // explicit relation mapping:
     *      'picture'           => 'profile.picture_path',
     *      // simple alias
     *      'dev_friendly_name' => 'badlynamedcolumn',
     *      'alias'             => 'real_column'
     * ]
     *
     * @var array
     *
     * @see https://github.com/jarektkaczyk/eloquence/wiki/Mappable
     */
    protected $maps = [
        'type_slug' => 'documentType.slug',
        'car_slug'  => 'car.slug',
    ];
    
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'type_id' => 'required',
        'car_id'  => 'required',
    ];
    
    /**
     * @inheritdoc
     */
    public static function boot()
    {
        // обработка событий
        // действия перед созданием модели
        static::creating(function (Document $model) {
            if (!$model->name) {
                // автоматическая генерация имени документа
                $model->name = self::newDocumentName($model->documentType, $model->car);
            }
            
            return true;
        });
        // действия перед сохранением модели
        static::saving(function (Document $model) {
            // @todo кажется дату нужно обрабатывать валидатором, вот так: 'start_date' => 'date|nullable'
            if (!$model->start_date) {
                $model->start_date = null;
            }
            // этим людям зачем-то всегда нужна дата окончания - установим ее из deus ex
            if (!$model->end_date) {
                // The year is 2037
                // It's a time of great innovation
                // and technological advancement
                // It's also a time of chaos
                // and conspiracy
                // (а вообще сингулярность должна в этот год случиться так что ждем)
                $model->end_date = \Carbon\Carbon::create(2037, 01, 01);
            }
            
            return true;
        });
        // действия перед удалением модели
        static::deleting(function (Document $model) {
            // ищем все файлы документа из коллекции автомобиля
            $documentFiles = $model->car->getMedia($model->documentType->slug, function ($media) use ($model) {
                /** @var \Spatie\MediaLibrary\Media $media */
                if ($media->getCustomProperty('documentId') == $model->id) {
                    return false;
                }
                
                return true;
            });
            // удаляем файлы документа из коллекции автомобиля
            $documentFiles->each(function ($file) {
                /** @var \Spatie\MediaLibrary\Media $file */
                $file->delete();
            });
            
            return true;
        });
        
        parent::boot();
    }
    
    /**
     * Сгенерировать название документа из типа документа и названия автомобиля
     *
     * @param DocumentType $documentType
     * @param Car          $car
     *
     * @return string
     */
    public static function newDocumentName(DocumentType $documentType, Car $car)
    {
        return $documentType->name . '  для ' . $car->name;
    }
    
    /**
     * Сгенерировать дату окончания действия по умолчанию (требование заказчика)
     *
     * @return string
     */
    public static function newDocumentEndDate()
    {
        $timestamp = mktime(0, 0, 0, 1, 1, 2037);
        
        return date('Y-m-d', $timestamp);
    }
    
    /**
     * Проверить наличие истекших документов и отправить оповещения
     *
     * @return void
     * 
     * @todo добавить статус к каждому документу чтобы можно было хранить архив, на который не реагируют оповещения
     */
    public static function checkDocumentsExpiration()
    {
        $documents = Document::all();
        
        foreach ($documents as $document) {
            // проверяем истекает ли каждый документ, и если истекает то когда
            if ($document->end_date) {
                $endDate = Carbon::createFromFormat('Y-m-d', $document->end_date);
                $type    = $document->documentType;
                // пожалуйста задайте стандартный slug для типов документов чтобы не пришлось его угадывать
                $docTypeIsKasko = (str_contains($type->name, 'КАСКО') || str_contains($type->slug, 'kasko'));
                $docTypeIsOsago = (str_contains($type->name, 'ОСАГО') || str_contains($type->slug, 'osago'));
                if ($endDate->lt(Carbon::now())) {
                    // дата окончания действия меньше (то есть раньше) сегодняшней - документ истек
                    event(new DocumentExpired($document->car, $document));
                    if ($docTypeIsKasko) {
                        event(new KaskoExpired($document->car, $document));
                        echo "kasko expired";
                    }
                    if ($docTypeIsOsago) {
                        event(new OsagoExpired($document->car, $document));
                    }
                } elseif ($endDate->lte(Carbon::now()->addDays(14))) {
                    // через 2 недели дата окончания действия
                    // будет меньше (то есть раньше) сегодняшней - документ истек
                    event(new DocumentExpires($document->car, $document));
                    if ($docTypeIsKasko) {
                        event(new KaskoExpires($document->car, $document));
                    }
                    if ($docTypeIsOsago) {
                        event(new OsagoExpires($document->car, $document));
                    }
                }
            }
        }
    }
    
    /// RELATIONS ///
    
    /**
     * @return BelongsTo|DocumentType
     */
    public function documentType()
    {
        return $this->belongsTo(DocumentType::class, 'type_id');
    }
    
    /**
     * @return BelongsTo|DocumentType
     */
    /*public function type()
    {
        return $this->documentType();
    }*/
    
    /**
     * @return BelongsTo|Car
     */
    public function car()
    {
        return $this->belongsTo(Car::class);
    }
    
    /**
     * @return BelongsTo|\App\User
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class, 'creator_id');
    }
    
    /// SCOPES ///
    
    /**
     * Все документы указанного типа
     *
     * @param Builder      $query
     * @param DocumentType $documentType
     *
     * @return Builder
     */
    public function scopeOfType($query, DocumentType $documentType)
    {
        return $query->where('type_id', '=', $documentType->id);
    }
    
    /// GETTERS & SETTERS ///
    
    /**
     *
     * @return string
     */
    public function getCarNameAttribute()
    {
        return $this->car->name;
    }
    
    /**
     *
     * @return string
     */
    public function getCarFullNameAttribute()
    {
        return $this->car->full_name;
    }
    
    /**
     *
     * @return string
     */
    public function getTypeNameAttribute()
    {
        return $this->documentType->name;
    }
    
    /**
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return $this->documentType->name . ' для ' . $this->car->name;
    }
    
    /// METHODS ///
    
    /**
     * Преобразования для загруженных файлов
     *
     * @see https://docs.spatie.be/laravel-medialibrary/v4/converting-images/defining-conversions
     *
     * @return null
     *
     * @throws \Spatie\MediaLibrary\Exceptions\InvalidConversionParameter
     */
    public function registerMediaConversions()
    {
        // маленькие иконки изображений (для предпросмотра)
        $this->addMediaConversion('w_150')
            //->setManipulations(['w' => 150])
            ->setWidth(150)
            ->performOnCollections('images')
            ->nonQueued();
        
        // средние изображения (для inline-галереи)
        $this->addMediaConversion('w_500')
            //->setManipulations(['w' => 500])
            ->setWidth(500)
            ->performOnCollections('images')
            ->nonQueued();
        
        // большие изображения (полный экран)
        $this->addMediaConversion('w_1000')
            //->setManipulations(['w' => 1000])
            ->setWidth(1000)
            ->performOnCollections('images')
            ->nonQueued();
    }
    
    /**
     * Get the options for generating the slug.
     *
     * @return SlugOptions
     */
    public function getSlugOptions()
    {
        return SlugOptions::create()
            ->generateSlugsFrom(['type_slug', 'car_slug'])
            ->saveSlugsTo('slug')
            ->usingSeparator('_');
    }
    
    /**
     * Получить полный путь к папке с файлами документа
     *
     * @return string
     */
    public function getFolderPath()
    {
        return $this->car->getDocumentFolderPath($this);
    }
    
    /**
     * Получить Url к папке с файлами этого документа
     *
     * @return string
     */
    public function getFolderUrl()
    {
        return $this->car->getDocumentFolderUrl($this);
    }
    
    /**
     * Получить стандартное имя файла для этого документа
     *
     * @param UploadedFile $file
     *
     * @return string
     */
    public function getFileName(UploadedFile $file)
    {
        return $this->documentType->slug
            . '_' .
            $this->car->slug
            . '_' .
            str_random(4)
            . '.' .
            $file->extension();
    }
    
    /**
     * Получить полный путь к загрузке файла документа (папка + имя файла)
     *
     * @param UploadedFile $file
     *
     * @return string
     */
    public function getUploadPath(UploadedFile $file)
    {
        return $this->getFolderPath() . '/' . $this->getFileName($file);
    }
    
    /**
     * Сохранить загруженный файл и привязать его к автомобилю
     *
     * @param UploadedFile $file
     *
     * @return \Spatie\MediaLibrary\Media
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function uploadFile(UploadedFile $file)
    {
        $folder     = $this->getFolderPath();
        $name       = $this->getFileName($file);
        // файлы, которые не являются изображениями хранятся в отдельной коллекции
        // (чтобы было проще составлять галерею)
        $collection = 'extras';
        
        // дополнительные параметры файла
        $props = [
            'collection'        => $this->documentType->slug,
            'originalName'      => $file->getClientOriginalName(),
            'originalExtension' => $file->getClientOriginalExtension(),
            'mimeType'          => $file->getMimeType(),
            'documentType'      => $this->type_slug,
            'documentTypeName'  => $this->type_name,
            'documentName'      => $this->full_name,
            'documentId'        => $this->id,
            'carName'           => $this->car_name,
            'carId'             => $this->car->id,
            'isImage'           => false,
        ];
        // определяем тип файла (изображение или нет)
        if (is_image($file->getPath() . '/' . $file->getFilename())) {
            // загружено изображение - определяем его параметры
            // (размеры картинки будут необходимы для построения галереи)
            $imageInfo        = getimagesize($file->getPath() . '/' . $file->getFilename());
            $props['isImage'] = true;
            $props['width']   = $imageInfo[0];
            $props['height']  = $imageInfo[1];
            // файл определен как изображение - значит его нужно помемтить в отдельную коллекцию
            // из которой будет создаваться галерея
            $collection       = 'images';
        }
        // сохраняем файл в папку автомобиля
        //$path   = $file->storeAs($folder, $name);
        $file->storeAs($folder, $name);
        
        // привязываем файл к документу
        return $this->addMedia($file)
            ->usingFileName($name)
            ->withCustomProperties($props)
            ->toCollection($collection);
    }
    
    /**
     * Получить все загруженные файлы этого документа (все изображения + все файлы других типов)
     *
     * @return \Illuminate\Support\Collection
     */
    public function getFiles()
    {
        return $this->getMedia();
    }
    
    /**
     * Получить все дополнительные файлы этого документа (кроме изображений)
     *
     * @return \Illuminate\Support\Collection
     */
    public function getExtras()
    {
        return $this->getMedia('extras');
    }
    
    /**
     * Получить все загруженные изображения этого документа (без дополнительных файлов)
     *
     * @return \Illuminate\Support\Collection
     */
    public function getImages()
    {
        return $this->getMedia('images');
    }
    
    /**
     * Определить количество загруженных файлов
     *
     * @return int
     */
    public function countFiles()
    {
        return $this->getFiles()->count();
    }
    
    /**
     * Определить есть ли для этого документа загруженные файлы
     *
     * @return bool
     */
    public function hasFiles()
    {
        return ($this->countFiles() > 0);
    }
    
    /**
     * Определить количество загруженных изображений
     *
     * @return int
     */
    public function countImages()
    {
        return $this->getImages()->count();
    }
    
    /**
     * Определить есть ли для этого документа загруженные изображения
     *
     * @return bool
     */
    public function hasImages()
    {
        return ($this->countImages() > 0);
    }
}
