<?php


namespace App\Exceptions;

/**
 * Исключение возникающее при конфликте имен функций хелперов проекта с хелперами внешних библиотек
 *
 * @package App\Exceptions
 */
class HelperNameConflictException extends \Exception
{

}
