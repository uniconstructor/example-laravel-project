<?php

namespace App\Listeners;

use Unisharp\Laravelfilemanager\Events\ImageWasUploaded; 

/**
 * Обработчик событий загрузки файлов
 *
 * @package App\Listeners
 * 
 * @see https://unisharp.github.io/laravel-filemanager/events
 */
class UploadListener
{
    public function handle($event)
    {
        $method = 'on'.class_basename($event);
        if (method_exists($this, $method)) {
            call_user_func([$this, $method], $event);
        }
    }
    
    /**
     * Обработка события окончани загрузки файла
     *
     * @param ImageWasUploaded $event
     *
     * @return null
     */
    public function onImageWasUploaded(ImageWasUploaded $event)
    {
        $path = $event->path();
        //your code, for example resizing and cropping
        logger()->info('file uploaded: ' . $path);
    }
}