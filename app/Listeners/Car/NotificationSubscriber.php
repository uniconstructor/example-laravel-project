<?php

namespace App\Listeners\Car;

// events
use App\Events\Car\DocumentExpired;
use App\Events\Car\DocumentExpires;
use App\Events\Car\KaskoExpirationNotice;
use App\Events\Car\KaskoExpires;
use App\Events\Car\LastMaintenanceUpdated;
use App\Events\Car\LastOilRefreshUpdated;
use App\Events\Car\MaintenanceMileageNotice;
use App\Events\Car\MileageAdded;
use App\Events\Car\MileageOverMaintenanceInterval;
use App\Events\Car\MileageOverMaintenanceMargin;
use App\Events\Car\MileageOverOilRefreshInterval;
use App\Events\Car\MileageOverOilRefreshMargin;
use App\Events\Car\OilRefreshMileageNotice;
// app models
use App\Events\Car\OsagoExpirationNotice;
use App\Events\Car\OsagoExpires;
use App\Models\CarNotificationType;
use App\Models\Car;
// notification factory
use App\Notifications\CarNotification;

/**
 * Отправить оповещения (email и sms) о необходимости замены масла для автомобиля
 * Оповещения отправляются по двум каналам (email и sms) администратору системы и сотруднику за которым
 * закреплен автомобиль.
 * Текст оповещения настраивается в админке
 * Каналы оповещений и оповещаемые пользователи можно задать в настройках модели
 * CarNotificationType)
 *
 * @package App\Listeners
 */
class NotificationSubscriber
{
    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        // события изменения пробега
        $events->listen(
            'App\Events\Car\MileageAdded',
            'App\Listeners\Car\NotificationSubscriber@onMileageAdded'
        );
        $events->listen(
            'App\Events\Car\MileageOverMaintenanceMargin',
            'App\Listeners\Car\NotificationSubscriber@onMileageOverMaintenanceMargin'
        );
        $events->listen(
            'App\Events\Car\MileageOverOilRefreshMargin',
            'App\Listeners\Car\NotificationSubscriber@onMileageOverOilRefreshMargin'
        );
        $events->listen(
            'App\Events\Car\LastMaintenanceUpdated',
            'App\Listeners\Car\NotificationSubscriber@onLastMaintenanceUpdated'
        );
        $events->listen(
            'App\Events\Car\LastOilRefreshUpdated',
            'App\Listeners\Car\NotificationSubscriber@onLastOilRefreshUpdated'
        );
        
        // события отправки оповещений
        $events->listen(
            'App\Events\Car\OilRefreshMileageNotice',
            'App\Listeners\Car\NotificationSubscriber@onOilRefreshMileageNotice'
        );
        $events->listen(
            'App\Events\Car\MaintenanceMileageNotice',
            'App\Listeners\Car\NotificationSubscriber@onMileageNotice'
        );
        $events->listen(
            'App\Events\Car\KaskoExpirationNotice',
            'App\Listeners\Car\NotificationSubscriber@onDocumentExpirationNotice'
        );
        $events->listen(
            'App\Events\Car\OsagoExpirationNotice',
            'App\Listeners\Car\NotificationSubscriber@onDocumentExpirationNotice'
        );
        
        // события истечения срока действия документов
        $events->listen(
            'App\Events\Car\DocumentExpires',
            'App\Listeners\Car\NotificationSubscriber@onDocumentExpires'
        );
        $events->listen(
            'App\Events\Car\KaskoExpires',
            'App\Listeners\Car\NotificationSubscriber@onKaskoExpires'
        );
        $events->listen(
            'App\Events\Car\OsagoExpires',
            'App\Listeners\Car\NotificationSubscriber@onOsagoExpires'
        );
    }
    
    /**
     * Отправить оповещения о необходимости замены масла для указанного в событии автомобиля
     * Оповещения отправляются по нескольким каналам (email, sms, ...) и нескольким пользователям:
     * администратору системы и сотруднику за которым закреплен автомобиль.
     *
     * Текст шаблона каждого оповещения настраивается в админке
     * Текст самого оповещения генерируется в этом методе при помощи подстановки
     * в шаблон оповещения данных автомобиля
     * Используемые каналы оповещений и оповещаемых пользователей можно задать
     * в настройках модели CarNotificationType
     *
     * @param $event
     *
     * @return null
     */
    public function onOilRefreshMileageNotice($event)
    {
        $this->onMileageNotice($event);
    }
    
    /**
     * Обработка событий имеющих отношение к пробегу
     *
     * @param $event
     *
     * @return null
     */
    public function onMileageNotice($event)
    {
        // получаем все типы оповещений которые реагируют на это событие
        if (!$notificationTypes = CarNotificationType::ofEvent($event)->get()) {
            return;
        }
        // создаем и отправляем оповещения каждого типа
        foreach ($notificationTypes as $notificationType) {
            $notification = new CarNotification($notificationType, $event->car);
            // оповещаем администратора обо всех событиях через Slack
            $admin = \App\User::find(1);
            $admin->notify($notification);
        }
    }
    
    /**
     * Обработка событий имеющих отношение к истечению срока действия документов
     *
     * @param $event
     *
     * @return null
     */
    public function onDocumentExpirationNotice($event)
    {
        // получаем все типы оповещений которые реагируют на это событие
        if (!$notificationTypes = CarNotificationType::ofEvent($event)->get()) {
            return;
        }
        // создаем и отправляем оповещения каждого типа
        foreach ($notificationTypes as $notificationType) {
            $notification = new CarNotification($notificationType, $event->car);
            // оповещаем администратора обо всех событиях через Slack
            $admin = \App\User::find(1);
            $admin->notify($notification);
        }
    }
    
    /**
     * Обработка события увеличения пробега
     *
     * @param $event
     *
     * @return null
     */
    public function onMileageAdded(MileageAdded $event)
    {
        $car = $event->car;
        
        if ($car->isOverMaintenanceMargin()) {
            event(new MileageOverMaintenanceMargin($car));
        }
        if ($car->isOverMaintenanceInterval()) {
            event(new MileageOverMaintenanceInterval($car));
        }
        if ($car->isOverOilRefreshMargin()) {
            event(new MileageOverOilRefreshMargin($car));
        }
        if ($car->isOverOilRefreshInterval()) {
            event(new MileageOverOilRefreshInterval($car));
        }
        
        $this->onMileageNotice($event);
    }
    
    /**
     * Обработка события "пробег превысил границу оповещения о необходимости ТО"
     *
     * @param $event
     *
     * @return null
     */
    public function onMileageOverMaintenanceMargin(MileageOverMaintenanceMargin $event)
    {
        $car       = $event->car;
        $eventName = class_basename($event);
    
        if (!$car->isNotifiedAbout(class_basename($event))) {
            event(new MaintenanceMileageNotice($car));
            $car->markNotifiedAbout($eventName);
        }
    }
    
    /**
     * Обработка события "пробег превысил границу оповещения замены масла"
     *
     * @param $event
     *
     * @return null
     */
    public function onMileageOverOilRefreshMargin(MileageOverOilRefreshMargin $event)
    {
        $car       = $event->car;
        $eventName = class_basename($event);
    
        if (!$car->isNotifiedAbout($eventName)) {
            event(new OilRefreshMileageNotice($car));
            $car->markNotifiedAbout($eventName);
        }
    }
    
    /**
     * Обработка события "обновлен пробег последнего ТО"
     *
     * @param $event
     *
     * @return null
     */
    public function onLastMaintenanceUpdated(LastMaintenanceUpdated $event)
    {
        $car = $event->car;
        
        foreach ($event->clearNotifications as $eventName) {
            $car->clearNotificationAbout($eventName);
            $this->onMileageNotice($event);
        }
    }
    
    /**
     * Обработка события "обновлен пробег последней замены масла"
     *
     * @param $event
     *
     * @return null
     */
    public function onLastOilRefreshUpdated(LastOilRefreshUpdated $event)
    {
        $car = $event->car;
        
        foreach ($event->clearNotifications as $eventName) {
            $car->clearNotificationAbout($eventName);
        }
    }
    
    /**
     * Обработка события "истекает КАСКО"
     *
     * @param $event
     *
     * @return null
     */
    public function onKaskoExpires(KaskoExpires $event)
    {
        $car       = $event->car;
        $eventName = class_basename($event);
    
        if (!$car->isNotifiedAbout(class_basename($event))) {
            event(new KaskoExpirationNotice($car));
            $car->markNotifiedAbout($eventName);
        }
    }
    
    /**
     * Обработка события "истекает ОСАГО"
     *
     * @param $event
     *
     * @return null
     */
    public function onOsagoExpires(OsagoExpires $event)
    {
        $car       = $event->car;
        $eventName = class_basename($event);
    
        if (!$car->isNotifiedAbout(class_basename($event))) {
            event(new OsagoExpirationNotice($car));
            $car->markNotifiedAbout($eventName);
        }
    }
    
    /**
     * Обработка события "истекает срок действия документа"
     *
     * @param $event
     *
     * @return null
     */
    public function onDocumentExpires(DocumentExpires $event)
    {
        $car       = $event->car;
        $eventName = class_basename($event);
        
        if (!$car->isNotifiedAbout($eventName)) {
            $this->onDocumentExpirationNotice($event);
            $car->markNotifiedAbout($eventName);
        }
    }
    
    /**
     * Обработка события "срок действия документа истек"
     *
     * @param $event
     *
     * @return null
     */
    public function onDocumentExpired(DocumentExpired $event)
    {
        $car       = $event->car;
        $eventName = class_basename($event);
    
        if (!$car->isNotifiedAbout($eventName)) {
            $this->onDocumentExpirationNotice($event);
            $car->markNotifiedAbout($eventName);
        }
    }
    
    /**
     * Обработка события "пробег превысил интервал ТО"
     *
     * @param $event
     *
     * @return null
     */
    public function onMileageOverMaintenanceInterval(MileageOverMaintenanceInterval $event)
    {
        $car       = $event->car;
        $eventName = class_basename($event);
    
        if (!$car->isNotifiedAbout($eventName)) {
            $this->onMileageNotice($event);
            $car->markNotifiedAbout($eventName);
        }
    }
    
    /**
     * Обработка события "пробег превысил интервал замены масла"
     *
     * @param $event
     *
     * @return null
     */
    public function onMileageOverOilRefreshInterval(MileageOverOilRefreshInterval $event)
    {
        $car       = $event->car;
        $eventName = class_basename($event);
    
        if (!$car->isNotifiedAbout($eventName)) {
            $this->onMileageNotice($event);
            $car->markNotifiedAbout($eventName);
        }
    }
}