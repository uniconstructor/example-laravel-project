<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\DocumentType;
use Illuminate\Validation\Rule;

class UpdateDocumentTypeRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = DocumentType::$rules;
        $rules['slug'][] = Rule::unique('document_types', 'slug')->ignore($this->request->get('id'));
        
        return $rules;
    }
}
