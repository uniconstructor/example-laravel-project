<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Car;
use Illuminate\Validation\Rule;

/**
 * Запрос обновления записи автомобиля через форму редактирования автомобиля
 *
 * @package App\Http\Requests
 */
class UpdateCarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Car::$rules;
        $rules['reg_number'][] = Rule::unique('cars', 'reg_number')->ignore($this->request->get('id'));
        
        return $rules;
    }
}
