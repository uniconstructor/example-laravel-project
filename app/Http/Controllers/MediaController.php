<?php

namespace App\Http\Controllers;

use File;
use Response;
// models
use Spatie\MediaLibrary\Media;


/**
 * Контроллер для работы с пользовательскими файлами
 * - ограничивает доступ к просмотру файлов
 * - ограничивает доступ к скачиванию файлов
 *
 * @package App\Http\Controllers
 */
class MediaController extends AppBaseController
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Отобразить файл или его версию (для изображений и pdf)
     *
     * @param int    $id
     * @param string $conversion
     *
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     * 
     * @throws \Spatie\MediaLibrary\Exceptions\InvalidConversion
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function viewMedia($id, $conversion='')
    {
        /** @var Media $media */
        if (!$media = Media::find($id)) {
            $this->sendError(true, 'Media not found');
        }
        // получаем файл или его сжатую версию
        $path     = $media->getPath($conversion);
        $file     = File::get($path);
        $type     = File::mimeType($path);
        
        return Response::stream(function () use ($file) {
            echo $file;
        }, 200, ['Content-Type' => $type]);
    }
    
    /**
     * Скачать файл
     *
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * 
     * @throws \Spatie\MediaLibrary\Exceptions\InvalidConversion
     */
    public function downloadMedia($id)
    {
        /** @var Media $media */
        if (!$media = Media::find($id)) {
            $this->sendError(true, 'Media not found');
        }
        // получаем файл
        $path      = $media->getPath();
        $name      = File::name($path) . '.' . File::extension($path);
        $type      = File::mimeType($path);
        
        return Response::download($path, $name, ['Content-Type' => $type]);
    }
    
    /**
     * Удалить файл из списка медиа-файлов модели
     *
     * @param int $id
     * 
     * @return \Illuminate\Http\JsonResponse
     * 
     * @throws \Exception
     */
    public function deleteMedia($id)
    {
        /** @var Media $media */
        if ($media = Media::find($id)) {
            $media->delete();
        }
        
        return $this->sendResponse($id, 'Media deleted');
    }
}