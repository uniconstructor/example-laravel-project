<?php

namespace App\Http\Controllers;

use App\DataTables\CarNotificationTypeDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCarNotificationTypeRequest;
use App\Http\Requests\UpdateCarNotificationTypeRequest;
use App\Repositories\CarNotificationTypeRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CarNotificationTypeController extends AppBaseController
{
    /** 
     * @var  CarNotificationTypeRepository 
     */
    private $carNotificationTypeRepository;

    public function __construct(CarNotificationTypeRepository $carNotificationTypeRepo)
    {
        $this->carNotificationTypeRepository = $carNotificationTypeRepo;
    }

    /**
     * Display a listing of the CarNotificationType.
     *
     * @param CarNotificationTypeDataTable $carNotificationTypeDataTable
     * @return Response
     */
    public function index(CarNotificationTypeDataTable $carNotificationTypeDataTable)
    {
        return $carNotificationTypeDataTable->render('car_notification_types.index');
    }

    /**
     * Show the form for creating a new CarNotificationType.
     *
     * @return Response
     */
    public function create()
    {
        return view('car_notification_types.create');
    }

    /**
     * Store a newly created CarNotificationType in storage.
     *
     * @param CreateCarNotificationTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateCarNotificationTypeRequest $request)
    {
        $input = $request->all();

        $carNotificationType = $this->carNotificationTypeRepository->create($input);

        Flash::success('Тип оповещения создан.');

        return redirect(route('carNotificationTypes.index'));
    }

    /**
     * Display the specified CarNotificationType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $carNotificationType = $this->carNotificationTypeRepository->findWithoutFail($id);

        if (empty($carNotificationType)) {
            Flash::error('Car Notification Type not found');

            return redirect(route('carNotificationTypes.index'));
        }

        return view('car_notification_types.show')->with('carNotificationType', $carNotificationType);
    }

    /**
     * Show the form for editing the specified CarNotificationType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $carNotificationType = $this->carNotificationTypeRepository->findWithoutFail($id);

        if (empty($carNotificationType)) {
            Flash::error('Car Notification Type not found');

            return redirect(route('carNotificationTypes.index'));
        }

        return view('car_notification_types.edit')->with('carNotificationType', $carNotificationType);
    }

    /**
     * Update the specified CarNotificationType in storage.
     *
     * @param  int              $id
     * @param UpdateCarNotificationTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCarNotificationTypeRequest $request)
    {
        $carNotificationType = $this->carNotificationTypeRepository->findWithoutFail($id);

        if (empty($carNotificationType)) {
            Flash::error('Car Notification Type not found');

            return redirect(route('carNotificationTypes.index'));
        }

        $carNotificationType = $this->carNotificationTypeRepository->update($request->all(), $id);

        Flash::success('Оповещение обновлено.');

        return redirect(route('carNotificationTypes.index'));
    }

    /**
     * Remove the specified CarNotificationType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $carNotificationType = $this->carNotificationTypeRepository->findWithoutFail($id);

        if (empty($carNotificationType)) {
            Flash::error('Car Notification Type not found');

            return redirect(route('carNotificationTypes.index'));
        }

        $this->carNotificationTypeRepository->delete($id);

        Flash::success('Тип оповещения удален.');

        return redirect(route('carNotificationTypes.index'));
    }
}
