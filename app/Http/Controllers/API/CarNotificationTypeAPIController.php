<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCarNotificationTypeAPIRequest;
use App\Http\Requests\API\UpdateCarNotificationTypeAPIRequest;
use App\Models\CarNotificationType;
use App\Repositories\CarNotificationTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CarNotificationTypeController
 * @package App\Http\Controllers\API
 */

class CarNotificationTypeAPIController extends AppBaseController
{
    /** 
     * @var  CarNotificationTypeRepository 
     */
    private $carNotificationTypeRepository;

    public function __construct(CarNotificationTypeRepository $carNotificationTypeRepo)
    {
        $this->carNotificationTypeRepository = $carNotificationTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/carNotificationTypes",
     *      summary="Get a listing of the CarNotificationTypes.",
     *      tags={"CarNotificationType"},
     *      description="Get all CarNotificationTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CarNotificationType")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->carNotificationTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->carNotificationTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $carNotificationTypes = $this->carNotificationTypeRepository->all();

        return $this->sendResponse($carNotificationTypes->toArray(), 'Car Notification Types retrieved successfully');
    }

    /**
     * @param CreateCarNotificationTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/carNotificationTypes",
     *      summary="Store a newly created CarNotificationType in storage",
     *      tags={"CarNotificationType"},
     *      description="Store CarNotificationType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CarNotificationType that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CarNotificationType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CarNotificationType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCarNotificationTypeAPIRequest $request)
    {
        $input = $request->all();

        $carNotificationTypes = $this->carNotificationTypeRepository->create($input);

        return $this->sendResponse($carNotificationTypes->toArray(), 'Car Notification Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/carNotificationTypes/{id}",
     *      summary="Display the specified CarNotificationType",
     *      tags={"CarNotificationType"},
     *      description="Get CarNotificationType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CarNotificationType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CarNotificationType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CarNotificationType $carNotificationType */
        $carNotificationType = $this->carNotificationTypeRepository->findWithoutFail($id);

        if (empty($carNotificationType)) {
            return $this->sendError('Car Notification Type not found');
        }

        return $this->sendResponse($carNotificationType->toArray(), 'Car Notification Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCarNotificationTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/carNotificationTypes/{id}",
     *      summary="Update the specified CarNotificationType in storage",
     *      tags={"CarNotificationType"},
     *      description="Update CarNotificationType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CarNotificationType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CarNotificationType that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CarNotificationType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CarNotificationType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCarNotificationTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var CarNotificationType $carNotificationType */
        $carNotificationType = $this->carNotificationTypeRepository->findWithoutFail($id);

        if (empty($carNotificationType)) {
            return $this->sendError('Car Notification Type not found');
        }

        $carNotificationType = $this->carNotificationTypeRepository->update($input, $id);

        return $this->sendResponse($carNotificationType->toArray(), 'CarNotificationType updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/carNotificationTypes/{id}",
     *      summary="Remove the specified CarNotificationType from storage",
     *      tags={"CarNotificationType"},
     *      description="Delete CarNotificationType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CarNotificationType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CarNotificationType $carNotificationType */
        $carNotificationType = $this->carNotificationTypeRepository->findWithoutFail($id);

        if (empty($carNotificationType)) {
            return $this->sendError('Car Notification Type not found');
        }

        $carNotificationType->delete();

        return $this->sendResponse($id, 'Car Notification Type deleted successfully');
    }
}
