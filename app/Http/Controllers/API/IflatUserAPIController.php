<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateIflatUserAPIRequest;
use App\Http\Requests\API\UpdateIflatUserAPIRequest;
use App\Models\IflatUser;
use App\Repositories\IflatUserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class IflatUserController
 * @package App\Http\Controllers\API
 */

class IflatUserAPIController extends AppBaseController
{
    /** 
     * @var  IflatUserRepository 
     */
    private $iflatUserRepository;

    public function __construct(IflatUserRepository $iflatUserRepo)
    {
        $this->iflatUserRepository = $iflatUserRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/iflatUsers",
     *      summary="Get a listing of the IflatUsers.",
     *      tags={"IflatUser"},
     *      description="Get all IflatUsers",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/IflatUser")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->iflatUserRepository->pushCriteria(new RequestCriteria($request));
        $this->iflatUserRepository->pushCriteria(new LimitOffsetCriteria($request));
        $iflatUsers = $this->iflatUserRepository->all();

        return $this->sendResponse($iflatUsers->toArray(), 'Iflat Users retrieved successfully');
    }

    /**
     * @param CreateIflatUserAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/iflatUsers",
     *      summary="Store a newly created IflatUser in storage",
     *      tags={"IflatUser"},
     *      description="Store IflatUser",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="IflatUser that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/IflatUser")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IflatUser"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateIflatUserAPIRequest $request)
    {
        $input = $request->all();

        $iflatUsers = $this->iflatUserRepository->create($input);

        return $this->sendResponse($iflatUsers->toArray(), 'Iflat User saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/iflatUsers/{id}",
     *      summary="Display the specified IflatUser",
     *      tags={"IflatUser"},
     *      description="Get IflatUser",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IflatUser",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IflatUser"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var IflatUser $iflatUser */
        $iflatUser = $this->iflatUserRepository->findWithoutFail($id);

        if (empty($iflatUser)) {
            return $this->sendError('Iflat User not found');
        }

        return $this->sendResponse($iflatUser->toArray(), 'Iflat User retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateIflatUserAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/iflatUsers/{id}",
     *      summary="Update the specified IflatUser in storage",
     *      tags={"IflatUser"},
     *      description="Update IflatUser",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IflatUser",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="IflatUser that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/IflatUser")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IflatUser"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateIflatUserAPIRequest $request)
    {
        $input = $request->all();

        /** @var IflatUser $iflatUser */
        $iflatUser = $this->iflatUserRepository->findWithoutFail($id);

        if (empty($iflatUser)) {
            return $this->sendError('Iflat User not found');
        }

        $iflatUser = $this->iflatUserRepository->update($input, $id);

        return $this->sendResponse($iflatUser->toArray(), 'IflatUser updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/iflatUsers/{id}",
     *      summary="Remove the specified IflatUser from storage",
     *      tags={"IflatUser"},
     *      description="Delete IflatUser",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IflatUser",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var IflatUser $iflatUser */
        $iflatUser = $this->iflatUserRepository->findWithoutFail($id);

        if (empty($iflatUser)) {
            return $this->sendError('Iflat User not found');
        }

        $iflatUser->delete();

        return $this->sendResponse($id, 'Iflat User deleted successfully');
    }
}
