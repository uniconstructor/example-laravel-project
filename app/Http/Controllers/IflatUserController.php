<?php

namespace App\Http\Controllers;

use App\DataTables\IflatUserDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateIflatUserRequest;
use App\Http\Requests\UpdateIflatUserRequest;
use App\Repositories\IflatUserRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class IflatUserController extends AppBaseController
{
    /** 
     * @var  IflatUserRepository 
     */
    private $iflatUserRepository;

    public function __construct(IflatUserRepository $iflatUserRepo)
    {
        $this->iflatUserRepository = $iflatUserRepo;
    }

    /**
     * Display a listing of the IflatUser.
     *
     * @param IflatUserDataTable $iflatUserDataTable
     * @return Response
     */
    public function index(IflatUserDataTable $iflatUserDataTable)
    {
        return $iflatUserDataTable->render('iflat_users.index');
    }

    /**
     * Show the form for creating a new IflatUser.
     *
     * @return Response
     */
    public function create()
    {
        return view('iflat_users.create');
    }

    /**
     * Store a newly created IflatUser in storage.
     *
     * @param CreateIflatUserRequest $request
     *
     * @return Response
     */
    public function store(CreateIflatUserRequest $request)
    {
        $input = $request->all();

        $iflatUser = $this->iflatUserRepository->create($input);

        Flash::success('Iflat User saved successfully.');

        return redirect(route('iflatUsers.index'));
    }

    /**
     * Display the specified IflatUser.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $iflatUser = $this->iflatUserRepository->findWithoutFail($id);

        if (empty($iflatUser)) {
            Flash::error('Iflat User not found');

            return redirect(route('iflatUsers.index'));
        }

        return view('iflat_users.show')->with('iflatUser', $iflatUser);
    }

    /**
     * Show the form for editing the specified IflatUser.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $iflatUser = $this->iflatUserRepository->findWithoutFail($id);

        if (empty($iflatUser)) {
            Flash::error('Iflat User not found');

            return redirect(route('iflatUsers.index'));
        }

        return view('iflat_users.edit')->with('iflatUser', $iflatUser);
    }

    /**
     * Update the specified IflatUser in storage.
     *
     * @param  int              $id
     * @param UpdateIflatUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIflatUserRequest $request)
    {
        $iflatUser = $this->iflatUserRepository->findWithoutFail($id);

        if (empty($iflatUser)) {
            Flash::error('Iflat User not found');

            return redirect(route('iflatUsers.index'));
        }

        $iflatUser = $this->iflatUserRepository->update($request->all(), $id);

        Flash::success('Iflat User updated successfully.');

        return redirect(route('iflatUsers.index'));
    }

    /**
     * Remove the specified IflatUser from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $iflatUser = $this->iflatUserRepository->findWithoutFail($id);

        if (empty($iflatUser)) {
            Flash::error('Iflat User not found');

            return redirect(route('iflatUsers.index'));
        }

        $this->iflatUserRepository->delete($id);

        Flash::success('Iflat User deleted successfully.');

        return redirect(route('iflatUsers.index'));
    }
}
