<?php

namespace App\Http\Controllers;

use App\DataTables\DocumentDataTable;
use App\Http\Requests;
use App\Models\Document;
use App\Http\Requests\CreateDocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use App\Repositories\DocumentRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class DocumentController extends AppBaseController
{
    /** 
     * @var  DocumentRepository 
     */
    private $documentRepository;

    public function __construct(DocumentRepository $documentRepo)
    {
        $this->documentRepository = $documentRepo;
    }

    /**
     * Display a listing of the Document.
     *
     * @param DocumentDataTable $documentDataTable
     * @return Response
     */
    public function index(DocumentDataTable $documentDataTable)
    {
        return $documentDataTable->render('documents.index');
    }

    /**
     * Show the form for creating a new Document.
     *
     * @return Response
     */
    public function create()
    {
        return view('documents.create');
    }

    /**
     * Store a newly created Document in storage.
     *
     * @param CreateDocumentRequest $request
     *
     * @return Response
     * 
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function store(CreateDocumentRequest $request)
    {
        $input = $request->all();

        $document = $this->documentRepository->create($input);
        // загружаем файлы для документа (если они есть)
        $this->uploadDocumentFiles($document, request()->allFiles());

        Flash::success('Документ создан.');

        return redirect(route('documents.index'));
    }

    /**
     * Display the specified Document.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $document = $this->documentRepository->findWithoutFail($id);

        if (empty($document)) {
            Flash::error('Document not found');

            return redirect(route('documents.index'));
        }

        return view('documents.show')->with('document', $document);
    }

    /**
     * Show the form for editing the specified Document.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $document = $this->documentRepository->findWithoutFail($id);

        if (empty($document)) {
            Flash::error('Document not found');

            return redirect(route('documents.index'));
        }

        return view('documents.edit')->with('document', $document);
    }

    /**
     * Update the specified Document in storage.
     *
     * @param  int              $id
     * @param UpdateDocumentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDocumentRequest $request)
    {
        $document = $this->documentRepository->findWithoutFail($id);

        if (empty($document)) {
            Flash::error('Document not found');

            return redirect(route('documents.index'));
        }

        $document = $this->documentRepository->update($request->all(), $id);
        // загружаем файлы для документа (если они есть)
        $this->uploadDocumentFiles($document, request()->allFiles());

        Flash::success('Документ обновлен.');

        return redirect(route('documents.index'));
    }

    /**
     * Remove the specified Document from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $document = $this->documentRepository->findWithoutFail($id);

        if (empty($document)) {
            Flash::error('Document not found');

            return redirect(route('documents.index'));
        }

        $this->documentRepository->delete($id);

        Flash::success('Документ удален.');

        return redirect(route('documents.index'));
    }
    
    /**
     * Создать документ указанного типа для автомобиля
     * - создает документ
     * - обрабатывает загруженные файлы документа
     * - прикрепляет документ к автомобилю
     * - прикрепляет файлы к документу
     *
     * @param int    $id   id автомобиля
     * @param string $type тип документа (поле slug в модели DocumentType)
     *
     * @return null
     * 
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     *
     * @todo принудительно устанавливать type_id и car_id
     */
    public function createCarDocument($id, $type)
    {
        $input = request()->all();
        if (empty($car = \App\Models\Car::find($id))) {
            Flash::error('Не удалось создать документ: автомобиль не найден id=' . $id);
            
            return redirect(route('cars.index'));
        }
        if (empty($documentType = \App\Models\DocumentType::whereSlug($type)->first())) {
            Flash::error('Не удалось создать документ: неизвестный тип документа: ' . $type);
    
            return redirect('/cars/' . $id . '/edit');
        }
        
        // создаем документ
        $document = $this->documentRepository->create($input);
        // загружаем файлы для документа (если они есть)
        if ($this->uploadDocumentFiles($document, request()->allFiles())) {
            Flash::success('Документ "' . e($document->name) . '" успешно добавлен.<br>' . 
                'Фaйлы документа загружены.');
        } else {
            Flash::success('Документ "' . e($document->name) . '" успешно добавлен.');
        }
        
        return redirect('/cars/' . $id . '/edit');
    }
    
    /**
     * Загрузить файлы документа
     *
     * @param Document $document
     * @param array    $files
     *
     * @return bool
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    protected function uploadDocumentFiles(Document $document, $files)
    {
        if (empty($files['files'])) {
            return false;
        }
        foreach ($files['files'] as $file) {
            $photos[] = $document->uploadFile($file);
        }
    
        return true;
    }
}
