<?php

namespace App\Http\Controllers;

use App\DataTables\CarDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCarRequest;
use App\Http\Requests\UpdateCarRequest;
use App\Repositories\CarRepository;
use App\Models\Car;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class CarController extends AppBaseController
{
    /** @var  CarRepository */
    private $carRepository;

    public function __construct(CarRepository $carRepo)
    {
        $this->carRepository = $carRepo;
    }

    /**
     * Display a listing of the Car.
     *
     * @param CarDataTable $carDataTable
     * @return Response
     */
    public function index(CarDataTable $carDataTable)
    {
        return $carDataTable->render('cars.index');
    }

    /**
     * Show the form for creating a new Car.
     *
     * @return Response
     */
    public function create()
    {
        return view('cars.create');
    }

    /**
     * Store a newly created Car in storage.
     *
     * @param CreateCarRequest $request
     *
     * @return Response
     */
    public function store(CreateCarRequest $request)
    {
        $input = $request->all();
        
        $car = $this->carRepository->create($input);
        
        // загружаем фото для автомобиля (если они есть)
        if ($this->uploadCarPhotos($car, $request->allFiles())) {
            Flash::success('<br>Автомобиль создан.<br>Фотографии автомобиля загружены.');
        } else {
            Flash::success('Автомобиль создан.');
        }
    
        return redirect(route('cars.edit', ['id' => $car->id]));
    }

    /**
     * Display the specified Car.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $car = $this->carRepository->findWithoutFail($id);

        if (empty($car)) {
            Flash::error('Автомобиль не найден');

            return redirect(route('cars.index'));
        }

        return view('cars.show')->with('car', $car);
    }

    /**
     * Show the form for editing the specified Car.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $car = $this->carRepository->findWithoutFail($id);

        if (empty($car)) {
            Flash::error('Автомобиль не найден');

            return redirect(route('cars.index'));
        }

        return view('cars.edit')->with('car', $car);
    }

    /**
     * Update the specified Car in storage.
     *
     * @param  int              $id
     * @param UpdateCarRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCarRequest $request)
    {
        $car = $this->carRepository->findWithoutFail($id);
        
        if (empty($car)) {
            Flash::error('Автомобиль не найден');

            return redirect(route('cars.index'));
        }
        
        $car = $this->carRepository->update($request->all(), $id);
        // загружаем фото для автомобиля (если они есть)
        if ($this->uploadCarPhotos($car, $request->allFiles())) {
            Flash::success('Данные автомобиля обновлены.<br>Фотографии автомобиля загружены.');
        } else {
            Flash::success('Данные автомобиля обновлены.');
        }

        return redirect(route('cars.edit', ['id' => $car->id]));
    }

    /**
     * Remove the specified Car from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $car = $this->carRepository->findWithoutFail($id);

        if (empty($car)) {
            Flash::error('Автомобиль не найден');

            return redirect(route('cars.index'));
        }

        $this->carRepository->delete($id);

        Flash::success('Автомобиль удален.');

        return redirect(route('cars.index'));
    }
    
    /**
     * Загрузить фотографию для автомобиля
     *
     * @param int $id id автомобиля
     *
     * @return null
     * 
     * @todo проверка mime-типа
     * 
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function uploadPhotos($id)
    {
        if (empty($car = $this->carRepository->findWithoutFail($id))) {
            Flash::error('Автомобиль не найден');
            
            return redirect(route('cars.index'));
        }
        // загружаем фото для автомобиля (если они есть)
        $this->uploadCarPhotos($car, request()->allFiles());
        
        return redirect('/cars/' . $car->id);
    }
    
    /**
     * Загрузить фотографии для автомобиля
     *
     * @param Car   $car
     * @param array $files
     *
     * @return bool
     * 
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    protected function uploadCarPhotos(Car $car, $files)
    {
        if (empty($files['photos'])) {
            return false;
        }
        foreach ($files['photos'] as $file) {
            $photos[] = $car->uploadPhotoFile($file);
        }
    
        return true;
    }
}
