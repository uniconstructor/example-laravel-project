<?php

namespace App\Console\Commands;

use App\Models\Car;
use App\Models\Document;
use Illuminate\Console\Command;

class CheckDocumentsExpiration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cars:check-expired-documents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверить истечение срока действия документов для всех автомобилей';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Document::checkDocumentsExpiration();
    }
}
