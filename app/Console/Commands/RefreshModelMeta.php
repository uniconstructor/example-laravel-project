<?php

namespace App\Console\Commands;

use App\Models\Car;
use App\Models\IflatUser;
use App\Models\Department;
use App\Models\Document;
use App\Models\DocumentType;
use Illuminate\Console\Command;

class RefreshModelMeta extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'model:meta:refresh';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить метаданные для всех моделей системы';
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!empty($cars = Car::all())) {
            foreach ($cars as $car) {
                $car->save();
            }
        }
    }
}
