<?php

namespace App\Events\Car;

use App\Models\Car;
use Illuminate\Queue\SerializesModels;

/**
 * Пробег автомобиля достиг значения при котором скоро потребуется ТО
 *
 * @package App\Events\Car
 */
final class MaintenanceMileageNotice
{
    use SerializesModels;
    
    /**
     * @var Car
     */
    public $car;
    
    /**
     * Create a new event instance.
     *
     * @param  Car $car
     *
     * @return void
     */
    public function __construct(Car $car)
    {
        $this->car = $car;
    }
}