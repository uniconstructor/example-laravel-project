<?php

namespace App\Events\Car;

use App\Models\Car;
use App\Models\Document;
use Illuminate\Queue\SerializesModels;

/**
 * Срок действия документа истек
 *
 * @package App\Events\Car
 */
final class DocumentExpired
{
    use SerializesModels;
    
    /**
     * @var Car
     */
    public $car;
    /**
     * @var Document
     */
    public $document;
    
    /**
     * Create a new event instance.
     *
     * @param Car      $car
     * @param Document $document
     *
     * @return void
     */
    public function __construct(Car $car, Document $document)
    {
        $this->car      = $car;
        $this->document = $document;
    }
}