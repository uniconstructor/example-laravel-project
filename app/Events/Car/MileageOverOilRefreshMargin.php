<?php

namespace App\Events\Car;

use App\Models\Car;
use Illuminate\Queue\SerializesModels;

/**
 * Пробег автомобиля превысил значение после которого выводится
 * предупреждение о необходимости замены масла
 *
 * @package App\Events\Car
 */
final class MileageOverOilRefreshMargin
{
    use SerializesModels;
    
    /**
     * @var Car
     */
    public $car;
    
    /**
     * Create a new event instance.
     *
     * @param  Car $car
     *
     * @return void
     */
    public function __construct(Car $car)
    {
        $this->car = $car;
    }
}