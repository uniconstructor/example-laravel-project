<?php

namespace App\Events\Car;

use App\Models\Car;
use Illuminate\Queue\SerializesModels;

/**
 * Пробег автомобиля превысил интервал пробега для замены масла
 *
 * @package App\Events\Car
 */
final class MileageOverOilRefreshInterval
{
    use SerializesModels;
    
    /**
     * @var Car
     */
    public $car;
    
    /**
     * Create a new event instance.
     *
     * @param  Car $car
     *
     * @return void
     */
    public function __construct(Car $car)
    {
        $this->car = $car;
    }
}