<?php

namespace App\Events\Car;

use App\Models\Car;
use Illuminate\Queue\SerializesModels;

/**
 * Пробег автомобиля превысил значение после которого выводится
 * предупреждение о необходимости прохождения ТО
 *
 * @package App\Events\Car
 */
final class MileageOverMaintenanceMargin
{
    use SerializesModels;
    
    /**
     * @var Car
     */
    public $car;
    
    /**
     * Create a new event instance.
     *
     * @param  Car $car
     *
     * @return void
     */
    public function __construct(Car $car)
    {
        $this->car = $car;
    }
}