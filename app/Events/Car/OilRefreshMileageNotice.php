<?php

namespace App\Events\Car;

use App\Models\Car;
use Illuminate\Queue\SerializesModels;

/**
 * Пробег автомобиля достиг значения при котором скоро потребуется замена масла (первое предупреждение)
 *
 * @package App\Events\Car
 */
final class OilRefreshMileageNotice
{
    use SerializesModels;
    
    /**
     * @var Car
     */
    public $car;
    
    /**
     * Create a new event instance.
     *
     * @param  Car $car
     * 
     * @return void
     */
    public function __construct(Car $car)
    {
        $this->car = $car;
    }
}