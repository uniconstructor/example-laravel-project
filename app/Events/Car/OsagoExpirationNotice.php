<?php

namespace App\Events\Car;

use App\Models\Car;
use Illuminate\Queue\SerializesModels;

/**
 * Полис ОСАГО истекает через 2 недели
 *
 * @package App\Events\Car
 */
final class OsagoExpirationNotice
{
    use SerializesModels;
    
    /**
     * @var Car
     */
    public $car;
    
    /**
     * Create a new event instance.
     *
     * @param  Car $car
     *
     * @return void
     */
    public function __construct(Car $car)
    {
        $this->car = $car;
    }
}