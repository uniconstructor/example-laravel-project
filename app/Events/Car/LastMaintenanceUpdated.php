<?php

namespace App\Events\Car;

use App\Models\Car;
use Illuminate\Queue\SerializesModels;

final class LastMaintenanceUpdated
{
    use SerializesModels;
    
    /**
     * @var Car
     */
    public $car;
    /**
     * @var array 
     */
    public $clearNotifications = [];
    
    /**
     * Create a new event instance.
     *
     * @param Car $car
     *
     * @return void
     */
    public function __construct(Car $car)
    {
        $this->car = $car;
        
        $this->clearNotifications[] = class_basename(MileageOverMaintenanceMargin::class);
        $this->clearNotifications[] = class_basename(MileageOverMaintenanceInterval::class);
    }
}