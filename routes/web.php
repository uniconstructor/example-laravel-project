<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(['middleware' => 'auth'], function($router) {
    // интерфейс управления автомобилями
    Route::resource('cars', 'CarController');
    // загрузка фото для автомобиля
    Route::post('/cars/{id}/uploadPhoto', 'CarController@uploadPhotos');
    // просмотр фото автомобиля
    Route::post('/cars/{id}/photo/{photoId}', 'CarController@viewPhoto');
    // загрузка файлов документа для автомобиля
    Route::post('/cars/{id}/uploadDocumentFile/{documentId}', 'CarController@uploadDocumentFiles');
    
    // интерфейс просмотра подразделений
    Route::resource('departments', 'DepartmentController');
    
    // интерфейс просмотра пользователей
    Route::resource('iflatUsers', 'IflatUserController');
    
    // интерфейс работы со списком документов
    Route::resource('documentTypes', 'DocumentTypeController');
    
    // интерфейс работы с документами
    Route::resource('documents', 'DocumentController');
    // создать новый документ указанного типа для автомобиля
    Route::post('/documents/{id}/createCarDocument/{type}', 'DocumentController@createCarDocument');
    
    // загруженные файлы
    // просмотр в браузере
    //Route::get('/media/{id}/view', 'MediaController@viewMedia');
    Route::get('/media/{id}/view/{conversion?}', 'MediaController@viewMedia');
    // скачивание файла
    Route::get('/media/{id}/download', 'MediaController@downloadMedia');
    // удаление файла
    Route::delete('/media/{id}/delete', 'MediaController@deleteMedia');
    
    // типы оповещений
    Route::resource('carNotificationTypes', 'CarNotificationTypeController');
});

// просмотр логов (только для разработки и тестирования)
// @see https://github.com/rap2hpoutre/laravel-log-viewer
Route::get('/systemLogs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware('auth');