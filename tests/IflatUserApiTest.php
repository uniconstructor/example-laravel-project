<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IflatUserApiTest extends TestCase
{
    use MakeIflatUserTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateIflatUser()
    {
        $iflatUser = $this->fakeIflatUserData();
        $this->json('POST', '/api/iflatUsers', $iflatUser);

        $this->assertApiResponse($iflatUser);
    }

    /**
     * @test
     */
    public function testReadIflatUser()
    {
        $iflatUser = $this->makeIflatUser();
        $this->json('GET', '/api/iflatUsers/'.$iflatUser->id);

        $this->assertApiResponse($iflatUser->toArray());
    }

    /**
     * @test
     */
    public function testUpdateIflatUser()
    {
        $iflatUser = $this->makeIflatUser();
        $editedIflatUser = $this->fakeIflatUserData();

        $this->json('PUT', '/api/iflatUsers/'.$iflatUser->id, $editedIflatUser);

        $this->assertApiResponse($editedIflatUser);
    }

    /**
     * @test
     */
    public function testDeleteIflatUser()
    {
        $iflatUser = $this->makeIflatUser();
        $this->json('DELETE', '/api/iflatUsers/'.$iflatUser->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/iflatUsers/'.$iflatUser->id);

        $this->assertResponseStatus(404);
    }
}
