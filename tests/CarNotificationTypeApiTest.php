<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CarNotificationTypeApiTest extends TestCase
{
    use MakeCarNotificationTypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCarNotificationType()
    {
        $carNotificationType = $this->fakeCarNotificationTypeData();
        $this->json('POST', '/api/carNotificationTypes', $carNotificationType);

        $this->assertApiResponse($carNotificationType);
    }

    /**
     * @test
     */
    public function testReadCarNotificationType()
    {
        $carNotificationType = $this->makeCarNotificationType();
        $this->json('GET', '/api/carNotificationTypes/'.$carNotificationType->id);

        $this->assertApiResponse($carNotificationType->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCarNotificationType()
    {
        $carNotificationType = $this->makeCarNotificationType();
        $editedCarNotificationType = $this->fakeCarNotificationTypeData();

        $this->json('PUT', '/api/carNotificationTypes/'.$carNotificationType->id, $editedCarNotificationType);

        $this->assertApiResponse($editedCarNotificationType);
    }

    /**
     * @test
     */
    public function testDeleteCarNotificationType()
    {
        $carNotificationType = $this->makeCarNotificationType();
        $this->json('DELETE', '/api/carNotificationTypes/'.$carNotificationType->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/carNotificationTypes/'.$carNotificationType->id);

        $this->assertResponseStatus(404);
    }
}
