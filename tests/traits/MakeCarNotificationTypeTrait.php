<?php

use Faker\Factory as Faker;
use App\Models\CarNotificationType;
use App\Repositories\CarNotificationTypeRepository;

trait MakeCarNotificationTypeTrait
{
    /**
     * Create fake instance of CarNotificationType and save it in database
     *
     * @param array $carNotificationTypeFields
     * @return CarNotificationType
     */
    public function makeCarNotificationType($carNotificationTypeFields = [])
    {
        /** @var CarNotificationTypeRepository $carNotificationTypeRepo */
        $carNotificationTypeRepo = App::make(CarNotificationTypeRepository::class);
        $theme = $this->fakeCarNotificationTypeData($carNotificationTypeFields);
        return $carNotificationTypeRepo->create($theme);
    }

    /**
     * Get fake instance of CarNotificationType
     *
     * @param array $carNotificationTypeFields
     * @return CarNotificationType
     */
    public function fakeCarNotificationType($carNotificationTypeFields = [])
    {
        return new CarNotificationType($this->fakeCarNotificationTypeData($carNotificationTypeFields));
    }

    /**
     * Get fake data of CarNotificationType
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCarNotificationTypeData($carNotificationTypeFields = [])
    {
        $fake = Faker::create('ru_RU');

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->text,
            'trigger_event' => $fake->word,
            'subject_template' => $fake->word,
            'action_text_template' => $fake->word,
            'action_url_template' => $fake->word,
            'email_template' => $fake->word,
            'sms_template' => $fake->word,
            'slack_template' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $carNotificationTypeFields);
    }
}
