<?php

use Faker\Factory as Faker;
use App\Models\IflatUser;
use App\Repositories\IflatUserRepository;

trait MakeIflatUserTrait
{
    /**
     * Create fake instance of IflatUser and save it in database
     *
     * @param array $iflatUserFields
     *
     * @return IflatUser
     */
    public function makeIflatUser($iflatUserFields = [])
    {
        /** @var IflatUserRepository $iflatUserRepo */
        $iflatUserRepo = App::make(IflatUserRepository::class);
        $theme         = $this->fakeIflatUserData($iflatUserFields);
        
        return $iflatUserRepo->create($theme);
    }
    
    /**
     * Get fake instance of IflatUser
     *
     * @param array $iflatUserFields
     *
     * @return IflatUser
     */
    public function fakeIflatUser($iflatUserFields = [])
    {
        return new IflatUser($this->fakeIflatUserData($iflatUserFields));
    }
    
    /**
     * Get fake data of IflatUser
     *
     * @param array $postFields
     *
     * @return array
     */
    public function fakeIflatUserData($iflatUserFields = [])
    {
        $fake = Faker::create('ru_RU');
        
        return array_merge(
            [
                'department_id' => 1,
                'firstname'     => $fake->firstName(),
                'lastname'      => $fake->lastName,
                'middlename'    => '-',
                'email'         => $fake->email,
                'phone'         => $fake->phoneNumber,
                'description'   => $fake->text,
                'position'      => $fake->word,
                'status'        => 'active',
            ], $iflatUserFields
        );
    }
}
