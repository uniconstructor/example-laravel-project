<?php

use Faker\Factory as Faker;
use App\Models\Document;
use App\Repositories\DocumentRepository;

trait MakeDocumentTrait
{
    /**
     * Create fake instance of Document and save it in database
     *
     * @param array $documentFields
     * @return Document
     */
    public function makeDocument($documentFields = [])
    {
        /** @var DocumentRepository $documentRepo */
        $documentRepo = App::make(DocumentRepository::class);
        $theme = $this->fakeDocumentData($documentFields);
        return $documentRepo->create($theme);
    }

    /**
     * Get fake instance of Document
     *
     * @param array $documentFields
     * @return Document
     */
    public function fakeDocument($documentFields = [])
    {
        return new Document($this->fakeDocumentData($documentFields));
    }

    /**
     * Get fake data of Document
     *
     * @param array $postFields
     * @return array
     */
    public function fakeDocumentData($documentFields = [])
    {
        $fake = Faker::create('ru_RU');

        return array_merge([
            'name' => $fake->word,
            'slug' => $fake->word,
            'description' => $fake->text,
            'type_id' => $fake->randomDigitNotNull,
            'car_id' => $fake->randomDigitNotNull,
            'creator_id' => $fake->randomDigitNotNull,
            'start_date' => $fake->word,
            'end_date' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $documentFields);
    }
}
