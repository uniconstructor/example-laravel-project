<?php

use Faker\Factory as Faker;
use App\Models\Car;
use App\Repositories\CarRepository;

trait MakeCarTrait
{
    /**
     * Create fake instance of Car and save it in database
     *
     * @param array $carFields
     *
     * @return Car
     */
    public function makeCar($carFields = [])
    {
        /** @var CarRepository $carRepo */
        $carRepo = App::make(CarRepository::class);
        $theme   = $this->fakeCarData($carFields);
        
        return $carRepo->create($theme);
    }
    
    /**
     * Get fake instance of Car
     *
     * @param array $carFields
     *
     * @return Car
     */
    public function fakeCar($carFields = [])
    {
        return new Car($this->fakeCarData($carFields));
    }
    
    /**
     * Get fake data of Car
     *
     * @param array $postFields
     *
     * @return array
     */
    public function fakeCarData($carFields = [])
    {
        $fake      = Faker::create('ru_RU');
        $regNumber = $fake->randomLetter . $fake->randomNumber(3) . $fake->randomLetter . $fake->randomLetter;
        
        return array_merge(
            [
                'vendor'        => $fake->word,
                'model'         => $fake->word,
                'reg_number'    => $regNumber,
                'description'   => $fake->text,
                'department_id' => 0,
                'iflat_user_id' => 0,
                'status'        => 'draft',
            ], $carFields
        );
    }
}
