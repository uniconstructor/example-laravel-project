<?php

use App\Models\IflatUser;
use App\Repositories\IflatUserRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IflatUserRepositoryTest extends TestCase
{
    use MakeIflatUserTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var IflatUserRepository
     */
    protected $iflatUserRepo;

    public function setUp()
    {
        parent::setUp();
        $this->iflatUserRepo = App::make(IflatUserRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateIflatUser()
    {
        $iflatUser = $this->fakeIflatUserData();
        $createdIflatUser = $this->iflatUserRepo->create($iflatUser);
        $createdIflatUser = $createdIflatUser->toArray();
        $this->assertArrayHasKey('id', $createdIflatUser);
        $this->assertNotNull($createdIflatUser['id'], 'Created IflatUser must have id specified');
        $this->assertNotNull(IflatUser::find($createdIflatUser['id']), 'IflatUser with given id must be in DB');
        $this->assertModelData($iflatUser, $createdIflatUser);
    }

    /**
     * @test read
     */
    public function testReadIflatUser()
    {
        $iflatUser = $this->makeIflatUser();
        $dbIflatUser = $this->iflatUserRepo->find($iflatUser->id);
        $dbIflatUser = $dbIflatUser->toArray();
        $this->assertModelData($iflatUser->toArray(), $dbIflatUser);
    }

    /**
     * @test update
     */
    public function testUpdateIflatUser()
    {
        $iflatUser = $this->makeIflatUser();
        $fakeIflatUser = $this->fakeIflatUserData();
        $updatedIflatUser = $this->iflatUserRepo->update($fakeIflatUser, $iflatUser->id);
        $this->assertModelData($fakeIflatUser, $updatedIflatUser->toArray());
        $dbIflatUser = $this->iflatUserRepo->find($iflatUser->id);
        $this->assertModelData($fakeIflatUser, $dbIflatUser->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteIflatUser()
    {
        $iflatUser = $this->makeIflatUser();
        $resp = $this->iflatUserRepo->delete($iflatUser->id);
        $this->assertTrue($resp);
        $this->assertNull(IflatUser::find($iflatUser->id), 'IflatUser should not exist in DB');
    }
}
