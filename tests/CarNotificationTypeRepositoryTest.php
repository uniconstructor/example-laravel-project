<?php

use App\Models\CarNotificationType;
use App\Repositories\CarNotificationTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CarNotificationTypeRepositoryTest extends TestCase
{
    use MakeCarNotificationTypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CarNotificationTypeRepository
     */
    protected $carNotificationTypeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->carNotificationTypeRepo = App::make(CarNotificationTypeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCarNotificationType()
    {
        $carNotificationType = $this->fakeCarNotificationTypeData();
        $createdCarNotificationType = $this->carNotificationTypeRepo->create($carNotificationType);
        $createdCarNotificationType = $createdCarNotificationType->toArray();
        $this->assertArrayHasKey('id', $createdCarNotificationType);
        $this->assertNotNull($createdCarNotificationType['id'], 'Created CarNotificationType must have id specified');
        $this->assertNotNull(CarNotificationType::find($createdCarNotificationType['id']), 'CarNotificationType with given id must be in DB');
        $this->assertModelData($carNotificationType, $createdCarNotificationType);
    }

    /**
     * @test read
     */
    public function testReadCarNotificationType()
    {
        $carNotificationType = $this->makeCarNotificationType();
        $dbCarNotificationType = $this->carNotificationTypeRepo->find($carNotificationType->id);
        $dbCarNotificationType = $dbCarNotificationType->toArray();
        $this->assertModelData($carNotificationType->toArray(), $dbCarNotificationType);
    }

    /**
     * @test update
     */
    public function testUpdateCarNotificationType()
    {
        $carNotificationType = $this->makeCarNotificationType();
        $fakeCarNotificationType = $this->fakeCarNotificationTypeData();
        $updatedCarNotificationType = $this->carNotificationTypeRepo->update($fakeCarNotificationType, $carNotificationType->id);
        $this->assertModelData($fakeCarNotificationType, $updatedCarNotificationType->toArray());
        $dbCarNotificationType = $this->carNotificationTypeRepo->find($carNotificationType->id);
        $this->assertModelData($fakeCarNotificationType, $dbCarNotificationType->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCarNotificationType()
    {
        $carNotificationType = $this->makeCarNotificationType();
        $resp = $this->carNotificationTypeRepo->delete($carNotificationType->id);
        $this->assertTrue($resp);
        $this->assertNull(CarNotificationType::find($carNotificationType->id), 'CarNotificationType should not exist in DB');
    }
}
