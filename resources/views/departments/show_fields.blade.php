<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $department->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Название:') !!}
    <p>{!! $department->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Описание:') !!}
    <p>{!! $department->description !!}</p>
</div>

<!-- Parent Id Field -->
<!--div class="form-group">
    {!! Form::label('parent_id', 'Parent Id:') !!}
    <p>{!! $department->parent_id !!}</p>
</div-->

<!-- Автомобили подразделения -->
<div class="form-group">
    {!! Form::label('cars', 'Автомобили:') !!}
    @if(count($department->cars))
        <ul>
            @foreach($department->cars as $car)
                <li>
                    <a href="/cars/{{$car->id}}">{{ $car->vendor }} {{ $car->model }}</a>
                    {{ $car->reg_number }}
                </li>
            @endforeach
        </ul>
    @else
        <p>[Нет]</p>
    @endif
</div>
