@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <iframe src="/laravel-filemanager" style="width: 100%; height: 700px; overflow: scroll; border: none;"></iframe>
        </div>
    </div>
@endsection
