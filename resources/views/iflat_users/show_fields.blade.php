<!-- Id Field -->
<!--div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $iflatUser->id !!}</p>
</div-->

<!-- Department Id Field -->
<!--div class="form-group">
    {!! Form::label('department_id', 'Department Id:') !!}
    <p>{!! $iflatUser->department_id !!}</p>
</div-->

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $iflatUser->email !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Телефон:') !!}
    <p>{!! $iflatUser->phone !!}</p>
</div>

<!-- Description Field -->
@if($iflatUser->description)
<div class="form-group">
    {!! Form::label('description', 'Описание:') !!}
    <p>{!! $iflatUser->description !!}</p>
</div>
@endif

<!-- Position Field -->
<div class="form-group">
    {!! Form::label('position', 'Должность:') !!}
    <p>{!! $iflatUser->position !!}</p>
</div>

<div class="form-group">
    {!! Form::label('cars', 'Автомобили:') !!}
    @if(count($iflatUser->cars))
        <ul>
        @foreach($iflatUser->cars as $car)
            <li>
                <a href="/cars/{{$car->id}}">{{ $car->vendor }} {{ $car->model }}</a>
                {{ $car->reg_number }}
            </li>
        @endforeach
        </ul>
    @else
        <p>[Нет]</p>
    @endif
</div>

<!-- Status Field -->
<!--div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $iflatUser->status !!}</p>
</div-->

