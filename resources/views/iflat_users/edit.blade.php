@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            {!! $iflatUser->name !!}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($iflatUser, ['route' => ['iflatUsers.update', $iflatUser->id], 'method' => 'patch']) !!}

                        @include('iflat_users.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection