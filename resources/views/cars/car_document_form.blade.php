<?php
/** @var \App\Models\Car $car */
/** @var \App\Models\Document $document */
/** @var \App\Models\DocumentType $documentType */
$newDocument = new \App\Models\Document();
//$document    = $car->documents()->whereTypeId($documentType->id)->orderBy('id', 'desc')->first();
$defaultName    = \App\Models\Document::newDocumentName($documentType, $car);
$defaultEndDate = \App\Models\Document::newDocumentEndDate();
// @todo возможность редактировать ранее добавленные документы
?>
<div class="col-md-12">
    <hr>
    <h2 class="text-center">{{ $documentType->name }}</h2>
    <div class="">
        <div class="col-sm-6">
            <h3 class="text-center">Список</h3>
            <div class="">
                @if($car->hasDocumentType($documentType->slug))
                    <ol>
                    @foreach($car->documents()->whereTypeId($documentType->id)->latest()->get() as $document)
                        <li>
                            <a href="/documents/{{ $document->id }}">{{ $document->name }}</a>&nbsp;
                            [{{ $document->created_at->format('Y-m-d') }}]
                        </li>
                    @endforeach
                    </ol>
                @else
                    <div class="alert alert-info text-center">(пусто)</div>
                @endif
            </div>
        </div>
        
        <div class="col-sm-6">
            <h3 class="text-center">Создать</h3>
            <div class="">
                <!-- всегда выводим форму добавления нового документа -->
                {!! Form::open(['url' => "/documents/{$car->id}/createCarDocument/{$documentType->slug}", 'enctype' => 'multipart/form-data']) !!}
                
                {!! Form::hidden('type_id', $documentType->id) !!}
                {!! Form::hidden('car_id', $car->id) !!}
                <div class="form-group col-sm-6">
                    {!! Form::label('start_date', 'Дата начала действия:') !!}
                    {!! Form::date('start_date', date('Y-m-d'), ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('end_date', 'Дата окончания действия:') !!}
                    {!! Form::date('end_date', $defaultEndDate, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-6">
                    {!! Form::label('name', 'Название:') !!}
                    {!! Form::text('name', $defaultName, ['class' => 'form-control', 'placeholder' => $defaultName]) !!}
                </div>
                <!-- Загрузка файлов документа -->
                <div class="form-group col-sm-6">
                    <label for="files">Файлы:</label><br>
                    <span class="btn btn-success fileinput-button">
                        <i class="glyphicon glyphicon-plus"></i>
                        <span>Добавить файлы...</span>
                        <input type="file" name="files[]" multiple>
                    </span>
                </div>
                <!-- Описание -->
                <div class="form-group col-sm-12">
                    {!! Form::label('description', 'Описание:') !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group col-sm-12">
                    {!! Form::submit('Создать ' . $documentType->name , ['class' => 'btn btn-primary']) !!}
                </div>
                
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
