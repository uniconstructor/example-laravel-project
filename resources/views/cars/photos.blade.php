<?php
/** @var $car \App\Models\Car */
/** @var $photo \Spatie\MediaLibrary\Media */
?>
@if($car->exists && $car->hasPhoto())
    <!-- Список фото -->
    <div class="form-group col-sm-12">
        <h4>Фотографии автомобиля:</h4>
        <ol>
            @foreach($car->getPhoto() as $photo)
                <li id="media_list_item_{{$photo->id}}">
                    <a href="/media/{{$photo->id}}/view" target="_blank"
                       title="{{$photo->getCustomProperty('localName')}} {{$loop->iteration}}">
                        {{$photo->getCustomProperty('originalName')}} {{$loop->iteration}}
                    </a>
                    &nbsp;
                    <a href="/media/{{$photo->id}}/download" class="btn btn-success btn-sm">
                        <i class="glyphicon glyphicon-save"></i> Cкачать
                    </a>
                    &nbsp;
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i> Удалить', [
                        'type'    => 'button',
                        'class'   => 'btn btn-danger btn-sm',
                        'onclick' => "deleteMedia({$photo->id});"
                    ]) !!}
                </li>
            @endforeach
        </ol>
        <a href="#" id="car_photo_gallery" class="btn btn-info">Открыть галерею</a>
    </div>

    <script>
        /**
         * Открыть фотогалерею
         */
        var openPhotoSwipe = function() {
            var pswpElement = document.querySelectorAll('.pswp')[0];
        
            // build items array
            var items = [
                @foreach($car->getPhoto() as $photo)
                {
                    'src' : '/media/{{$photo->id}}/view',
                    'w'   : '{{$photo->getCustomProperty('width')}}',
                    'h'   : '{{$photo->getCustomProperty('height')}}'
                },
                @endforeach
            ];
        
            // define PhotoSwipe gallery options
            var options = {
                // history & focus options are disabled
                history               : false,
                focus                 : false,
                showAnimationDuration : 0,
                hideAnimationDuration : 0
            };
        
            var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
            gallery.init();
            
            return false;
        };

        document.getElementById('car_photo_gallery').onclick = openPhotoSwipe;
    </script>
@endif

@include('gallery')