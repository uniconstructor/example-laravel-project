@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Редактировать автомобиль
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       @include('flash::message')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($car, ['route' => ['cars.update', $car->id], 'method' => 'patch', 'enctype' => 'multipart/form-data']) !!}

                        @include('cars.fields')

                   {!! Form::close() !!}
                   
                   <div class="col-sm-12">
                       <h2 class="text-center">Документы</h2>
                   </div>
                   @foreach(\App\Models\DocumentType::all() as $documentType)
                       @include('cars.car_document_form', ['car' => $car, 'documentType' => $documentType])
                   @endforeach
               </div>
           </div>
       </div>
   </div>
@endsection