<?php
/** @var $car \App\Models\Car */
?>
<!-- Vendor Field -->
<!--div class="form-group">
    {!! Form::label('vendor', 'Vendor:') !!}
        <p>{!! $car->vendor !!}</p>
</div-->

<!-- Model Field -->
<!--div class="form-group">
    {!! Form::label('model', 'Model:') !!}
        <p>{!! $car->model !!}</p>
</div-->

<!-- Reg Number Field -->
<div class="form-group col-md-3">
    {!! Form::label('reg_number', 'Регистрационный номер:') !!}
    <p>{!! $car->reg_number !!}</p>
</div>

<!-- Status Field -->
<div class="form-group col-md-3">
    {!! Form::label('status', 'Статус:') !!}
    <p>{!! $car->status_text !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group col-md-3">
    {!! Form::label('created_at', 'Дата создания:') !!}
    <p>{!! $car->created_at->format('Y-m-d H:i:s') !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group col-md-3">
    {!! Form::label('updated_at', 'Последнее изменение:') !!}
    <p>{!! $car->updated_at->format('Y-m-d H:i:s') !!}</p>
</div>

<!-- Description Field -->
<div class="form-group col-md-12">
    {!! Form::label('description', 'Описание:') !!}
    <p>{!! $car->description !!}</p>
</div>

<div class="form-group col-md-6">
    {!! Form::label('department', 'Подразделение:') !!}
    @if($car->department)
        <p><a href="/departments/{{$car->department->id}}">{!! $car->department->name !!}</a></p>
    @else
        <p>[Нет]</p>
    @endif
    {!! Form::label('iflat_user', 'Сотрудник:') !!}
    @if($car->iflatUser)
        <p>
            <a href="/iflatUsers/{{$car->iflatUser->id}}">{!! $car->iflatUser->fullname !!}</a>
            <br>
            ({{ $car->iflatUser->position }})
        </p>
    @else
        <p>[Нет]</p>
    @endif
</div>

<!-- список документов -->
<div class="form-group col-md-6">
    {!! Form::label('documents', 'Документы на этот автомобиль:') !!}
    @if(count($car->documents))
        <ul>
            @foreach($car->documents as $document)
                <li>
                    <a href="/documents/{{$document->id}}">
                        [{{$document->id}}]: {{ $document->name }}
                    </a>
                    @if($document->end_date && !str_contains('2037', $document->end_date))
                        (Истекает {{$document->end_date}})
                    @endif
                </li>
            @endforeach
        </ul>
    @else
        <p>[Нет]</p>
    @endif
</div>


<div class="form-group col-md-12">
    {!! Form::label('mileage', 'Пробег:') !!}
    <p>{{ $car->mileage }} км</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('last_maintenance', 'Последнее ТО:') !!}
    <p>{{ $car->last_maintenance }} км</p>
    {!! Form::label('next_maintenance', 'Следующее ТО:') !!}
    <p>{{ ($car->last_maintenance + $car->maintenance_interval) }} км</p>
    {!! Form::label('maintenance_interval', 'Интервал ТО:') !!}
    <p>{{ $car->maintenance_interval }} км</p>
    {!! Form::label('maintenance_notification_margin', 'Оповещение о необходимости ТО:') !!}
    <p>За {{ $car->maintenance_notification_margin }} км</p>
</div>
<div class="form-group col-md-6">
    {!! Form::label('last_oil_refresh', 'Последняя замена масла:') !!}
    <p>{{ $car->last_oil_refresh }} км</p>
    {!! Form::label('next_oil_refresh', 'Следующая замена масла:') !!}
    <p>{{ ($car->last_oil_refresh + $car->oil_refresh_interval) }} км</p>
    {!! Form::label('oil_refresh_interval', 'Интервал замены масла:') !!}
    <p>{{ $car->oil_refresh_interval }} км</p>
    {!! Form::label('oil_notification_margin', 'Оповещение о замене масла:') !!}
    <p>За {{ $car->oil_notification_margin }} км</p>
</div>


<!-- Список фото -->
@include('cars.photos', ['car' => $car])

@section('scripts')
    @parent
    <script>
        
    </script>
@endsection