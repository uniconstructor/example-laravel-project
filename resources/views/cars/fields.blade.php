<?php
// список подразделений (для выпадающего списка)
$departments = \App\Models\Department::orderBy('name')->get()
    ->keyBy('id')
    ->map(
        function ($department) {
            return $department->name;
        }
    )
    ->prepend('Нет', '0')
    ->toArray();
// список сотрудников (для выпадающего списка)
$iflatUsers = \App\Models\IflatUser::orderBy('lastname')->get()
    ->keyBy('id')
    ->map(
        function ($user) {
            return $user->lastname . ' ' . $user->firstname . ' (' . $user->position . ')';
        }
    )
    ->prepend('Нет', '0')
    ->toArray();

if (empty($car)) {
    $car = new \App\Models\Car();
}
?>
{!! Form::hidden('id', $car->id) !!}
<!-- Vendor Field -->
<div class="form-group col-sm-6">
    {!! Form::label('vendor', 'Марка:') !!}
    {!! Form::text('vendor', null, ['class' => 'form-control', 'required' => true]) !!}
</div>

<!-- Model Field -->
<div class="form-group col-sm-6">
    {!! Form::label('model', 'Модель:') !!}
    {!! Form::text('model', null, ['class' => 'form-control', 'required' => true]) !!}
</div>

<!-- Reg Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('reg_number', 'Регистрационный номер:') !!}
    {!! Form::text('reg_number', null, [
            'class'       => 'form-control',
            'required'    => true,
            'placeholder' => 'х888хх199',
            'minlength'   => 8,
            'maxlength'   => 9,
            'pattern'     => '[а-яА-Я]{1}[0-9]{3}[а-яА-Я]{2}[0-9]{2,3}',
        ])
    !!}
    Вводится в формате <b>х888хх99</b> или <b>х888хх199</b>, без пробелов, 
    с номером региона (2 или 3 цифры), все буквы номера русские.<br>
    Длина значения от 8 до 9 символов.
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Описание:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('department_id', 'Подразделение:') !!}
            {!! Form::select('department_id', $departments, $car->department_id) !!}
        </div>

        <div class="form-group">
            {!! Form::label('iflat_user_id', 'Сотрудник:') !!}
            {!! Form::select('iflat_user_id', $iflatUsers, $car->iflat_user_id) !!}
        </div>
    
        <!-- Status Field -->
        <div class="form-group">
            {!! Form::label('status', 'Статус:') !!}
            {!! Form::select('status', \App\Models\Car::$statusList, $car->status) !!}
        </div>
    </div>
    <div class="col-md-6">
        <!-- Список фото -->
        @include('cars.photos', ['car' => $car])

        <!-- Загрузка фото -->
        <div class="form-group">
            <span class="btn btn-success fileinput-button">
                <i class="glyphicon glyphicon-plus"></i>
                <span>Добавить фото...</span>
                <input type="file" name="photos[]" multiple>
            </span>
        </div>
    </div>

<hr>
<div class="form-group col-sm-12">
    {!! Form::label('mileage', 'Пробег:') !!}
    {!! Form::text('mileage', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-4">
    {!! Form::label('last_maintenance', 'Пробег на момент последнего ТО:') !!}
    {!! Form::text('last_maintenance', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-4">
    {!! Form::label('maintenance_interval', 'Интервал ТО (км):') !!}
    {!! Form::text('maintenance_interval', $car->maintenance_interval ?: 15000, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-4">
    {!! Form::label('maintenance_notification_margin', 'Оповещение до окончания ТО (км):') !!}
    {!! Form::text('maintenance_notification_margin', $car->maintenance_notification_margin ?: 1000, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-4">
    {!! Form::label('last_oil_refresh', 'Пробег на момент последней замены масла:') !!}
    {!! Form::text('last_oil_refresh', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-4">
    {!! Form::label('oil_refresh_interval', 'Интервал замены масла (км):') !!}
    {!! Form::text('oil_refresh_interval', $car->oil_refresh_interval ?: 10000, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-4">
    {!! Form::label('oil_notification_margin', 'Оповещение о замене масла (осталось км):') !!}
    {!! Form::text('oil_notification_margin', $car->oil_notification_margin ?: 1000, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('cars.index') !!}" class="btn btn-default">Отмена</a>
</div>
