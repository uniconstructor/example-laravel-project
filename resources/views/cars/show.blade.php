@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            {!! $car->vendor !!} {!! $car->model !!}
        </h1>
    </section>
    <div class="content">
        @include('flash::message')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    
                    @include('cars.show_fields')
                    <div class="form-group col-md-12">
                        <a href="{!! route('cars.index') !!}" class="btn btn-default">К списку</a>
                        <a href="/cars/{{ $car->id }}/edit" class="btn btn-warning">Редактировать</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
