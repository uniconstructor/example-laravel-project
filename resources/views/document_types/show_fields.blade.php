<!-- Id Field -->
<!--div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $documentType->id !!}</p>
</div-->

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Название:') !!}
    <p>{!! $documentType->name !!}</p>
</div>

<!-- Slug Field -->
<div class="form-group">
    {!! Form::label('slug', 'Код:') !!}
    <p>{!! $documentType->slug !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Описание:') !!}
    <p>{!! $documentType->description !!}</p>
</div>

<!-- Parent Id Field -->
<!--div class="form-group">
    {!! Form::label('parent_id', 'Вышестоящий тип:') !!}
    <p>{!! $documentType->parent_id !!}</p>
</div-->

<!-- Creator Id Field -->
<!--div class="form-group">
    {!! Form::label('creator_id', 'Создан пользователем:') !!}
    <p>{!! $documentType->creator ? $documentType->creator->name : '' !!}</p>
</div-->

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Дата создания:') !!}
    <p>{!! $documentType->created_at->format('Y-m-d H:i:s') !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Последнее изменение:') !!}
    <p>{!! $documentType->updated_at->format('Y-m-d H:i:s') !!}</p>
</div>

<div class="form-group">
    {!! Form::label('cars', 'Все документы этого типа:') !!}
    @if(count($documentType->documents))
        <ul>
            @foreach($documentType->documents as $document)
                <li>
                    <a href="/documents/{{$document->id}}">
                        [{{$document->id}}]:{{ $document->name }}
                    </a>
                    для
                    <a href="/cars/{{$document->car->id}}">
                        {{ $document->car->vendor }} {{ $document->car->model }}
                    </a>
                    [{{ $document->car->reg_number }}]
                </li>
            @endforeach
        </ul>
    @else
        <p>[Нет]</p>
    @endif
</div>

