{!! Form::open(['route' => ['documentTypes.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('documentTypes.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{{ route('documentTypes.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    @if(\App\Models\DocumentType::findOrFail($id)->canBeDeleted())
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type'    => 'submit',
        'class'   => 'btn btn-danger btn-xs',
        'onclick' => "return confirm('Удалить этот тип документа?')"
    ])
    !!}
    @endif
</div>
{!! Form::close() !!}
