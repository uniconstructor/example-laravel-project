<?php
/** @var \App\Models\DocumentType $documentType */
// запрещаем изменять код типа документа если для него создан хотя бы один документ
$disableSlug = true;
if (empty($documentType) || $documentType->canBeDeleted()) {
    $disableSlug = false;
}
?>
{!! Form::hidden('id', empty($documentType) ? 0 : $documentType->id) !!}
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => true]) !!}
</div>

<!-- Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('slug', 'Код:') !!}
    {!!
        Form::text('slug', null, [
            'class'    => 'form-control',
            'disabled' => $disableSlug,
            'pattern'  => '[A-Za-z0-9_]+',
        ])
    !!}
    Используется для генерации стандартных имен файлов, прикрепляемых к документам этого типа.<br>
    Формат: только латинские буквы, цифры, и знак подчеркивания, без пробелов.<br>
    При создании это поле можно оставить пустым, тогда оно будет заполнено автоматически.<br>
    Это поле нельзя редактировать если был создан хотя бы 1 документ этого типа.<br>
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Описание:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- TODO Вышестоящий тип документа (для иерархии) -->
<!--div class="form-group col-sm-6">
    {-- Form::label('parent_id', 'Вышестоящий тип:') --}
    {-- Form::number('parent_id', null, ['class' => 'form-control']) --}
</div-->


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('documentTypes.index') !!}" class="btn btn-default">Отмена</a>
</div>
