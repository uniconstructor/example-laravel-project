@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            {!! $documentType->name !!}
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            @include('flash::message')
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('document_types.show_fields')
                    <a href="{!! route('documentTypes.index') !!}" class="btn btn-default">К списку</a>
                    <a href="/documentTypes/{{ $documentType->id }}/edit" class="btn btn-warning">Редактировать</a>
                </div>
            </div>
        </div>
    </div>
@endsection
