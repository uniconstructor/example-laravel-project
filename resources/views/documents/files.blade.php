<?php
/** @var $document \App\Models\Document */
/** @var $file     \Spatie\MediaLibrary\Media */
?>
@if($document->exists && $document->hasFiles())
    <!-- Список файлов документа -->
    <div class="form-group col-sm-12">
        <h4>Загруженные файлы:</h4>
        <ol>
        @foreach($document->getFiles() as $file)
            <li id="media_list_item_{{$file->id}}">
                <a href="/media/{{$file->id}}/view" target="_blank"
                   title="{{$file->getCustomProperty('localName')}} {{$loop->iteration}}">
                    {{$file->getCustomProperty('originalName')}}
                </a>
                &nbsp;
                <a href="/media/{{$file->id}}/download" class="btn btn-success btn-sm">
                    <i class="glyphicon glyphicon-save"></i> Cкачать
                </a>
                &nbsp;
                {!! Form::button('<i class="glyphicon glyphicon-trash"></i> Удалить', [
                    'type'    => 'button',
                    'class'   => 'btn btn-danger btn-sm',
                    'onclick' => "deleteMedia({$file->id});"
                ]) !!}
            </li>
        @endforeach
        </ol>
        <button id="document_{{$document->id}}_photo_gallery" class="btn btn-info">Открыть галерею</button>
    </div>

    <script>
      var openPhotoSwipe = function() {
        var pswpElement = document.querySelectorAll('.pswp')[0];
    
        // build items array
        var items = [
          @foreach($document->getImages() as $image)
          {
            'src' : '/media/{{$file->id}}/view',
            'w'   : '{{$image->getCustomProperty('width')}}',
            'h'   : '{{$image->getCustomProperty('height')}}'
          },
          @endforeach
        ];
    
        // define PhotoSwipe gallery options
        var options = {
          // history & focus options are disabled
          history               : false,
          focus                 : false,
          showAnimationDuration : 0,
          hideAnimationDuration : 0
        };
    
        var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
      };
  
      document.getElementById('document_{{$document->id}}_photo_gallery').onclick = openPhotoSwipe;
    </script>
@endif

@include('gallery')