<?php

$cars = \App\Models\Car::orderBy('vendor')->get()
    ->keyBy('id')
    ->map(
        function (\App\Models\Car $car) {
            return $car->vendor . ' ' . $car->model. ' ' . '[' . $car->reg_number . ']';
        }
    )
    ->prepend('[Автомобиль]', '')
    ->toArray();

$types = \App\Models\DocumentType::orderBy('name')->get()
    ->keyBy('id')
    ->map(
        function (\App\Models\DocumentType $type) {
            return $type->name;
        }
    )
    ->prepend('[Тип документа]', '')
    ->toArray();
/** @var \App\Models\Document $document */
if (empty($document)) {
    $document = new \App\Models\Document();
}
?>
{!! Form::hidden('id', $document->id) !!}
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => '(создать автоматически)']) !!}
    Необязательное поле: если его не заполнить будет использовано название типа документа.
</div>

<!-- Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type_id', 'Тип документа:') !!}
    {!! Form::select('type_id', $types, $document->type_id, ['class' => 'form-control', 'required' => true]) !!}
</div>

<!-- Car Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('car_id', 'Автомобиль:') !!}
    {!! Form::select('car_id', $cars, $document->car_id, ['class' => 'form-control', 'required' => true]) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Комментарии:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Start Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('start_date', 'Дата начала действия:') !!}
    {!! Form::date('start_date', $document->exists ? null : date('Y-m-d'), ['class' => 'form-control']) !!}
</div>

<!-- End Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('end_date', 'Дата окончания действия:') !!}
    {!! Form::date('end_date', $document->exists ? null : $document::newDocumentEndDate(), ['class' => 'form-control']) !!}
</div>

<!-- Список файлов -->
@include('documents.files', ['document' => $document])

<!-- Загрузка файлов -->
<div class="form-group col-sm-12">
    <span class="btn btn-success fileinput-button">
        <i class="glyphicon glyphicon-plus"></i>
        <span>Добавить файлы...</span>
        <input type="file" name="files[]" multiple>
    </span>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('documents.index') !!}" class="btn btn-default">Отмена</a>
</div>
