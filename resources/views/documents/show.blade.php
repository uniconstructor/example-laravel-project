@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            {!! $document->name !!}
        </h1>
    </section>
    <div class="content">
        @include('flash::message')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('documents.show_fields')
                    <a href="{!! route('documents.index') !!}" class="btn btn-default">К списку</a>
                    <a href="/documents/{{ $document->id }}/edit" class="btn btn-warning">Редактировать</a>
                </div>
            </div>
        </div>
    </div>
@endsection
