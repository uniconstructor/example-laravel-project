<?php 
/** @var $document \App\Models\Document */
?>
<!-- Id Field -->
<!--div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $document->id !!}</p>
</div-->

<div class="row">
    <div class="col-md-4">
        <!-- Name Field -->
        <div class="form-group">
            {!! Form::label('name', 'Название:') !!}
            <p>{!! $document->name !!}</p>
        </div>
    </div>
    <div class="col-md-4">
        <!-- Type Id Field -->
        <div class="form-group">
            {!! Form::label('type_id', 'Тип документа:') !!}
            <p>
                <a href="/documentTypes/{{$document->documentType->id}}">{{ $document->type_name }}</a>
            </p>
        </div>
    </div>
    <div class="col-md-4">
        <!-- Car Id Field -->
        <div class="form-group">
            {!! Form::label('car_id', 'Автомобиль:') !!}
            <p>
                <a href="/cars/{{$document->car->id}}">{{ $document->car->name }}</a>
                [{{ $document->car->reg_number }}]
            </p>
        </div>
    </div>
</div>

<!-- Slug Field -->
<!--div class="form-group">
    {!! Form::label('slug', 'Slug:') !!}
    <p>{!! $document->slug !!}</p>
</div-->

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Комментарии:') !!}
    <p>{!! $document->description !!}</p>
</div>

<div class="row">
    <div class="col-md-6">
        
    </div>
    <div class="col-md-6">
        
    </div>
</div>


<!-- Creator Id Field -->
<!--div class="form-group">
    {!! Form::label('creator_id', 'Загружен пользователем:') !!}
    <p>{!! $document->creator_id !!}</p>
</div-->

<div class="row">
    <div class="col-md-6">
        <!-- Start Date Field -->
        <div class="form-group">
            {!! Form::label('start_date', 'Дата начала действия:') !!}
            <p>{!! local_date($document->start_date, 'd F Y') !!}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Created At Field -->
        <div class="form-group">
            {!! Form::label('created_at', 'Дата добавления:') !!}
            <p>{!! $document->created_at->format('Y-m-d H:i:s') !!}</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <!-- End Date Field -->
        <div class="form-group">
            {!! Form::label('end_date', 'Дата окончания действия:') !!}
            <p>{!! local_date($document->end_date, 'd F Y') !!}</p>
        </div>
    </div>
    <div class="col-md-6">
        <!-- Updated At Field -->
        <div class="form-group">
            {!! Form::label('updated_at', 'Последнее изменение:') !!}
            <p>{!! $document->updated_at->format('Y-m-d H:i:s') !!}</p>
        </div>
    </div>
</div>

<!-- Список файлов -->
@include('documents.files', ['document' => $document])


@section('scripts')
    @parent
    <script>
    </script>
@endsection








