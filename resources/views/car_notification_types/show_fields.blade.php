<?php
$eventOptions = \App\Models\CarNotificationType::getTriggerEventOptions();
?>
<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Описание:') !!}
    <p>{!! $carNotificationType->description !!}</p>
</div>

<!-- Trigger Event Field -->
<div class="form-group">
    {!! Form::label('trigger_event', 'Событие, запускающее оповещение:') !!}
    <p>{!! $eventOptions[$carNotificationType->trigger_event] !!} [{!! $carNotificationType->trigger_event !!}]</p>
</div>

<!-- Subject Template Field -->
<div class="form-group">
    {!! Form::label('subject_template', 'Шаблон темы оповещения:') !!}
    <p>{!! $carNotificationType->subject_template !!}</p>
</div>

<!-- Action Text Template Field -->
<div class="form-group">
    {!! Form::label('action_text_template', 'Шаблон текста на кнопке действия:') !!}
    <p>{!! $carNotificationType->action_text_template !!}</p>
</div>

<!-- Action Url Template Field -->
<div class="form-group">
    {!! Form::label('action_url_template', 'Шаблон Url для действия:') !!}
    <p>{!! $carNotificationType->action_url_template !!}</p>
</div>

<!-- Email Template Field -->
<div class="form-group">
    {!! Form::label('email_template', 'Email-Шаблон:') !!}
    <p>{!! $carNotificationType->email_template !!}</p>
</div>

<!-- Sms Template Field -->
<div class="form-group">
    {!! Form::label('sms_template', 'Sms-Шаблон:') !!}
    <p>{!! $carNotificationType->sms_template !!}</p>
</div>

<!-- Slack Template Field -->
<div class="form-group">
    {!! Form::label('slack_template', 'Slack-Шаблон:') !!}
    <p>{!! $carNotificationType->slack_template !!}</p>
</div>

