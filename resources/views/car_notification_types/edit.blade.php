@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            {!! $carNotificationType->name !!}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($carNotificationType, ['route' => ['carNotificationTypes.update', $carNotificationType->id], 'method' => 'patch']) !!}

                        @include('car_notification_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection