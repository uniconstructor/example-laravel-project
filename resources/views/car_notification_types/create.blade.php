@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Новый тип оповещения
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'carNotificationTypes.store']) !!}

                        @include('car_notification_types.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
