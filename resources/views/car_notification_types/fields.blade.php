<?php
$eventOptions = \App\Models\CarNotificationType::getTriggerEventOptions();
?>
<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('description', 'Описание:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Trigger Event Field -->
<div class="form-group col-sm-6">
    {!! Form::label('trigger_event', 'Событие, запускающее оповещение:') !!}
    {!! Form::select('trigger_event', $eventOptions, null) !!}
</div>

<!-- Subject Template Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subject_template', 'Шаблон темы оповещения:') !!}
    {!! Form::text('subject_template', null, ['class' => 'form-control']) !!}
</div>

<!-- Action Text Template Field -->
<div class="form-group col-sm-6">
    {!! Form::label('action_text_template', 'Шаблон текста на кнопке действия:') !!}
    {!! Form::text('action_text_template', null, ['class' => 'form-control']) !!}
</div>

<!-- Action Url Template Field -->
<div class="form-group col-sm-6">
    {!! Form::label('action_url_template', 'Шаблон Url для действия:') !!}
    {!! Form::text('action_url_template', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Template Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('email_template', 'Email-Шаблон:') !!}
    {!! Form::textarea('email_template', null, ['class' => 'form-control']) !!}
</div>

<!-- Sms Template Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('sms_template', 'Sms-Шаблон:') !!}
    {!! Form::textarea('sms_template', null, ['class' => 'form-control']) !!}
</div>

<!-- Slack Template Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('slack_template', 'Slack-Шаблон:') !!}
    {!! Form::textarea('slack_template', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('carNotificationTypes.index') !!}" class="btn btn-default">Отмена</a>
</div>
