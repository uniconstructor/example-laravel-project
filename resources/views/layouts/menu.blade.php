<li class="{{ Request::is('cars*') ? 'active' : '' }}">
    <a href="{!! route('cars.index') !!}"><i class="fa fa-car"></i><span>Автомобили</span></a>
</li>

<li class="{{ Request::is('departments*') ? 'active' : '' }}">
    <a href="{!! route('departments.index') !!}"><i class="fa fa-building"></i><span>Подразделения</span></a>
</li>

<li class="{{ Request::is('iflatUsers*') ? 'active' : '' }}">
    <a href="{!! route('iflatUsers.index') !!}"><i class="fa fa-user"></i><span>Сотрудники</span></a>
</li>

<li class="{{ Request::is('documents*') ? 'active' : '' }}">
    <a href="{!! route('documents.index') !!}"><i class="fa fa-files-o"></i><span>Документы</span></a>
</li>

<li>
    <span><b style="color: #fff;padding-left: 20px;">Администрирование</b></span>
</li>

<li class="{{ Request::is('documentTypes*') ? 'active' : '' }}">
    <a href="{!! route('documentTypes.index') !!}"><i class="fa fa-file-text"></i><span>Типы документов</span></a>
</li>

<li class="{{ Request::is('carNotificationTypes*') ? 'active' : '' }}">
    <a href="{!! route('carNotificationTypes.index') !!}"><i class="fa fa-bell"></i><span>Типы оповещений</span></a>
</li>

<li class="{{ Request::is('systemLogs*') ? 'active' : '' }}">
    <a href="/systemLogs" target="_blank"><i class="fa fa-terminal"></i><span>Журнал событий</span></a>
</li>

