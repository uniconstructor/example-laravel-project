<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен быть длиннее 6 символов, и совпадать с полем подтверждения пароля.',
    'reset'    => 'Ваш пароль изменен.',
    'sent'     => 'На ваш email отправлена ссылка для восстановления пароля.',
    'token'    => 'Неправильная ссылка для восстановления пароля.',
    'user'     => 'Пользователь с таким email не найден.',

];
