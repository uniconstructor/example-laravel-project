<?php

return [
    
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    
    'accepted'             => 'Необходимо подтвердить согласие в поле :attribute',
    'active_url'           => 'Поле :attribute содержит некорректный URL.',
    'after'                => 'Поле :attribute должно содержать дату после :date.',
    'alpha'                => 'Поле :attribute должно содержать только латинские буквы.',
    'alpha_dash'           => 'Поле :attribute должно содержать только латинские буквы, цифры, и знак подчеркивания.',
    'alpha_num'            => 'Поле :attribute только латинские буквы или цифры.',
    'array'                => 'Поле :attribute должно содержать массив.',
    'before'               => 'Поле :attribute должно содержать дату до :date.',
    'between'              => [
        'numeric' => 'Значение поля :attribute должно быть между :min и :max.',
        'file'    => 'Размер файла в поле :attribute должен быть между :min и :max килобайт.',
        'string'  => 'Длина строки в поле :attribute должна быть между :min и :max',
        'array'   => 'Количество элементов в поле :attribute должно быть между :min и :max',
    ],
    'boolean'              => 'Значение поля :attribute может быть либо истинным (true) либо ложным (false).',
    'confirmed'            => 'Значение в поле :attribute и его подтверждение не совпадают.',
    'date'                 => 'Поле :attribute содержит некорректную дату.',
    'date_format'          => 'Дата :attribute должна быть указана в формате :format.',
    'different'            => 'Значение полей :attribute и :other должны различаться.',
    'digits'               => 'Поле :attribute должно содержать указанное количество цифр: :digits',
    'digits_between'       => 'Поле :attribute должно содержать от :min до :max цифр.',
    'distinct'             => 'Поле :attribute не должно содержать повторяющихся значений.',
    'email'                => 'Поле :attribute должно содержать корректный email.',
    'exists'               => 'Значение указанное в поле :attribute некорректно (возможно выбранные элемент не существует в базе).',
    'filled'               => 'Поле :attribute должно быть заполнено.',
    'image'                => 'Поле :attribute должно содержать изображение.',
    'in'                   => 'Выбранное значение в поле :attribute некорректно.',
    'in_array'             => 'Значение в поле :attribute не найдено в :other.',
    'integer'              => 'В поле :attribute должно быть целое число.',
    'ip'                   => 'Поле :attribute должно содержать корректный IP-адрес.',
    'json'                 => 'Поле :attribute должно быть корректной JSON-строкой.',
    'max'                  => [
        'numeric' => 'Значение в поле :attribute не должно превышать :max',
        'file'    => 'Размер файла в поле :attribute не должен превышать :max килобайт',
        'string'  => 'Значение в поле :attribute не должно превышать :max символов.',
        'array'   => 'Поле :attribute должно содержать максимум :max элементов.',
    ],
    'mimes'                => 'Тип файла ждя поля :attribute должен быть одним из указанных: :values.',
    'min'                  => [
        'numeric' => 'Значение в поле :attribute должно быть :min или больше.',
        'file'    => 'Размер файла в поле :attribute должен быть больше :min килобайт.',
        'string'  => 'Количество символов в поле :attribute должно быть :min или больше.',
        'array'   => 'Поле :attribute должно содержать :min или более элементов.',
    ],
    'not_in'               => 'Выбранное значение в поле :attribute является некорректным.',
    'numeric'              => 'Поле :attribute должно содержать число.',
    'present'              => 'Поле :attribute должно быть заполнено.',
    'regex'                => 'Не соблюден формат ввода данных для поля :attribute',
    'required'             => 'Поле :attribute является обязательным.',
    'required_if'          => 'Поле :attribute является обязательным когда :other содерит значение :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'Значения полей :attribute и :other должны совпадать.',
    'size'                 => [
        'numeric' => 'Значение в поле :attribute должно быть равно :size',
        'file'    => 'Размер файла в поле :attribute должен быть :size килобайт.',
        'string'  => 'Количество символов в поле :attribute должно быть равно :size',
        'array'   => 'Поле :attribute должно содержать :size элементов.',
    ],
    'string'               => 'В поле :attribute должна быть строка.',
    'timezone'             => 'Некорректно указана временная зона в поле :attribute.',
    'unique'               => 'Значение поля :attribute должно быть уникальным, указанное значение уже используется.',
    'url'                  => 'Некорректный формат поля :attribute.',
    
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    
    'required_table_columns' => 'Не заполнены обязательные поля в таблице-форме :attribute',
    
    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */
    
    'attributes' => [
        'reg_number'    => 'Регистрационный номер',
        'start_date'    => 'Дата начала действия',
        'end_date'      => 'Дата окончания действия',
        'name'          => 'Название',
        'description'   => 'Описание',
        'slug'          => 'Код',
        'iflat_user_id' => 'Сотрудник',
        'department_id' => 'Подразделение',
        'car_id'        => 'Автомобиль',
        'status'        => 'Статус',
        'type_id'       => 'Тип',
    ],

];
