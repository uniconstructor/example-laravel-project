##### Установка
```bash
docker -v
Docker version 18.03.0-ce, build 0520e24
```
Клонируем проект
```bash
mkdir ru.iflat.transport
cd ru.iflat.transport/
git clone git@gitlab.com:iflat/transport.git .
```
Заходим в контейнер
```bash
docker-compose exec transport bash
```
В контейнере выполняем:
```bash
composer install
cp .example .env
./artisan key:generate # Полученный ключ записать в .env APP_KEY = base64:...
./artisan migrate
```

##### Build docker image
Билдим образ только если вносили изменения в файл: `transport.docker`
```bash
docker build -f docker/transport.docker -t registry.gitlab.com/iflat/transport .
docker push registry.gitlab.com/iflat/transport
```
