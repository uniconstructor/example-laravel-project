<?php

return [
    
    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | This value is the name of your application. This value is used when the
    | framework needs to place the application's name in a notification or
    | any other location as required by the application or its packages.
    */
    'name'            => 'Transport',
    
    /*
    |--------------------------------------------------------------------------
    | Application Environment
    |--------------------------------------------------------------------------
    |
    | This value determines the "environment" your application is currently
    | running in. This may determine how you prefer to configure various
    | services your application utilizes. Set this in your ".env" file.
    |
    */
    'env'             => env('APP_ENV', 'production'),
    
    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */
    'debug'           => env('APP_DEBUG', true),
    
    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */
    'url'             => env('APP_URL', 'http://iflat-transport.dev'),
    
    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */
    'timezone'        => env('APP_TIMEZONE', 'Europe/Moscow'),
    
    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */
    'locale'          => 'ru',
    
    /**
     * Дополнительные настройки для даты и времени
     *
     * @see http://stackoverflow.com/questions/40073696/laravel-carbon-localization-not-working-get-localized-name-of-month-from-number
     */
    'locale_time'     => 'ru_RU.UTF-8',
    'date_format'     => 'DD Month YYYY',
    'time_format'     => 'HH:MM:SS',
    'datetime_format' => 'DD Month YYYY HH:MM:SS',
    
    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */
    'fallback_locale' => 'en',
    
    /*
    |--------------------------------------------------------------------------
    | Default Phone Number Country
    |--------------------------------------------------------------------------
    |
    */
    'phone_country'   => 'RU',
    
    /*
    |--------------------------------------------------------------------------
    | Default Phone Number Format
    |--------------------------------------------------------------------------
    |
    | libphonenumber\PhoneNumberFormat::E164          = 0;
    | libphonenumber\PhoneNumberFormat::INTERNATIONAL = 1;
    | libphonenumber\PhoneNumberFormat::NATIONAL      = 2;
    | libphonenumber\PhoneNumberFormat::RFC3966       = 3;
    */
    'phone_format'    => 1,
    
    // русский язык для генерируемых тестовых данных
    // @see https://stackoverflow.com/questions/34496720/change-faker-locale-in-laravel-5-2
    'faker_locale'    => 'ru_RU',
    
    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */
    'key'             => env('APP_KEY'),
    'cipher'          => 'AES-256-CBC',
    
    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */
    'log'             => env('APP_LOG', 'daily'),
    'log_level'       => env('APP_LOG_LEVEL', 'debug'),
    
    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */
    
    'providers' => [
        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Notifications\NotificationServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,
        
        /*
         * Package Service Providers...
         */
        /**
         * Поддержка расширенного набора генераторов Laravel (InfyOm)
         *
         * @see http://labs.infyom.com/laravelgenerator/docs/5.3/installation#service-providers
         */
        Collective\Html\HtmlServiceProvider::class,
        Laracasts\Flash\FlashServiceProvider::class,
        Prettus\Repository\Providers\RepositoryServiceProvider::class,
        InfyOm\Generator\InfyOmGeneratorServiceProvider::class,
        InfyOm\AdminLTETemplates\AdminLTETemplatesServiceProvider::class,
        
        /**
         * Laravel datatables
         */
        Yajra\Datatables\DatatablesServiceProvider::class,
        
        /**
         * Swagger API для Laravel
         */
        Jlapp\Swaggervel\SwaggervelServiceProvider::class,
        
        /**
         * Расширенный функционал модели
         *
         * @see https://github.com/jarektkaczyk/eloquence
         */
        Sofa\Eloquence\ServiceProvider::class,
        
        /**
         * Коннектор Fractal для Laravel
         *
         * @see https://github.com/spatie/laravel-fractal
         */
        Spatie\Fractal\FractalServiceProvider::class,
        
        /**
         * Создание библиотеки из загруженных файлов
         *
         * @see https://docs.spatie.be/laravel-medialibrary/v4
         */
        Spatie\MediaLibrary\MediaLibraryServiceProvider::class,
        
        /**
         * Файловый менеджер Laravel
         *
         * @see https://unisharp.github.io/laravel-filemanager/
         */
        Unisharp\Laravelfilemanager\LaravelFilemanagerServiceProvider::class,
        Intervention\Image\ImageServiceProvider::class,
        
        /**
         * Просмотр логов Laravel
         *
         * @see https://github.com/rap2hpoutre/laravel-log-viewer
         */
        Rap2hpoutre\LaravelLogViewer\LaravelLogViewerServiceProvider::class,

        /**
         * @see https://github.com/jasekz/laradrop
         * 
         * @todo не пригодилось, удалить при рефакторинге
         */
        //Jasekz\Laradrop\LaradropServiceProvider::class,
        
        /**
         * Этот пакет требуется для нормальной работы экспорта в PDF
         * 
         * @see https://github.com/barryvdh/laravel-dompdf
         * @see https://github.com/barryvdh/laravel-snappy
         */
        Barryvdh\DomPDF\ServiceProvider::class,
        Barryvdh\Snappy\ServiceProvider::class,

        /**
         * Этот пакет требуется для нормальной работы экспорта в excel
         * 
         * @see https://github.com/Maatwebsite/Laravel-Excel
         */
        Maatwebsite\Excel\ExcelServiceProvider::class,
        
        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        App\Providers\BroadcastServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,
    ],
    
    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */
    
    'aliases' => [
        'App'          => Illuminate\Support\Facades\App::class,
        'Artisan'      => Illuminate\Support\Facades\Artisan::class,
        'Auth'         => Illuminate\Support\Facades\Auth::class,
        'Blade'        => Illuminate\Support\Facades\Blade::class,
        'Bus'          => Illuminate\Support\Facades\Bus::class,
        'Cache'        => Illuminate\Support\Facades\Cache::class,
        'Config'       => Illuminate\Support\Facades\Config::class,
        'Cookie'       => Illuminate\Support\Facades\Cookie::class,
        'Crypt'        => Illuminate\Support\Facades\Crypt::class,
        'DB'           => Illuminate\Support\Facades\DB::class,
        'Eloquent'     => Illuminate\Database\Eloquent\Model::class,
        'Event'        => Illuminate\Support\Facades\Event::class,
        'File'         => Illuminate\Support\Facades\File::class,
        'Gate'         => Illuminate\Support\Facades\Gate::class,
        'Hash'         => Illuminate\Support\Facades\Hash::class,
        'Lang'         => Illuminate\Support\Facades\Lang::class,
        'Log'          => Illuminate\Support\Facades\Log::class,
        'Mail'         => Illuminate\Support\Facades\Mail::class,
        'Notification' => Illuminate\Support\Facades\Notification::class,
        'Password'     => Illuminate\Support\Facades\Password::class,
        'Queue'        => Illuminate\Support\Facades\Queue::class,
        'Redirect'     => Illuminate\Support\Facades\Redirect::class,
        'Redis'        => Illuminate\Support\Facades\Redis::class,
        'Request'      => Illuminate\Support\Facades\Request::class,
        'Response'     => Illuminate\Support\Facades\Response::class,
        'Route'        => Illuminate\Support\Facades\Route::class,
        'Schema'       => Illuminate\Support\Facades\Schema::class,
        'Session'      => Illuminate\Support\Facades\Session::class,
        'Storage'      => Illuminate\Support\Facades\Storage::class,
        'URL'          => Illuminate\Support\Facades\URL::class,
        'Validator'    => Illuminate\Support\Facades\Validator::class,
        'View'         => Illuminate\Support\Facades\View::class,
        // Набор алиасов требуемый для работы расширенного набора генераторов InfyOm
        // @see http://labs.infyom.com/laravelgenerator/docs/5.3/installation#aliases
        'Form'         => Collective\Html\FormFacade::class,
        'Html'         => Collective\Html\HtmlFacade::class,
        'Flash'        => Laracasts\Flash\Flash::class,
        // Laravel Fractal connector
        'Fractal'      => Spatie\Fractal\FractalFacade::class,
        // Laravel filemanager
        'Image'        => Intervention\Image\Facades\Image::class,
        // DOM to PDF conversion
        //'PDF' => Barryvdh\DomPDF\Facade::class,
        //'PDF' => Barryvdh\DomPDF\Facade::class,
        'Excel' => Maatwebsite\Excel\Facades\Excel::class,
    ],
];
