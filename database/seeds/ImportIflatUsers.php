<?php

use Illuminate\Database\Seeder;
use App\Models\IflatUser;

class ImportIflatUsers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \DB::connection('userside')
            ->table('tbl_pers')
            ->where('ISWORK', '=', 1)
            ->get()
            ->toArray();
        
        foreach ($users as $userData) {
            $nameData   = explode(' ', $userData->FIO);
            $lastname   = '';
            $firstname  = '';
            $middlename = '';
            if (isset($nameData[0])) {
                $lastname = $nameData[0];
            }
            if (isset($nameData[1])) {
                $firstname = $nameData[1];
            }
            if (isset($nameData[2])) {
                $middlename = $nameData[2];
            }
            $user = IflatUser::create(
                [
                    'firstname'   => $firstname,
                    'lastname'    => $lastname,
                    'middlename'  => $middlename,
                    'position'    => $userData->DOLG,
                    'phone'       => $userData->ACTUAL_PHONE,
                    'email'       => $userData->ACTUAL_MAIL,
                    'description' => $userData->OPIS,
                ]
            );
            $user->save();
            
            $user->setMeta('iflat_id', $userData->CODE);
            $user->setMeta('iflat_fullname', $userData->FIO);
            $user->setMeta('iflat_shortname', $userData->FIOSHORT);
            $user->save();
        }
    }
}
