<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CarsTableSeeder::class);
        //php artisan db:seed --class="DocumentTypesTableSeeder" -vvv
        $this->call(DocumentTypesTableSeeder::class);
        //php artisan db:seed --class="ImportDepartments" -vvv
        //$this->call(ImportDepartments::class);
        //php artisan db:seed --class="ImportIflatUsers" -vvv
        //$this->call(ImportIflatUsers::class);
    }
}
