<?php

use Illuminate\Database\Seeder;
use App\Models\Department;

class ImportDepartments extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = \DB::connection('userside')->table('tbl_pers_otdel')->get()->toArray();
        
        foreach ($departments as $iflatData) {
            $department = Department::create([
                'name'        => $iflatData->NAZV,
                'description' => $iflatData->OPIS,
            ]);
            $department->save();
            
            $department->setMeta('iflat_id', $iflatData->CODE);
            $department->setMeta('iflat_parent_id', $iflatData->PARENTCODE);
            $department->setMeta('iflat_incal', $iflatData->INCAL);
            $department->save();
        }
    }
}
