<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

/**
 * Создание изначального набора стандартных оповещений
 *
 */
class NotificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('car_notification_types')->insert(
            [
                [
                    'name'                 => 'Оповещение о прохождении ТО',
                    'description'          => 'Определяется пробегом автомобиля. ' .
                        'Высылается за 1000 км до окончания действия ТО. ' .
                        'Интервал пробега для замены масла может быть задан для каждого автомобиля отдельно.',
                    'trigger_event'        => 'App\Events\Car\MaintenanceMileageNotice',
                    'send_email'           => 1,
                    'send_sms'             => 1,
                    'subject_template'     => 'Напоминание: пройти ТО для {{full_name}}',
                    'email_template'       => 'Для автомобиля {{name}} регистрационный номер {{reg_number}} необходимо пройти ТО',
                    'sms_template'         => 'Для автомобиля {{name}} регистрационный номер {{reg_number}} необходимо пройти ТО',
                    'slack_template'       => 'Для автомобиля {{name}} регистрационный номер {{reg_number}} необходимо пройти ТО',
                    'action_url_template'  => '/cars/{{id}}',
                    'action_text_template' => 'Данные автомобиля',
                    'notify_car_user'      => 1,
                    'notify_admin_user'    => 1,
                    'created_at'           => Carbon::now(),
                    'updated_at'           => Carbon::now(),
                ],
                [
                    'name'                 => 'Оповещение о замене масла',
                    'description'          => 'Определяется пробегом автомобиля. ' .
                        'Интервал пробега для ТО может быть задан для каждого автомобиля отдельно.' .
                        'По умолчанию высылается за 10000 км до окончания действия ТО.',
                    'trigger_event'        => 'App\Events\Car\OilRefreshMileageNotice',
                    'send_email'           => 1,
                    'send_sms'             => 1,
                    'subject_template'     => 'Напоминание: замена масла для {{full_name}}',
                    'email_template'       => 'Для автомобиля {{name}} регистрационный номер {{reg_number}} необходимо заменить масло',
                    'sms_template'         => 'Для автомобиля {{name}} регистрационный номер {{reg_number}} необходимо заменить масло',
                    'slack_template'       => 'Для автомобиля {{name}} регистрационный номер {{reg_number}} необходимо заменить масло',
                    'action_url_template'  => '/cars/{{id}}',
                    'action_text_template' => 'Данные автомобиля',
                    'notify_car_user'      => 1,
                    'notify_admin_user'    => 1,
                    'created_at'           => Carbon::now(),
                    'updated_at'           => Carbon::now(),
                ],
                [
                    'name'                 => 'Оповещение КАСКО (за 2 недели)',
                    'description'          => 'Высылается за две недели до даты окончания действия документа',
                    'trigger_event'        => 'App\Events\Car\KaskoExpirationNotice',
                    'send_email'           => 1,
                    'send_sms'             => 1,
                    'subject_template'     => 'Напоминание: КАСКО для {{full_name}} истекает через 2 недели',
                    'email_template'       => 'Для автомобиля {{name}} регистрационный номер {{reg_number}} ' .
                        'через две недели заканчивается действие КАСКО',
                    'sms_template'         => 'Для автомобиля {{name}} регистрационный номер {{reg_number}} ' .
                        'через две недели заканчивается действие КАСКО',
                    'slack_template'       => 'Для автомобиля {{name}} регистрационный номер {{reg_number}} ' .
                        'через две недели заканчивается действие КАСКО',
                    'action_url_template'  => '/cars/{{id}}',
                    'action_text_template' => 'Данные автомобиля',
                    'notify_car_user'      => 1,
                    'notify_admin_user'    => 1,
                    'created_at'           => Carbon::now(),
                    'updated_at'           => Carbon::now(),
                ],
                [
                    'name'                 => 'Оповещение ОСАГО (за 2 недели)',
                    'description'          => 'Высылается за две недели до даты окончания действия документа',
                    'trigger_event'        => 'App\Events\Car\OsagoExpirationNotice',
                    'send_email'           => 1,
                    'send_sms'             => 1,
                    'subject_template'     => 'Напоминание: ОСАГО для {{full_name}} истекает через 2 недели',
                    'email_template'       => 'Для автомобиля {{name}} регистрационный номер {{reg_number}} ' .
                        'через две недели заканчивается действие ОСАГО',
                    'sms_template'         => 'Для автомобиля {{name}} регистрационный номер {{reg_number}} ' .
                        'через две недели заканчивается действие ОСАГО',
                    'slack_template'       => 'Для автомобиля {{name}} регистрационный номер {{reg_number}} ' .
                        'через две недели заканчивается действие ОСАГО',
                    'action_url_template'  => '/cars/{{id}}',
                    'action_text_template' => 'Данные автомобиля',
                    'notify_car_user'      => 1,
                    'notify_admin_user'    => 1,
                    'created_at'           => Carbon::now(),
                    'updated_at'           => Carbon::now(),
                ],
            ]
        );
    }
}
