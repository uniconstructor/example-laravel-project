<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\Models\DocumentType;

/**
 * Создаем стандартный набор типов документов
 */
class DocumentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            [
                'name'        => 'Полис КАСКО',
                'slug'        => 'kasko',
                'description' => 'Страховой полис КАСКО',
            ],
            [
                'name'        => 'Полис ОСАГО',
                'slug'        => 'osago',
                'description' => 'Страховой полис ОСАГО',
            ],
            [
                'name'        => 'ПТС',
                'slug'        => 'pts',
                'description' => 'Паспорт транспортного средства',
            ],
            [
                'name'        => 'СТС',
                'slug'        => 'sts',
                'description' => 'Свидетельство о регистрации транспортного средства',
            ],
            [
                'name'        => 'Техосмотр',
                'slug'        => 'maintaince',
                'description' => 'Свидетельство о техосмотре транспортного средства',
            ],
        ];
        foreach ($types as $type) {
            DocumentType::create($type);
        }
        /*DB::table('document_types')->insert([
            [
                'name'        => 'Полис КАСКО',
                'slug'        => 'kasko',
                'description' => 'Страховой полис КАСКО',
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now(),
            ],
            [
                'name'        => 'Полис ОСАГО',
                'slug'        => 'osago',
                'description' => 'Страховой полис ОСАГО',
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now(),
            ],
            [
                'name'        => 'ПТС',
                'slug'        => 'pts',
                'description' => 'Паспорт транспортного средства',
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now(),
            ],
            [
                'name'        => 'СТС',
                'slug'        => 'sts',
                'description' => 'Свидетельство о регистрации транспортного средства',
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now(),
            ],
            [
                'name'        => 'Техосмотр',
                'slug'        => 'maintaince',
                'description' => 'Свидетельство о техосмотре транспортного средства',
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now(),
            ],
        ]);*/
    }
}