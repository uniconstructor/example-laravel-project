<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

/**
 * Заполняем таблицу cars демонстрационными данными
 *
 * Class CarsTableSeeder
 */
class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')->insert([
            [
                'vendor'        => 'Lada',
                'model'         => 'Largus',
                'reg_number'    => 'ф123ва750',
                'description'   => 'Автомобиль Лада б/у',
                'status'        => 'active',
                'created_at'    => Carbon::createFromDate(2011, 11, 20),
                'updated_at'    => Carbon::now(),
            ],
            [
                'vendor'        => 'Fiat',
                'model'         => 'Doblo',
                'reg_number'    => 'к456он750',
                'description'   => 'Каблук для монтажников',
                'status'        => 'active',
                'created_at'    => Carbon::createFromDate(2015, 5, 25),
                'updated_at'    => Carbon::now(),
            ],
            [
                'vendor'        => 'Lada',
                'model'         => 'Priora',
                'reg_number'    => 'д123зо750',
                'description'   => '',
                'status'        => 'draft',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ],
        ]);
    }
}
