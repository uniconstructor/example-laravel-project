<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Database\Migration;

class CreateTasksTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->integer('maintenance_interval')
                ->after('mileage')->unsigned()->nullable()->default(15000)->index();
            $table->integer('oil_refresh_interval')
                ->after('mileage')->unsigned()->nullable()->default(10000)->index();
            $table->integer('last_maintenance')
                ->after('mileage')->unsigned()->nullable()->default(0)->index();
            $table->integer('last_oil_refresh')
                ->after('mileage')->unsigned()->nullable()->default(0)->index();
            $table->integer('oil_notification_margin')
                ->after('mileage')->unsigned()->nullable()->default(10000)->index();
            $table->integer('maintenance_notification_margin')
                ->after('mileage')->unsigned()->nullable()->default(1000)->index();
        });
        
        Schema::create('car_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('car_id')->unsigned()->nullable()->default(0)->index();
            $table->integer('task_type_id')->unsigned()->nullable()->default(0)->index();
            $table->string('name')->nullable()->index();
            $table->text('description')->nullable();
            $table->string('status')->nullable()->default('pending')->index();
    
            $table->timestamps();
            $table->softDeletes();
            $table->index('created_at');
            $table->index('updated_at');
            $table->index('deleted_at');
        });
        
        Schema::create('car_task_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->index();
            $table->text('description')->nullable();
            $table->string('pending_event')->nullable()->index();
            $table->string('active_event')->nullable()->index();
            $table->string('finish_event')->nullable()->index();
            $table->string('expiring_event')->nullable()->index();
            $table->string('expired_event')->nullable()->index();
            $table->string('failed_event')->nullable()->index();
            
            $table->timestamps();
            $table->softDeletes();
            $table->index('created_at');
            $table->index('updated_at');
            $table->index('deleted_at');
        });
    
        Schema::create('car_notification_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->index();
            $table->text('description')->nullable();
            $table->string('level')->nullable()->default('info')->index();
            $table->string('trigger_event')->nullable()->index();
            $table->boolean('send_email')->nullable()->default(1)->index();
            $table->boolean('send_sms')->nullable()->default(1)->index();
            $table->string('subject_template')->nullable();
            $table->string('action_text_template')->nullable();
            $table->string('action_url_template')->nullable();
            $table->text('email_template')->nullable();
            $table->text('sms_template')->nullable();
            $table->text('slack_template')->nullable();
            $table->boolean('notify_car_user')->nullable()->default(1)->index();
            $table->boolean('notify_admin_user')->nullable()->default(1)->index();
            
            $table->timestamps();
            $table->softDeletes();
            $table->index('created_at');
            $table->index('updated_at');
            $table->index('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cars', function (Blueprint $table) {
            if (Schema::hasColumn('cars', 'maintenance_interval')) {
                $table->dropColumn('maintenance_interval');
            }
            if (Schema::hasColumn('cars', 'oil_refresh_interval')) {
                $table->dropColumn('oil_refresh_interval');
            }
            if (Schema::hasColumn('cars', 'last_maintenance')) {
                $table->dropColumn('last_maintenance');
            }
            if (Schema::hasColumn('cars', 'last_oil_refresh')) {
                $table->dropColumn('last_oil_refresh');
            }
            if (Schema::hasColumn('cars', 'oil_notification_margin')) {
                $table->dropColumn('oil_notification_margin');
            }
            if (Schema::hasColumn('cars', 'maintenance_notification_margin')) {
                $table->dropColumn('maintenance_notification_margin');
            }
            if (Schema::hasColumn('cars', 'maintenance_notification_margin')) {
                $table->dropColumn('maintenance_notification_margin');
            }
        });
    
        Schema::dropIfExists('car_tasks');
        Schema::dropIfExists('car_task_types');
        Schema::dropIfExists('car_notification_types');
    }
}
