<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDepartmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->index();
            $table->text('description')->nullable();
            $table->integer('parent_id')->unsigned()->nullable()->default(0)->index();
            $table->integer('_lft')->unsigned()->nullable()->default(0)->index();
            $table->integer('_rgt')->unsigned()->nullable()->default(0)->index();
            
            $table->timestamps();
            $table->softDeletes();
    
            $table->index('created_at');
            $table->index('updated_at');
            $table->index('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('departments');
    }
}
