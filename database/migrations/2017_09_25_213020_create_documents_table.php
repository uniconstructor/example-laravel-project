<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->index();
            $table->string('slug')->nullable()->index();
            $table->text('description')->nullable();
            $table->integer('type_id')->unsigned()->nullable()->default(0)->index();
            $table->integer('car_id')->unsigned()->nullable()->default(0)->index();
            $table->integer('creator_id')->unsigned()->nullable()->default(0)->index();
            $table->date('start_date')->nullable()->index();
            $table->date('end_date')->nullable()->index();
            
            $table->timestamps();
            $table->softDeletes();
    
            $table->index('created_at');
            $table->index('updated_at');
            $table->index('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('documents');
    }
}
