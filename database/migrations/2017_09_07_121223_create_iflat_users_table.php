<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIflatUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iflat_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('department_id')->unsigned()->nullable()->default(0)->index();
            $table->string('firstname')->nullable()->index();
            $table->string('lastname')->nullable()->index();
            $table->string('middlename')->nullable()->index();
            $table->string('email')->nullable()->index();
            $table->string('phone')->nullable()->index();
            $table->text('description')->nullable();
            $table->string('position')->nullable()->index();
            $table->string('status')->nullable()->index();
            
            $table->timestamps();
            $table->softDeletes();
    
            $table->index('created_at');
            $table->index('updated_at');
            $table->index('deleted_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('iflat_users');
    }
}
