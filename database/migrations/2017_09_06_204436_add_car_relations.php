<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCarRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->unsignedBigInteger('iflat_user_id')->after('description')->default(0)->index();
            $table->unsignedBigInteger('department_id')->after('description')->default(0)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cars', function (Blueprint $table) {
            if (Schema::hasColumn('cars', 'iflat_user_id')) {
                $table->dropColumn('iflat_user_id');
            }
            if (Schema::hasColumn('cars', 'department_id')) {
                $table->dropColumn('department_id');
            }
        });
    }
}
